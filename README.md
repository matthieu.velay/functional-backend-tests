# Functional routing tests

This repo contains functional tests dealing with routing stack e.g Tree, Maestro, Octopus


# How to launch tests

Export the following environment variables in your .bashrc or .zshrc, etc
```
export PUSHER_APPKEY=XXXX
export OCTOPUS_AUTH=XXXX
export TWILIO_AUTH=XXXX
export TWILIO_SID=XXXX
export TWILIO_TOKEN=XXX
```

## Use a virtual env


```
~/b/functionnal-backend-tests ❯❯❯ python3.7 -m venv routing-tests                                                                          
~/b/functionnal-backend-tests ❯❯❯ . ./routing-tests/bin/activate                                                
```

## Install dependencies

```
(routing-tests) ~/b/functionnal-backend-tests ❯❯❯ python3.7 -m pip install -r requirements.txt
```

## Launch all the tests

```
~/b/functionnal-backend-tests ❯❯❯ PYTHONPATH=. python3.7 -m pytest --html=report.html -vs tests/
```

# How to encrypt new credentials

Use the dedicated `add_new_users_encrypted` function in [back_tests/utils.py](https://bitbucket.org/aircall/functional-routing-tests/src/master/back_tests/utils.py)

Example in the terminal

```
~/b/functionnal-backend-tests ❯❯❯cd back_tests/framework/common
~/b/functionnal-backend-tests ❯❯❯python3  -c "from utils import add_new_users_encrypted;add_new_users_encrypted('environment_name', 'new_user@aircall.io', 'PASSWORD')"
```

# Pipelines

## Build strategy:

The tests strategy depends on the branch name: 

* __nobuild/*__   : None test executed
* __messaging/*__  : Messaging backend tests 
* __routing/*__    : Routing backend tests 
* __front/*__      : Routing frontend tests
* __master__       : messaging AND routing backend tests
* __default__      : messaging AND routing backend tests
