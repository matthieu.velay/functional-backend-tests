import json
import os
import logging

import requests

OCTOPUS_AUTH = os.getenv("OCTOPUS_AUTH")

log = logging.getLogger("OCTOPUS")

session = requests.Session()


class Octopus(object):
    instance = None

    @classmethod
    def get_instance(cls):
        """This method
        :return: class instance
        """
        if cls.instance is None:
            cls.instance = Octopus()
        return cls.instance

    def __init__(self):
        """
        __init__ method need to be documented
        """
        self.base_url = "https://octopus.aircall-staging.com/"
        self.headers = {"Authorization": "Basic {0}".format(OCTOPUS_AUTH)}

    def get_queue(self, company_id):
        """GET octopus queue endpoint

        Args:
            company_id (str): current company Id
        Returns:
            response (dict): payload response
        """

        url = self.base_url + "api/debug/companies/{0}/call_tasks".format(company_id)
        response = session.get(url, headers=self.headers)
        return response.json()

    def flush_agent_queue(self, user_id):
        """FLUSH agent queue

        Args:
            user_id (str): current agent Id
        Returns:
            response (dict): payload response
        """

        log.info("Flush user {0} in octopus".format(user_id))
        url = self.base_url + "api/debug/queues/{0}/flush".format(user_id)
        payload = {}
        response = session.post(url, headers=self.headers, data=json.dumps(payload))
        log.info("Flush agent {0} response {1}".format(user_id, response.status_code))
        return response

    # TODO: warning Sonarlint about complexity
    def is_agent_in_queue(self, user_id, company_id, state=None, task_id=None):
        """Check if user is in octopus

        Args:
            user_id (str): current agent Id
            company_id (str): current company Id
            task_id (str): call uuid
            state (str): current state, could 'creating', 'ringing', ...
                         if None, returns as soon as user_id is in the queue
        Returns:
            is_agent_in_queue (bool)
        """
        # Check agent is in the octopus queue
        log.debug("Starting agent is in octopus : {0} ".format(user_id))

        queue = self.get_queue(company_id)

        is_agent_in_queue = False
        for elt in queue:
            leg = elt.get("identity")
            if task_id:
                if leg["task_id"] != task_id:
                    log.info(
                        "Different tasks ID: {0} != {1}".format(leg["task_id"], task_id)
                    )
                    continue
            if leg["agent_id"] == user_id:
                log.debug(": user {0} in octopus: {1}".format(user_id, leg))
                if state is None:
                    return True
                # Check that the user is in the expected state <state>
                dispatches = elt.get("dispatches")
                is_agent_in_queue = check_dispatches(dispatches, user_id, state)

        return is_agent_in_queue

    def is_task_in_queue(self, task_id, company_id):
        """
        Args:
            company_id (str): current company Id
            task_id (str): call uuid
        Returns:
            is_task_in_queue (bool)
        """
        # Check task is in the octopus queue
        log.debug("Starting task is in octopus : {0} ".format(task_id))

        queue = self.get_queue(company_id)

        for elt in queue:
            leg = elt.get("identity")
            if leg["task_id"] == task_id:
                log.info("Octopus leg: {0}".format(leg))
                return True
        else:
            return False


def check_dispatches(dispatches, user_id, state):
    """Check dispatches, if there is one with user_id and state then returns True

    Args:
        dispatches (dict): all current dispatches
        user_id (str): current user Id
        state (str): current state, could 'creating', 'ringing', ...
    Returns:
        response (dict): payload response
    """

    for key in dispatches:
        # Check desktop agent is in <state> state
        # Only desktop device id is None
        dispatch = dispatches[key]
        if "state" in dispatch and dispatch["id"] is None:
            current_state = dispatch["state"]["state"]
            log.debug(
                "Found user {0} is in octopus with state {1}".format(user_id, state)
            )
            if current_state == state:
                return True
    else:
        return False


octopus = Octopus.get_instance()
