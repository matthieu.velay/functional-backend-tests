import logging
import os
import requests
from requests.auth import HTTPBasicAuth

#  TODO: optional base url stag - prod
BASE_URL = "https://api.aircall-staging.com/v1/"
USERNAME_PUBLIC_API = os.environ.get("USERNAME_PUBLIC_API")
PASSWORD_PUBLIC_API = os.environ.get("PASSWORD_PUBLIC_API")
AUTH_PUBLIC_API = HTTPBasicAuth(USERNAME_PUBLIC_API, PASSWORD_PUBLIC_API)

log = logging.getLogger("PUBLIC_API")


class PublicAPI:
    instance = None

    @classmethod
    def get_instance(cls):
        """This method
        :return: class instance
        """
        if cls.instance is None:
            cls.instance = PublicAPI()
        return cls.instance

    def __init__(self):
        self.base_url = BASE_URL
        self.auth_public_api = HTTPBasicAuth(USERNAME_PUBLIC_API, PASSWORD_PUBLIC_API)

    def get_calls(self, from_, to_):
        """ """
        url = self.base_url + "calls/?from={0}&to={1}".format(from_, to_)
        response = requests.get(url, auth=self.auth_public_api)
        return response.json()

    def transfers(self, call_id, payload):
        """ """
        url = self.base_url + "calls/{0}/transfers".format(call_id)
        log.info(url)
        response = requests.post(url, auth=self.auth_public_api, data=payload)

        return response

    def update_webhook(self, webhook_id):
        """ """
        url = self.base_url + "webhooks/{0}".format(webhook_id)

        log.info(url)
        payload = (
            '{"url": "https://telephony-new-routing-38780.ngrok.io", "active": true}'
        )
        headers = {"content-Type": "application/json"}
        response = requests.put(
            url, auth=self.auth_public_api, data=payload, headers=headers
        )
        return response.json()


public_api = PublicAPI.get_instance()
