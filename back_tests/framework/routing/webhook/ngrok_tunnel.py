import time
import logging
import subprocess
import os
from distutils.spawn import find_executable


logger = logging.getLogger("NGROK")

NGROK_AUTH_TOKEN = os.environ.get("NGROK_AUTH_TOKEN")


class NgrokTunnel:
    def __init__(self, port=4040, subdomain_base="telephony-new-routing-38780"):
        """
        Ngrok constructor
        """
        assert find_executable("ngrok"), "ngrok command must be installed"

        self.port = port
        self.auth_token = NGROK_AUTH_TOKEN
        self.subdomain = subdomain_base
        self.ngrok = None

    def start(self, ngrok_delay_starting=0.5):
        """
        Starts the thread on the background and blocks until we get a tunnel URL.
        """

        logger.debug(
            "Starting ngrok tunnel {0} for port {1}".format(self.subdomain, self.port)
        )

        subprocess.Popen(
            ["ngrok", "authtoken {}".format(self.auth_token)],
            stdout=subprocess.DEVNULL,
        )

        command = [
            "ngrok",
            "http",
            "-subdomain={}".format(self.subdomain),
            str(self.port),
        ]

        logger.info(" ".join(command))

        self.ngrok = subprocess.Popen(
            [
                "ngrok",
                "http",
                "-subdomain={}".format(self.subdomain),
                str(self.port),
            ],
            stdout=subprocess.DEVNULL,
        )

        # See that we don't instantly die
        time.sleep(ngrok_delay_starting)
        assert self.ngrok.poll() is None, "ngrok terminated with error"
        url = "https://{}.ngrok.io".format(self.subdomain)
        logger.info(url)
        return url

    def stop(self):
        """Tell ngrok to tear down the tunnel.

        Stop the background tunneling process.
        """
        self.ngrok.terminate()
