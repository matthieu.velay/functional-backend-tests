from flask import Flask, request, jsonify
import logging


log = logging.getLogger("FLASK_SERVER")


class FlaskServer:
    def __init__(self, host="localhost", port=4040):
        self.host = host
        self.port = port
        self.app = Flask("Webhook Public API")

        # Webhook event
        self.receive_event = []
        self.call_answered = []
        self.call_created = []
        self.call_transferred = []
        self.call_ringing_on_agent = []
        self.call_hangup = []
        self.call_ended = []

    def reset(self):
        self.receive_event.clear()
        self.call_answered.clear()
        self.call_created.clear()
        self.call_transferred.clear()
        self.call_ringing_on_agent.clear()
        self.call_hangup.clear()
        self.call_ended.clear()

    def start_app(self):
        @self.app.route("/", methods=["POST", "GET"])
        def event_process():
            if request.method == "POST":
                response = request.json
                log.info(response.get("event"))

                if response.get("event") == "call.answered":
                    self.call_answered.append(response)

                if response.get("event") == "call.created":
                    self.call_created.append(response)

                elif response.get("event") == "call.transferred":
                    self.call_transferred.append(response)

                elif response.get("event") == "call.ringing_on_agent":
                    self.call_ringing_on_agent.append(response)

                elif response.get("event") == "call.hangup":
                    self.call_hangup.append(response)

                elif response.get("event") == "call.ended":
                    self.call_ended.append(response)

                else:
                    self.receive_event.append(response)

            return jsonify(success=True)

        self.app.run(self.host, self.port)
