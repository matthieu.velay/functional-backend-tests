import json
import requests
import logging

session = requests.Session()

log = logging.getLogger("DASHBOARD")


class Dashboard(object):
    def __init__(self, email, password):
        """
        __init__ method need to be documented
        """
        self.base_url = "https://internal.aircall-staging.com"
        self.headers = {"Content-Type": "application/json", "User-Agent": "Mozilla/5.0"}

        # Initialize during incoming call
        self.tasks = []
        self.task_id = None

        # Initialize in login
        self.user_id = None
        self.company_id = None
        self.auth_token = None

        # Pusher availability
        self.pusher = None

        self.login(email, password)

    def login(self, email, password):
        """Login through API"""
        url = self.base_url + "/users/sign_in.json"
        payload = {"user": {"email": email, "password": password}}

        response = session.post(
            url, headers=self.headers, data=json.dumps(payload)
        ).json()

        if response["success"]:
            self.user_id = response["resource"]["id"]
            self.company_id = response["resource"]["company_id"]
            self.auth_token = response.get("access_token")
            log.info("User {0} login".format(self.user_id))
        else:
            log.error("User {0} can't login".format(self.user_id))
        return response

    def logout(self):
        """Logout through API, clean all data
        Args:
            None

        Returns:
            response (dict): response payload
        """

        url = self.base_url + "/users/sign_out.json"
        payload = {}
        response = session.post(url, headers=self.headers, data=json.dumps(payload))
        user_id = self.user_id
        if response.status_code == 204:
            self.user_id = None
            self.company_id = None
            self.auth_token = None
            self.tasks = []
            log.info("User {0} logout".format(user_id))
        else:
            log.error("User {0} can't logout".format(user_id))
        return response

    def create_team(self, team_name, user_ids):

        url = self.base_url + "/v3/teams"
        data = {
            "team": {"name": team_name, "image_name": "smile", "user_ids": user_ids}
        }

        headers = self.headers
        headers["authorization"] = "Bearer " + self.auth_token
        log.info("Create team {0} with agents: {1}".format(team_name, user_ids))

        response = session.post(url, headers=headers, data=json.dumps(data))
        return response.json()

    def delete_team(self):
        pass

    def add_user_team(self, number_id, priority, queuing_timeout, team_id):

        url = self.base_url + "/v4/numbers/" + number_id + "dispatch_groups"
        data = {
            "data": {
                "type": "dispatch_group",
                "attributes": {
                    "priority": priority,
                    "queuing_timeout": queuing_timeout,
                },
                "relationships": {
                    "dispatchable": {"data": {"id": team_id, "type": "team"}}
                },
            }
        }

        headers = self.headers
        headers["authorization"] = "Bearer " + self.auth_token

        response = session.post(url, headers=headers, data=json.dumps(data))
        return response.json()

    def get_dispatch_groups(self, number_id):
        """Retrieve all dispatch groups given a number ID"""
        url = self.base_url + "/v4/numbers/" + str(number_id) + "/dispatch_groups"
        headers = self.headers
        headers["authorization"] = "Bearer " + self.auth_token

        log.info(f"Get dispatch groups from number {number_id}")

        response = session.get(url, headers=headers)
        return response.json()

    def add_dispatch_group(self, number_id, user_id, priority):
        """
        Add user_id in number_id distribution with `priority`
        """
        payload = {
            "data": {
                "type": "dispatch_group",
                "attributes": {"priority": f"{priority}"},
                "relationships": {
                    "dispatchable": {"data": {"id": f"{user_id}", "type": "user"}}
                },
            }
        }
        url = f"{self.base_url}/v4/numbers/{number_id}/dispatch_groups/"
        headers = self.headers
        headers["authorization"] = "Bearer " + self.auth_token

        log.info(f"Add user {user_id} from distribution number {number_id}")
        response = session.post(url, data=json.dumps(payload), headers=headers)
        return response.status_code

    def delete_dispatch_group(self, number_id, user_id):
        """
        delete group with user_id inside given a number_id
        """

        dispatch_groups = self.get_dispatch_groups(number_id)

        def is_user_in_distrib():
            """
            Check that current user is in number distribution
            """
            for group in dispatch_groups["data"]:
                c_user_id = group["relationships"]["dispatchable"]["data"]["id"]
                if str(user_id) == str(c_user_id):
                    group_id = group["id"]
                    log.info(f"Found user {user_id} in group {group_id}")
                    return group_id
                else:
                    continue

            log.info(f"{user_id} not found in {number_id} distribution")
            return None

        group_id = is_user_in_distrib()

        if not group_id:
            # No group id found for this user_id
            return 404

        headers = self.headers
        headers["authorization"] = "Bearer " + self.auth_token

        url = f"{self.base_url}/v4/numbers/{number_id}/dispatch_groups/{group_id}"

        log.info(f"Delete user {user_id} from distribution number {number_id}")
        response = session.delete(url, headers=headers)

        return response.status_code

    def get_number_data(self, number_id):
        """Retrieve number data"""
        url = self.base_url + "/v4/numbers/" + str(number_id)
        headers = self.headers
        headers["authorization"] = "Bearer " + self.auth_token

        response = session.get(url, headers=headers)
        return response.json()

    def get_call_info(self, call_id, number_id):
        """Retrieve call infos"""
        url = (
            self.base_url + "/v3/calls/" + str(call_id) + "?number_id=" + str(number_id)
        )
        headers = self.headers
        headers["authorization"] = "Bearer " + self.auth_token

        response = session.get(url, headers=headers)
        return response.json()


def compare_dispatch_groups(group1, group2):
    """Compare dispatch groups"""
    return (
        group1.get("type") == group2.get("type")
        and group1.get("attributes") == group2.get("attributes")
        and group1.get("relationships") == group2.get("relationships")
    )
