import logging
import os
import time

from twilio.rest import Client
from twilio.twiml.voice_response import VoiceResponse

TWILIO_AUTH = os.environ["TWILIO_AUTH"]
TWILIO_SID = os.environ["TWILIO_SID"]
TWILIO_TOKEN = os.environ["TWILIO_TOKEN"]

RINGSTONE = "https://cdn.aircall.io/ringtones/applause_ringtone_30s.mp3"

log = logging.getLogger("TWILIO")
twilio_logger = logging.getLogger("twilio.http_client")
twilio_logger.setLevel(logging.WARNING)

# how to add new Twiml : https://www.twilio.com/docs/voice/twiml
class Twilio:
    instance = None

    @classmethod
    def get_instance(cls):
        """This method
        :return: class instance
        """
        if cls.instance is None:
            cls.instance = Twilio()
        return cls.instance

    def __init__(self):
        """
        __init__ method need to be documented
        """
        self.voice_response = VoiceResponse()
        self.client = Client(TWILIO_SID, TWILIO_TOKEN)
        self.call_sid_list = []
        self.sms_sid_list = []

    def inbound_call(self, from_, to_, loop_=1):
        """Trigger an inbound call
        Args:
            from_ (str): caller number
            to_ (str): called number
            loop_ (int): 30-seconds loop, default 1
        Returns:
            sid (str): twilio SID
        """

        self.voice_response = VoiceResponse()

        self.voice_response.play(RINGSTONE, loop=loop_)

        call_info = self.client.calls.create(
            twiml=self.voice_response,
            to=to_,
            from_=from_,
        )
        log.info(f"Create call: {call_info.sid} to {to_}")
        # Add this call to the list of inbound_calls
        self.call_sid_list.append(call_info.sid)
        return call_info.sid

    def get_call(self, call_sid):
        """Get call resource
        Args:
            call_sid (str): twilio SID
        Returns:
            payload (dict): call infos
        """
        call_info = self.client.calls(call_sid).fetch()

        return call_info

    def hang_up(self, call_sid):
        """
        Update the call from call uuid in order to hang_up
        Args:
            call_sid: call_uuid string
        Returns:
            call_sid
        """
        self.voice_response = VoiceResponse()

        self.voice_response.hangup()
        call_info = self.client.calls(call_sid).update(twiml=self.voice_response)
        log.info("Hang up: {0}".format(call_info.sid))

        # Remove this call to the list of inbound calls
        self.call_sid_list.remove(call_info.sid)

        return call_info.sid

    def press_digits(self, digits, call_sid, loop_=1):
        """
        Update the call from call uuid in order to press digits number
        Then continue playing music
        Args:
            digits: number to press
            call_sid: call_sid string
            loop_: 30-seconds loop, default 1
        Returns:
            call_sid
        """
        self.voice_response = VoiceResponse()

        self.voice_response.play(digits=str(digits))
        self.voice_response.play(RINGSTONE, loop=loop_)

        call_info = self.client.calls(call_sid).update(twiml=self.voice_response)

        return call_info.sid

    def get_call_duration(self, call_sid):
        call_info = self.get_call(call_sid)
        return float(call_info.duration)

    def waiting_until_call_status(self, call_sid, exp_status, timeout, loop_timeout=1):
        """
        Wait until the call is in <exp_status> or timeout
        Args:
            call_sid: call_sid string
            exp_status: expected status
            timeout (int): timeout in seconds
        Returns:
            boolean
        """
        start = time.time()
        status = None
        while time.time() - start < timeout and status != exp_status:
            call = self.get_call(call_sid)
            status = call.status
            log.info("{0}: {1}".format(status, call_sid))
            time.sleep(loop_timeout)
        return status == exp_status

    def waiting_call_status_remains(
        self, call_sid, exp_status, timeout, loop_timeout=1
    ):
        """
        Wait if the call is in <exp_status> during <timeout> or returns false
        Args:
            call_sid: call_sid string
            exp_status: expected status
            timeout (int): timeout in seconds
        Returns:
            boolean
        """
        start = time.time()
        status = exp_status
        while time.time() - start < timeout and status == exp_status:
            call = self.get_call(call_sid)
            status = call.status
            log.info("{0}: {1}".format(status, call_sid))
            time.sleep(loop_timeout)
        return status == exp_status

    def inbound_sms(self, body_, from_, to_):
        """Trigger an inbound sms
        Args:
            body="Join Earth's mightiest heroes. Like Kevin Bacon.",
            from_='+15017122661',
            to='+15558675310'
        Returns:
            sid (str): message.sid
        """
        sms_info = self.client.messages.create(body=body_, from_=from_, to=to_)
        log.info("Send sms: {0}".format(sms_info.sid))
        # Add this sms to the list of sms
        self.sms_sid_list.append(sms_info.sid)
        return sms_info.sid

    def status_sms(self, sms_sid, exp_status, timeout, loop_timeout=1):

        start = time.time()
        status = None
        while time.time() - start < timeout and status != exp_status:
            sms = self.get_sms(sms_sid)
            status = sms.status
            log.info(
                "Message status :{0} is for MessageID :{1}".format(status, sms_sid)
            )
            time.sleep(loop_timeout)
        return status == exp_status

    def get_sms(self, sms_sid):
        """Get sms resource
        Args:
            sms_sid (str): twilio SID
        Returns:
            payload (dict): sms infos
        """
        sms_info = self.client.messages(sms_sid).fetch()
        return sms_info

    def error_sms(self, body_, from_, to_):

        sms_fail_info = self.client.messages.create(body=body_, from_=from_, to=to_)
        log.info("sms failed: {0}".format(sms_fail_info.error_message))
        # Add this sms to the list of sms
        return sms_fail_info.error_message


twilio = Twilio.get_instance()
