import json
import logging
import os
import time

import pysher
import requests


APPKEY = os.environ["PUSHER_APPKEY"]
SECRET = os.environ["PUSHER_SECRET"]

session = requests.Session()

log = logging.getLogger("PUSHER")


class Pusher(object):
    def __init__(self, phone):
        self.phone = phone

    def _channel_incoming_call(self, *args, **kwargs):
        if self.phone.user_id:
            received_time = time.time()
            log.info("Incoming Call {0}: {1}".format(self.phone.user_id, args))

            current_task = json.loads(args[0])["task_id"]
            self.phone.task_id = current_task
            self.phone.tasks.append(current_task)
            self.phone.received_w_time.append((current_task, received_time))
            self.phone.received.append(current_task)

    def _channel_incoming_call_rejected(self, *args, **kwargs):
        if self.phone.user_id:
            rejected_time = time.time()
            log.info("Incoming Call Rejected {0}: {1}".format(self.phone.user_id, args))

            task_to_remove = json.loads(args[0])["task_id"]
            self.phone.rejected.append(task_to_remove)
            self.phone.rejected_w_time.append((task_to_remove, rejected_time))
            self.phone.tasks.remove(task_to_remove)

    # We can't subscribe until we've connected, so we use a callback handler
    # to subscribe when able
    def connect_handler(self, data):
        """pusher connection callback
        Args:
            data (str): should contain socket_id
        Returns:
            None
        """
        channels = [
            "outage-presence",
            "presence-user-" + str(self.phone.user_id),
            "private-phone-user-" + str(self.phone.user_id),
            "private-phone-company-" + str(self.phone.company_id),
        ]

        for channel_name in channels:
            channel = self.pusher.subscribe(channel_name)
            if channel_name == "private-phone-user-" + str(self.phone.user_id):
                channel.bind("incoming_call", self._channel_incoming_call)
                channel.bind(
                    "incoming_call_rejected", self._channel_incoming_call_rejected
                )

    def pusher_connect(self):
        """pusher connection
        Args:
            None
        Returns:
            None
        """
        self.pusher = pysher.Pusher(
            APPKEY,
            log_level=logging.WARNING,
            secret=SECRET,
            user_data={"user_id": self.phone.user_id},
        )
        self.pusher.connection.bind(
            "pusher:connection_established", self.connect_handler
        )
        self.pusher.connect()
