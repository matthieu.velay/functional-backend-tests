"""
Gather utilities functions
"""

import json
import os
import datetime

from multiprocessing.dummy import Pool

from cryptography.fernet import Fernet

KEY = Fernet(os.environ["FERNET_KEY"].encode())


def is_in_range(value, expected, percentage):
    """
    Check if value is in expected +- percentage range
    """
    return (1 - percentage) * expected < value < (1 + percentage) * expected


def multithreaded(len_pool, args, func):
    """
    Multithreaded function
    """
    pool = Pool(len_pool)
    if isinstance(args[0], tuple):
        responses = pool.starmap(func, args)
    else:
        responses = pool.map(func, args)
    pool.close()
    pool.join()

    return responses


def add_new_users_encrypted(environment, email, password):
    """
    Function to add a new to credentials -
    """
    if environment == "staging":
        creds_file = "../../../json_store/Credentials"
    elif environment == "production":
        creds_file = "../../../json_store/Credentials-Prod"
    else:
        raise Exception("env must be staging or production")
    with open(creds_file, "r") as file:
        credentials = file.read().encode()

    credentials = json.loads(KEY.decrypt(credentials))
    credentials[email] = password

    credentials_dumps = json.dumps(credentials)
    message = credentials_dumps.encode()
    encrypted = KEY.encrypt(message)

    with open(creds_file, "w") as file:
        file.write(encrypted.decode())


def compute_gap_between_times(timestamps):
    """
    Function that returns list of gap between consecutive timestamps
    """
    gaps = []
    gaps.append(timestamps[1] - timestamps[0])
    for index in range(2, len(timestamps)):
        gaps.append(timestamps[index] - timestamps[index - 1])

    return gaps


def get_year():
    year = datetime.date.today().year
    return year


def get_month():
    month = datetime.date.today().month
    return month
