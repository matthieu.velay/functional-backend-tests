import json
import logging

import requests

from .pusher import Pusher
from .twilio import twilio

log = logging.getLogger("PHONE")

# TODO: optional base url stag - prod
BASE_URL = "https://phone.aircall-staging.com"

session = requests.Session()


class Phone(object):
    def __init__(self, email, password):
        """
        __init__ method need to be documented
        """

        self.base_url = BASE_URL
        self.headers = {"Content-Type": "application/json", "User-Agent": "Mozilla/5.0"}

        # Initialize during incoming call
        self.tasks = []
        self.task_id = None
        self.received = []
        self.rejected = []
        self.received_w_time = []
        self.rejected_w_time = []

        # Initialize in login
        self.user_id = None
        self.company_id = None
        self.auth_token = None

        # Pusher availability
        self.pusher = None

        self.email = None
        self.password = None
        self.login(email, password)

        self.pusher = Pusher(self)
        self.pusher.pusher_connect()

    def login(self, email, password):
        """Login through API"""
        url = self.base_url + "/users/sign_in.json"
        payload = {"user": {"email": email, "password": password}}

        response = session.post(
            url, headers=self.headers, data=json.dumps(payload)
        ).json()

        if "success" in response:
            self.email = email
            self.password = password
            self.user_id = response["resource"]["id"]
            self.company_id = response["resource"]["company_id"]
            self.auth_token = response.get("access_token")
            log.info("User {0} login".format(self.user_id))
        else:
            log.info(response)
            raise Exception("User {0} can't login".format(email))
        return response

    def logout(self):
        """Logout through API, clean all data
        Args:
            None

        Returns:
            response (dict): response payload
        """

        url = self.base_url + "/users/sign_out.json"
        payload = {}
        headers = {"Content-Type": "application/json", "User-Agent": "Mozilla/5.0"}
        response = session.post(url, headers=headers, data=json.dumps(payload))
        log.info(
            "User {0} logout with response = {1}".format(
                self.user_id, response.status_code
            )
        )

        if response.status_code == 204:
            self.set_availability(False)
            self.email = None
            self.password = None
            self.tasks = []
            self.task_id = None
            self.received = []
            self.rejected = []
            self.received_w_time = []
            self.rejected_w_time = []
            self.user_id = None
            self.company_id = None
            self.auth_token = None
            self.pusher = None

        return response.status_code

    def set_availability(self, available):
        """Set pulse availability

        Args:
            available (bool)

        Returns:
            response (dict): response payload
        """
        url = self.base_url + "/v3/users/" + str(self.user_id)
        headers = self.headers
        headers["authorization"] = "Bearer " + self.auth_token
        data = {"user": {"state": "always_opened" if available else "always_closed"}}
        response = session.put(url, headers=headers, data=json.dumps(data))

        if available and response.status_code == 200:
            log.info("User {0} is available".format(self.user_id))
        else:
            log.info("User {0} is unavailable".format(self.user_id))

        return response.json()

    def set_wrap_up_time(self, wrap_up_time):
        url = self.base_url + "/v3/users/" + str(self.user_id)
        payload = {"user": {"wrap_up_time": wrap_up_time}}

        log.info(
            "User {0} set wrap_up to {1} seconds".format(self.user_id, wrap_up_time)
        )
        headers = self.headers
        headers["authorization"] = "Bearer " + self.auth_token

        response = session.put(url, headers=self.headers, data=json.dumps(payload))
        log.debug(response.json())
        return response.status_code

    def get_user_infos(self):
        url = self.base_url + "/v3/users/" + str(self.user_id)

        headers = self.headers
        headers["authorization"] = "Bearer " + self.auth_token
        response = session.get(url, headers=headers)

        return response.json()

    def get_availability(self):
        url = (
            self.base_url
            + "/v3/users/"
            + str(self.user_id)
            + "/availabilities?rules[]=phone_v3"
        )
        headers = self.headers
        headers["authorization"] = "Bearer " + self.auth_token

        response = session.get(url, headers=headers)
        return response.json()

    def answer_call(self, task_id):
        """Answer the call
        Args:
            task_id (str): task id, should be like "CAXXXXXXXXXXXXX"
        Returns:
            response (dict): response payload
        """
        url = "{0}/v4/incoming_calls/{1}/accept/desktop?agent_id={2}&company_id={3}".format(
            self.base_url, task_id, self.user_id, self.company_id
        )

        log.info(
            "User {0} answering the call uuid : {1}".format(self.user_id, self.task_id)
        )

        headers = self.headers
        headers["authorization"] = "Bearer " + self.auth_token
        response = requests.patch(url, headers=headers)
        log.info(response.json())

        return response.status_code

    def reject_call(self, task_id):
        """Answer the call
        Args:
            task_id (str): task id, should be like "CAXXXXXXXXXXXXX"
        Returns:
            response (dict): response payload
        """
        url = "{0}/v4/incoming_calls/{1}/reject/desktop?reason=agent_declined&agent_id={2}&company_id={3}".format(
            self.base_url, task_id, self.user_id, self.company_id
        )

        log.info("User {0} declines the call: {1}".format(self.user_id, self.task_id))

        headers = self.headers
        headers["authorization"] = "Bearer " + self.auth_token

        response = requests.patch(url, headers=headers)
        return response.status_code

    def get_call(self, call_uuid):
        """Get the call info
        Args:
            call_uuid (str): call ID, like "CAXXXXXXXXXXXXX"
        Returns:
            response (dict): response payload
        """
        url = "{0}/v3/calls?category=inbox&per_page=10".format(self.base_url)

        log.debug("Get call info: {0}".format(call_uuid))
        headers = self.headers
        headers["authorization"] = "Bearer " + self.auth_token

        response = requests.get(url, headers=headers)
        calls = response.json()
        call_info = [
            call for call in calls.get("calls") if call.get("call_uuid") == call_uuid
        ]
        if call_info:
            log.info("Call info:\n{0}\n".format(call_info[0]))
            return call_info[0]
        else:
            return {}
