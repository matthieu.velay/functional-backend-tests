import boto3
import logging
import os

log = logging.getLogger("AWS4AUTH")

# TODO: create a conftest to take into account env
# maybe a json with cluster name, domain name and db name
CLUSTER_ARN = os.getenv("CLUSTER_ARN")
SECRET_NAME = os.getenv("SECRET_NAME")


class _AWSSecretManager:
    def __init__(self):
        """
        __init__ method need to be documented
        """
        self.client = boto3.client(
            service_name="secretsmanager",
        )

    def get_secret(self, secret_name):
        try:
            secret_response = self.client.get_secret_value(SecretId=secret_name)
            return secret_response
        except Exception:
            log.error("Something wrong happens during get secret")
            raise Exception


class RDS:
    def __init__(self):
        """
        __init__ method need to be documented
        """
        self.client = boto3.client("rds-data")
        self.cluster_arn = CLUSTER_ARN
        self.secret_arn = _AWSSecretManager().get_secret(SECRET_NAME)["ARN"]
        self.database = "numbers_management"

    def execute_statement(self, sql_request):
        """
        Runs a SQL statement against a database
        Documentation : https://boto3.amazonaws.com/v1/documentation/api/latest/
                        reference/services/rds-data.html#RDSDataService.Client.execute_statement
        @:return: list of JSON without column meta data
        @information: Please put inside documentation part the column meta data related
        """
        response = self.client.execute_statement(
            database="numbers_management",
            includeResultMetadata=True,
            resourceArn=self.cluster_arn,
            secretArn=self.secret_arn,
            sql=sql_request,
        )

        # TODO: reformat the result of records because the column doesnt appears
        # >>> res = [ sub['name'] for sub in test.get("columnMetadata") ]
        return response["records"]
