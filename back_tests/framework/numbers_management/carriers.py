import logging
import requests
from .AWS_authentification import AWSRequestsAuth
from .RDS_aurora import RDS

log = logging.getLogger("CARRIERS")


class Carriers(AWSRequestsAuth, RDS):
    instance = None

    @classmethod
    def get_instance(cls):
        """This method
        :return: class instance
        """
        if cls.instance is None:
            cls.instance = Carriers()
        return cls.instance

    def __init__(self):
        """
        __init__ method need to be documented
        """
        AWSRequestsAuth.__init__(self)
        RDS.__init__(self)

        self.url = "https://" + self.domain_name + "/v1/carriers/"

    def carriers_create(self, payload):
        url_create = self.url + "create"
        response = requests.post(url_create, json=payload, auth=self.auth)
        return response

    def carriers_update(self, carriers_id, payload):
        url_update = self.url + carriers_id + "/update"
        response = requests.put(url_update, json=payload, auth=self.auth)
        return response

    def carriers_get_by_name(self, name):
        sql_request = "SELECT * FROM carriers WHERE name='{}'".format(name)
        return self.execute_statement(sql_request)

    def carriers_delete_by_name(self, name):
        sql_request = "DELETE FROM carriers WHERE name='{}'".format(name)
        return self.execute_statement(sql_request)

    def carriers_numbers_offerings_create(self, carriers_id, payload):
        url_create = self.url + carriers_id + "/numbers_offerings/create"
        response = requests.post(url_create, json=payload, auth=self.auth)
        return response

    def carriers_numbers_offerings_update(self, carriers_id, payload):
        url_update = self.url + carriers_id + "/numbers_offerings/update"
        response = requests.post(url_update, json=payload, auth=self.auth)
        return response

    def carriers_add_regions_params(self, carriers_id, payload):
        url_update = self.url + carriers_id + "/regions"
        response = requests.post(url_update, json=payload, auth=self.auth)
        return response

    def carriers_update_regions_params(self, carriers_id, payload):
        url_update = self.url + carriers_id + "/regions/update"
        response = requests.put(url_update, json=payload, auth=self.auth)
        return response

    def carriers_get_link_area_code(self, carrier_uuid, region_uuid):
        """
        Get carriers followed information/ColumnMetadata :
            - Id of carriers_capabilities
            - enabled
            - supportRequired
            - carrierId
            - regionId
            - capability

        @:return: a list of JSON
        @example: [[{'longValue': 283}, {'booleanValue': False}, {'booleanValue': True},
                    {'longValue': 519}, {'longValue': 5}, {'stringValue': 'sms'}]]
        """
        sql_request = (
            "select cr.*,crp.capability from carrier_regions cr "
            "left join regions r on r.id = cr.region_id "
            "left join carriers c on c.id = cr.carrier_id "
            "left join carrier_region_capabilities crp on crp.carrier_region_id = cr.id "
            "where c.carrier_uuid = '{carrier_uuid}' and r.region_uuid = '{region_uuid}';".format(
                carrier_uuid=carrier_uuid, region_uuid=region_uuid
            )
        )
        return self.execute_statement(sql_request)

    def carriers_remove_regions_params(self, carriers_id, payload):
        url_delete = self.url + carriers_id + "/regions/delete"
        response = requests.post(url_delete, json=payload, auth=self.auth)
        return response

    def carriers_specify_outbound_traffic(self, carriers_id, payload):
        url_update = self.url + carriers_id + "/outbound"
        response = requests.post(url_update, json=payload, auth=self.auth)
        return response

    def carriers_get_outbound_traffic(self, carrier_uuid, country_uuid):
        """
        Get carriers followed information/ColumnMetadata :
            - Id of carrier_country_outbounds
            - carrier_id
            - enabled
            - country_id
            - created_at
            - updated_at
            - capability

        @:return: a list of JSON
        @example: [[{'longValue': 53}, {'longValue': 760}, {'booleanValue': True}, {'longValue': 1},
                    {'stringValue': '2021-04-29 14:10:24.344842'}, {'stringValue': '2021-04-29 14:10:24.344842'},
                    {'stringValue': 'sms'}]]
        """
        sql_request = (
            "select cco.*,ccoc.capability from carrier_country_outbounds cco "
            "left join countries cou on cou.id = cco.country_id "
            "left join carriers car on car.id = cco.carrier_id "
            "left join carrier_country_outbound_capabilities ccoc on cco.id = ccoc.carrier_country_outbound_id "
            "where car.carrier_uuid = '{carrier_uuid}' and cou.country_uuid = '{country_uuid}';".format(
                carrier_uuid=carrier_uuid, country_uuid=country_uuid
            )
        )
        return self.execute_statement(sql_request)

    def carriers_create_inbound_services_countries(
        self, carriers_id, country_id, payload
    ):
        url_update = (
            self.url + carriers_id + "/countries/" + country_id + "/inbound_services"
        )
        response = requests.post(url_update, json=payload, auth=self.auth)
        return response

    def carriers_get_inbound_services(self, carrier_uuid, country_uuid):
        """
        Get carriers followed information/ColumnMetadata :
            - Id of carrier_country_inbound_services
            - carrier_id
            - country_id
            - service
            - created_at

        @:return: a list of JSON
        @example: [[{'longValue': 40}, {'longValue': 103}, {'longValue': 1}, {'stringValue': 'cnam'},
                    {'stringValue': '2021-05-04 11:56:21.399342'}, {'stringValue': 'cnam'}]]

        """
        sql_request = (
            "select ccis.*,cciss.service from carrier_country_inbound_services ccis "
            "left join countries cou on cou.id = ccis.country_id "
            "left join carriers car on car.id = ccis.carrier_id "
            "left join carrier_country_inbound_services cciss on ccis.id = cciss.id "
            "where car.carrier_uuid = '{carrier_uuid}' and cou.country_uuid = '{country_uuid}';".format(
                carrier_uuid=carrier_uuid, country_uuid=country_uuid
            )
        )
        return self.execute_statement(sql_request)

    def carriers_remove_outbound_traffic(self, carriers_id, payload):
        url_delete = self.url + carriers_id + "/outbound/delete"
        response = requests.post(url_delete, json=payload, auth=self.auth)
        return response

    def carriers_delete_inbound_services_countries(self, carriers_id, country_id):
        url_delete = (
            self.url
            + carriers_id
            + "/countries/"
            + country_id
            + "/inbound_services/delete"
        )
        response = requests.delete(url_delete, auth=self.auth)
        return response


carriers_client = Carriers.get_instance()
