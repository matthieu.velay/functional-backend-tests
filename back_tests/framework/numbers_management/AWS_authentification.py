import logging
import os
from aws_requests_auth.boto_utils import BotoAWSRequestsAuth

log = logging.getLogger("AWS4AUTH")


class AWSRequestsAuth:
    def __init__(self):
        """
        __init__ method need to be documented
        """
        self.domain_name = os.getenv("DOMAIN_NAME")
        self.auth = BotoAWSRequestsAuth(
            aws_host=self.domain_name, aws_region="us-west-2", aws_service="execute-api"
        )
