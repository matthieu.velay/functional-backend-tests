import logging
import requests
from .AWS_authentification import AWSRequestsAuth
from .RDS_aurora import RDS

log = logging.getLogger("COUNTRIES")


class Countries(AWSRequestsAuth, RDS):
    instance = None

    @classmethod
    def get_instance(cls):
        """This method
        :return: class instance
        """
        if cls.instance is None:
            cls.instance = Countries()
        return cls.instance

    def __init__(self):
        """
        __init__ method need to be documented
        """
        AWSRequestsAuth.__init__(self)
        RDS.__init__(self)
        self.url = "https://" + self.domain_name + "/v1/countries/"

    def countries_get_by_id(self, countries_id):

        url_create = self.url + countries_id
        response = requests.get(url_create, auth=self.auth)
        return response

    def countries_license_update(self, countries_id, payload):

        url_create = self.url + countries_id + "/license"

        response = requests.put(url_create, json=payload, auth=self.auth)
        return response

    def countries_get_by_alpha3_code(self, countries_alpha3):
        """
        [['stringValue': '600ebc3b-9d2f-41c9-809b-3a23f703603c'}]]
        """

        sql_request = (
            "SELECT countries.country_uuid FROM countries WHERE alpha3='{}'".format(
                countries_alpha3
            )
        )
        return self.execute_statement(sql_request)

    def countries_get_related_area_code(self, countries_alpha3, limit=10):
        sql_request = (
            "SELECT regions.region_uuid FROM regions "
            "INNER JOIN countries on regions.country_id = countries.id "
            "WHERE countries.alpha3 = '{countries_alpha3}' LIMIT {limit}".format(
                countries_alpha3=countries_alpha3, limit=limit
            )
        )
        return self.execute_statement(sql_request)

    def countries_get_random_code_uuid(self):
        """GET A RANDOM COUNTRY CODE UUID"""
        sql_request = (
            "SELECT countries.country_uuid FROM countries order by random() limit 1"
        )
        return self.execute_statement(sql_request)


countries_client = Countries.get_instance()
