from twilio.request_validator import RequestValidator
import requests
import os
import logging

log = logging.getLogger("TwilioUpdateStatus")

session = requests.Session()


class MessagingUpdateStatus(object):
    def __init__(self, to, from_, messagestatus, messageid, messagesid):
        """
        __init__ method need to be documented
        """
        # Initialize
        self.resp = None
        # constractor
        self.update_sms_status_twilio(to, from_, messagestatus, messageid, messagesid)

    def update_sms_status_twilio(self, to, from_, messagestatus, messageid, messagesid):
        # Your Auth Token from twilio.com/user/account saved as an environment variable
        # Remember never to hard code your auth token in code, browser Javascript, or distribute it in mobile apps
        auth_token = os.getenv("TWILIO_TOKEN")
        twilio_sid = os.getenv("TWILIO_SID")
        validator = RequestValidator(auth_token)
        # Replace this URL with your unique URL
        domain_name = "messaging.aircall-staging.com"

        request_parameters = {
            "AccountSid": twilio_sid,
            "ApiVersion": "2010-04-01",
            "From": from_,
            "messageId": messageid,
            "MessageSid": messagesid,
            "MessageStatus": messagestatus,
            "SmsSid": messagesid,
            "SmsStatus": messagestatus,
            "To": to,
        }

        # ************* SEND THE REQUEST *************
        endpoint = (
            "https://" + domain_name + "/v1/messages/" + messageid + "/status/twilio"
        )
        print("\nBEGIN REQUEST++++++++++++++++++++++++++++++++++++")
        log.info("URL = {0}".format(endpoint))
        log.info("Payload = {0}".format(request_parameters))

        # Need to send it as json for setting content-type as application/json
        signature = validator.compute_signature(endpoint, request_parameters)
        signature_check = validator.validate(endpoint, request_parameters, signature)
        log.info("Signature check: {0}".format(signature_check))
        headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "X-Twilio-Signature": signature,
        }
        r = requests.post(endpoint, headers=headers, data=request_parameters)

        print("\nRESPONSE++++++++++++++++++++++++++++++++++++")
        log.info("Response code: {0}".format(r.status_code))
        log.info("Twilio Signature: {0}".format(signature))
        self.resp = r.status_code
        log.info("Headers: {0}".format(headers))
        log.info("response = {0}".format(self.resp))
        return self.resp
