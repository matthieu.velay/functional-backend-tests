import json
import logging
import os
import time
import string

import requests

log = logging.getLogger("MESSAGING")

# TODO: optional base url stag - prod
BASE_URL = "https://phone.aircall-staging.com"
GRAPHQL_URL = os.getenv("GRAPHQL_URL")

session = requests.Session()


class Messaging(object):
    def __init__(self, auth_token):
        """
        __init__ method need to be documented
        """
        self.base_url = BASE_URL
        self.graphql_url = GRAPHQL_URL
        self.headers = {"Content-Type": "application/json", "User-Agent": "Mozilla/5.0"}

        # Initialize
        self.cognito_token = None
        self.conversation_id = None
        self.error_type = None
        self.error_message = None
        self.client_error_type = None
        self.client_error_message = None
        self.huge_string = None
        self.message_status = None
        self.message_id = None
        self.history_id = None
        self.retrieve_message_id = None
        self.query_message_id = None
        self.line_id = None
        self.phone_number = None
        self.update_query_message_id = None
        self.update_line_id = None
        self.update_phone_number = None
        self.ack_conversation_id = None
        self.ack_message_id_before = None
        self.unread_conversation_num_before = None
        self.unread_message_num_before = None
        self.unread_message_direction_before = None
        self.ack_message_id = None
        self.unread_conversation_num = None
        self.unread_message_num = None
        self.unread_message_direction = None

        self.catch_cognito_token(auth_token)

    def catch_cognito_token(self, auth_token):

        url = self.base_url + "/auth/v0/tokens"
        headers = self.headers
        headers["authorization"] = "Bearer " + auth_token
        response = session.get(url, headers=self.headers).json()

        if "accessToken" in response:
            self.cognito_token = response.get("idToken")
        else:
            raise Exception("Cognito Token Fail")
        return response

    def sendMessage(self, externalNumber, lineID, Body):

        url = self.graphql_url
        headers = self.headers
        headers["authorization"] = self.cognito_token
        payload = {
            "query": "mutation sendMessage($input: SendMessageInput!) \n{sendMessage(input: $input) {\n   lineID\n   messageID\n   status\n   conversationID\n   error{\n   type\n  message\n}\n}\n}",
            "variables": {
                "input": {
                    "externalNumber": externalNumber,
                    "lineID": lineID,
                    "text": Body,
                }
            },
        }
        response = session.post(url, headers=headers, data=json.dumps(payload))
        json_response_sms = response.json()
        log.info("Response is: {0}".format(json_response_sms))

        if response.status_code == 200:

            if (
                json_response_sms["data"] is not None
                and "conversationID" in json_response_sms["data"]["sendMessage"]
            ):
                response_with_data_dict = json_response_sms["data"]["sendMessage"]
                if response_with_data_dict["conversationID"] is not None:
                    self.conversation_id = response_with_data_dict["conversationID"]
                    self.message_id = response_with_data_dict["messageID"]
                    log.info("ConverstaionID {0}".format(self.conversation_id))
                    log.info("MessageID {0}".format(self.message_id))

                elif "error" in response_with_data_dict:
                    self.client_error_type = response_with_data_dict["error"]["type"]
                    self.client_error_message = response_with_data_dict["error"][
                        "message"
                    ]
                    log.info("Client errorType: {0}".format(self.client_error_type))
                    log.info(
                        "Client error message : {0}".format(self.client_error_message)
                    )

            elif "errorType" in json_response_sms["errors"][0]:
                response_error_dict = json_response_sms["errors"][0]
                self.error_type = response_error_dict["errorType"]
                self.error_message = response_error_dict["message"]
                log.info("errorType: {0}".format(self.error_type))
                log.info("error message : {0}".format(self.error_message))

            else:
                raise Exception("Send SMS Fail")

        else:
            raise Exception("Send SMS Fail")
        return json_response_sms

    def getConversation(
        self, exp_messageid, conversation_id, exp_status, timeout, loop_timeout=1
    ):

        start = time.time()
        while time.time() - start < timeout and (
            self.retrieve_message_id != exp_messageid
            or self.message_status != exp_status
        ):
            time.sleep(loop_timeout)
            url = self.graphql_url
            headers = self.headers
            headers["authorization"] = self.cognito_token
            payload = {
                "query": "query GetConversation($conversationID: ID!, $limit: Int, $nextToken: ID)"
                "{\n  conversation: getConversation(ID: $conversationID)"
                " {\n   ID\n    company {\n      ID\n      name\n    }\n "
                "line {\n      ID\n      name\n    }\n    externalLine {\n      phoneNumber\n    }\n "
                "listMessages(limit: $limit, nextToken: $nextToken)"
                "{\n      messages {\n       text\n       status\n       ID\n      }\n "
                "pageInfo {\n        currentToken\n        nextToken\n      }\n    }\n    createdAt\n    updatedAt\n  }\n}",
                "variables": {"conversationID": conversation_id},
            }
            response = session.post(url, headers=headers, data=json.dumps(payload))
            json_response_retrieve_sms = response.json()
            self.message_status = json_response_retrieve_sms["data"]["conversation"][
                "listMessages"
            ]["messages"][0]["status"]
            self.retrieve_message_id = json_response_retrieve_sms["data"][
                "conversation"
            ]["listMessages"]["messages"][0]["ID"]
            if (
                self.message_status == exp_status
                and self.retrieve_message_id == exp_messageid
            ):
                log.info("SMS Status is {0}".format(self.message_status))
                log.info("Retrieve Message ID is {0}".format(self.retrieve_message_id))
            else:
                log.info("Entered Loop again to confirm message status and message id")
                log.debug("Response : {0}".format(json_response_retrieve_sms))
            time.sleep(loop_timeout)
        return json_response_retrieve_sms

    # Messaging End Point return only conversation ID not Message ID that why I seperate this from getConversation
    # for decreasing the sleep time and saving some time for other tests
    def getConversationMessagingEndpoint(
        self, conversation_id, exp_status, timeout, loop_timeout=2
    ):

        start = time.time()
        while time.time() - start < timeout and self.message_status != exp_status:
            time.sleep(loop_timeout)
            url = self.graphql_url
            headers = self.headers
            headers["authorization"] = self.cognito_token
            payload = {
                "query": "query GetConversation($conversationID: ID!, $limit: Int, $nextToken: ID)"
                "{\n  conversation: getConversation(ID: $conversationID)"
                " {\n   ID\n    company {\n      ID\n      name\n    }\n "
                "line {\n      ID\n      name\n    }\n    externalLine {\n      phoneNumber\n    }\n "
                "listMessages(limit: $limit, nextToken: $nextToken)"
                "{\n      messages {\n       text\n       status\n       ID\n      }\n "
                "pageInfo {\n        currentToken\n        nextToken\n      }\n    }\n    createdAt\n    updatedAt\n  }\n}",
                "variables": {"conversationID": conversation_id},
            }
            response = session.post(url, headers=headers, data=json.dumps(payload))
            json_response_retrieve_sms = response.json()
            log.debug("Response : {0}".format(json_response_retrieve_sms))
            self.message_status = json_response_retrieve_sms["data"]["conversation"][
                "listMessages"
            ]["messages"][0]["status"]
            self.retrieve_message_id = json_response_retrieve_sms["data"][
                "conversation"
            ]["listMessages"]["messages"][0]["ID"]
            if self.message_status == exp_status:
                log.info("SMS Status is {0}".format(self.message_status))
                log.info("Retrieve Message ID is {0}".format(self.retrieve_message_id))
            else:
                log.info("Entered Loop again to confirm message status")
                log.debug("Response : {0}".format(json_response_retrieve_sms))
            time.sleep(loop_timeout)
        return json_response_retrieve_sms

    def getListConversationsQuery(
        self,
        query_size,
        exp_messageid,
        timeout,
        loop_timeout=1,
    ):

        start = time.time()
        while time.time() - start < timeout and self.history_id != exp_messageid:
            url = self.graphql_url
            headers = self.headers
            headers["authorization"] = self.cognito_token
            payload = {
                "query": "query GetListConversationsQuery($limit: Int, $nextToken: ID)"
                "{\n  listConversations(limit: $limit, nextToken: $nextToken)"
                "{\n  conversations\n  {\n  listMessages(limit: $limit, nextToken: $nextToken)"
                "{\n  messages{\n  text\n  status\n  ID\n  }\n  }\n  }\n  }\n  }",
                "variables": {"limit": query_size},
            }
            response = session.post(url, headers=headers, data=json.dumps(payload))
            json_response_query = response.json()
            self.history_id = json_response_query["data"]["listConversations"][
                "conversations"
            ][0]["listMessages"]["messages"][0]["ID"]
            if self.history_id == exp_messageid:
                log.info("SMS history ID {0}".format(self.history_id))
            else:
                log.info("Entered Loop again to confirm message history id")
                log.debug("Response : {0}".format(json_response_query))
            time.sleep(loop_timeout)
        return json_response_query

    def getConversationByLineAndPhoneNumber(
        self,
        lineID,
        phonenumber,
        exp_messageid,
        timeout,
        loop_timeout=2,
    ):

        start = time.time()
        while time.time() - start < timeout and self.query_message_id != exp_messageid:
            url = self.graphql_url
            headers = self.headers
            headers["authorization"] = self.cognito_token
            payload = {
                "query": "query GetConversationByLineAndPhoneNumber($lineID: ID!, $externalNumber: String!, $nextToken: ID, $limit: Int)    \n"
                "{\n  getConversationByLineAndExternal(lineID: $lineID, externalNumber: $externalNumber)"
                "{\n    ID\n     status\n   line {\n    ID\n   name\n countryISOCode\n } \n externalLine"
                "{\n    phoneNumber \ncontact {\n   fullName\n   companyName\n}\n}"
                "\n createdAt \n    updatedAt \n    listMessages  (limit: $limit, nextToken: $nextToken)"
                "{messages {\n  ID \n   text \n direction \n    agent {\n   fullName\n} \n  hasUnsupportedContent \n    status \n   createdAt\n}"
                "pageInfo {\n nextToken\n}\n    }\n }\n }",
                "variables": {"lineID": lineID, "externalNumber": phonenumber},
            }
            response = session.post(url, headers=headers, data=json.dumps(payload))
            json_response_query = response.json()

            self.query_message_id = json_response_query["data"][
                "getConversationByLineAndExternal"
            ]["listMessages"]["messages"][0]["ID"]
            self.line_id = json_response_query["data"][
                "getConversationByLineAndExternal"
            ]["line"]["ID"]
            self.phone_number = json_response_query["data"][
                "getConversationByLineAndExternal"
            ]["externalLine"]["phoneNumber"]
            if self.query_message_id == exp_messageid:
                log.info(
                    "SMS ID query with line&number: {0}".format(self.query_message_id)
                )
                log.info("Line ID query with line&number: {0}".format(self.line_id))
                log.info(
                    "Phone Number query with line&number: {0}".format(self.phone_number)
                )
            else:
                log.info("Entered Loop again to confirm message id")
                log.debug("Response : {0}".format(json_response_query))
            time.sleep(loop_timeout)
        return json_response_query

    def getMessageId(
        self,
        get_lineID,
        get_phonenumber,
    ):

        url = self.graphql_url
        headers = self.headers
        headers["authorization"] = self.cognito_token
        payload = {
            "query": "query GetConversationByLineAndPhoneNumber($lineID: ID!, $externalNumber: String!, $nextToken: ID, $limit: Int)    \n"
            "{\n  getConversationByLineAndExternal(lineID: $lineID, externalNumber: $externalNumber)"
            "{\n    ID\n     status\n   line {\n    ID\n   name\n countryISOCode\n } \n externalLine"
            "{\n    phoneNumber \ncontact {\n   fullName\n   companyName\n}\n}"
            "\n createdAt \n    updatedAt \n    listMessages  (limit: $limit, nextToken: $nextToken)"
            "{messages {\n  ID \n   text \n direction \n    agent {\n   fullName\n} \n  hasUnsupportedContent \n    status \n   createdAt\n}"
            "pageInfo {\n nextToken\n}\n    }\n }\n }",
            "variables": {"lineID": get_lineID, "externalNumber": get_phonenumber},
        }
        response = session.post(url, headers=headers, data=json.dumps(payload))
        json_response_query = response.json()
        log.info("Response: {0}".format(json_response_query))
        self.update_query_message_id = json_response_query["data"][
            "getConversationByLineAndExternal"
        ]["listMessages"]["messages"][0]["ID"]
        self.update_line_id = json_response_query["data"][
            "getConversationByLineAndExternal"
        ]["line"]["ID"]
        self.update_phone_number = json_response_query["data"][
            "getConversationByLineAndExternal"
        ]["externalLine"]["phoneNumber"]
        log.debug("Response : {0}".format(json_response_query))
        log.info(
            "SMS ID query with line&number: {0}".format(self.update_query_message_id)
        )
        log.info("Line ID query with line&number: {0}".format(self.update_line_id))
        log.info(
            "Phone Number query with line&number: {0}".format(self.update_phone_number)
        )
        return json_response_query

    def listUnReadConversations(
        self,
        query_size,
        exp_messageid,
        timeout,
        loop_timeout=1,
    ):

        start = time.time()
        while (
            time.time() - start < timeout
            and self.ack_message_id_before != exp_messageid
        ):
            url = self.graphql_url
            headers = self.headers
            headers["authorization"] = self.cognito_token
            payload = {
                "query": "query listConversations($nextToken: ID, $limit: Int)"
                "{\n  listConversations(nextToken: $nextToken, limit: $limit)"
                "{\n  unreadConversationsCount  \n  conversations\n  {\n  unreadMessagesCount  \n  listMessages(limit: $limit, nextToken: $nextToken)"
                "{\n  messages{\n  text\n  direction\n  status\n  ID\n  }\n  }\n  }\n  }\n  }",
                "variables": {"limit": query_size},
            }
            response = session.post(url, headers=headers, data=json.dumps(payload))
            json_response_ack_unread_before = response.json()
            self.ack_message_id_before = json_response_ack_unread_before["data"][
                "listConversations"
            ]["conversations"][0]["listMessages"]["messages"][0]["ID"]
            self.unread_conversation_num_before = json_response_ack_unread_before[
                "data"
            ]["listConversations"]["unreadConversationsCount"]
            self.unread_message_num_before = json_response_ack_unread_before["data"][
                "listConversations"
            ]["conversations"][0]["unreadMessagesCount"]
            self.unread_message_direction_before = json_response_ack_unread_before[
                "data"
            ]["listConversations"]["conversations"][0]["listMessages"]["messages"][0][
                "direction"
            ]
            if self.ack_message_id_before == exp_messageid:
                log.info("SMS history ID {0}".format(self.ack_message_id_before))
                log.info(
                    "Unread conversation count for the agent before ACK: {0}".format(
                        self.unread_conversation_num_before
                    )
                )
                log.info(
                    "Unread message count for the agent before ACK: {0}".format(
                        self.unread_message_num_before
                    )
                )
            else:
                log.info("Entered Loop again to confirm received messageID")
                log.debug("Response : {0}".format(json_response_ack_unread_before))
            time.sleep(loop_timeout)
        return json_response_ack_unread_before

    def listReadConversations(
        self,
        query_size,
        exp_messageid,
        exp_message_num,
        timeout,
        loop_timeout=1,
    ):

        start = time.time()
        while time.time() - start < timeout and (
            self.ack_message_id != exp_messageid
            or self.unread_message_num != exp_message_num
        ):
            url = self.graphql_url
            headers = self.headers
            headers["authorization"] = self.cognito_token
            payload = {
                "query": "query listConversations($nextToken: ID, $limit: Int)"
                "{\n  listConversations(nextToken: $nextToken, limit: $limit)"
                "{\n  unreadConversationsCount  \n  conversations\n  {\n  unreadMessagesCount  \n  listMessages(limit: $limit, nextToken: $nextToken)"
                "{\n  messages{\n  text\n  direction\n  status\n  ID\n  }\n  }\n  }\n  }\n  }",
                "variables": {"limit": query_size},
            }
            response = session.post(url, headers=headers, data=json.dumps(payload))
            json_response_ack_unread = response.json()
            self.ack_message_id = json_response_ack_unread["data"]["listConversations"][
                "conversations"
            ][0]["listMessages"]["messages"][0]["ID"]
            self.unread_conversation_num = json_response_ack_unread["data"][
                "listConversations"
            ]["unreadConversationsCount"]
            self.unread_message_num = json_response_ack_unread["data"][
                "listConversations"
            ]["conversations"][0]["unreadMessagesCount"]
            self.unread_message_direction = json_response_ack_unread["data"][
                "listConversations"
            ]["conversations"][0]["listMessages"]["messages"][0]["direction"]
            if self.unread_message_num == exp_message_num:
                log.info("SMS history ID {0}".format(self.ack_message_id))
                log.info(
                    "Unread conversation count for the agent after ACK: {0}".format(
                        self.unread_conversation_num
                    )
                )
                log.info(
                    "Unread message count for the agent after ACK: {0}".format(
                        self.unread_message_num
                    )
                )
            else:
                log.info("Wating for the ACK of Read message")
                log.debug("Response : {0}".format(json_response_ack_unread))
            time.sleep(loop_timeout)
        return json_response_ack_unread

    def acknowledgeMessage(self, conversationID, messageID):

        url = self.graphql_url
        headers = self.headers
        headers["authorization"] = self.cognito_token
        payload = {
            "query": "mutation readConversation($input: ReadConversationInput!)  \n{readConversation(input: $input)  {\n   conversationID}\n}",
            "variables": {
                "input": {
                    "conversationID": conversationID,
                    "messageID": messageID,
                }
            },
        }
        response = session.post(url, headers=headers, data=json.dumps(payload))
        json_response_ack = response.json()
        log.debug("Response is: {0}".format(json_response_ack))

        if response.status_code == 200:
            self.ack_conversation_id = json_response_ack["data"]["readConversation"][
                "conversationID"
            ]
            log.info(
                "Sucessfully acknowledge the message as read for {0}".format(
                    self.ack_conversation_id
                )
            )
        else:
            raise Exception("Can not acknowledge message as read")
        return self.ack_conversation_id
