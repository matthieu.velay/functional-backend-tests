import logging
import uuid
import requests
import os

from aws_requests_auth.boto_utils import BotoAWSRequestsAuth

log = logging.getLogger("AWS4AUTH")

MESSAGING_STAGING_DOMAIN_NAME = os.getenv("MESSAGING_STAGING_DOMAIN_NAME")
AWS_REGION = os.getenv("AWS_REGION")
AWS_SERVICE = os.getenv("AWS_SERVICE")

session = requests.Session()


class MessagingEndpoint(object):
    def __init__(self):
        """
        __init__ method need to be documented
        """
        # Initialize
        self.message_status = None
        self.conversationId = None
        self.jsonresp = None
        self.statuscode = None
        # set domain and auth
        self.domain_name = MESSAGING_STAGING_DOMAIN_NAME
        self.aws_region = AWS_REGION
        self.aws_service = AWS_SERVICE
        self.auth = BotoAWSRequestsAuth(
            aws_host=self.domain_name,
            aws_region=self.aws_region,
            aws_service=self.aws_service,
        )

    def send_sms_aws4(self, agentId, companyId, lineId, externalNumber, text):

        # Need to convert UUID to str to serialize it for Json
        # UUID = messageID
        message_id = str(uuid.uuid1())
        log.info("MessageID : {0}".format(message_id))

        request_parameters = {
            "messageId": message_id,
            "agentId": agentId,
            "companyId": companyId,
            "lineId": lineId,
            "externalNumber": externalNumber,
            "text": text,
        }

        # ************* SEND THE REQUEST *************
        endpoint = "https://" + self.domain_name + "/v1/messages/send"
        print("BEGIN SEND SMS REQUEST++++++++++++++++++++++++++++++++++++")
        log.info("URL = {0}".format(endpoint))
        log.info("Payload = {0}".format(str(request_parameters)))

        # Need to send it as json for setting content-type as application/json
        r = requests.post(endpoint, json=request_parameters, auth=self.auth)

        print("SEND SMS RESPONSE++++++++++++++++++++++++++++++++++++")
        log.info("Response code: {0}".format(r.status_code))
        self.message_status = r.status_code
        self.jsonresp = r.json()
        if self.message_status == 200:
            self.conversationId = self.jsonresp["conversationId"]
        log.info("response = {0}".format(self.jsonresp))
        return self.jsonresp

    def ack_sms_aws4(self, agentId, companyId, lineId, externalNumber, lastMessageId):

        request_parameters = {
            "agentId": agentId,
            "companyId": companyId,
            "lineId": lineId,
            "externalNumber": externalNumber,
            "lastMessageId": lastMessageId,
        }

        # ************* SEND THE REQUEST *************
        endpoint = "https://" + self.domain_name + "/v1/conversations/acknowledge"
        print("BEGIN ACKNOWLEDGE REQUEST++++++++++++++++++++++++++++++++++++")
        log.info("URL = {0}".format(endpoint))
        log.info("Payload = {0}".format(str(request_parameters)))

        # Need to send it as json for setting content-type as application/json
        r = requests.post(endpoint, json=request_parameters, auth=self.auth)

        print("ACKNOWLEDGE RESPONSE++++++++++++++++++++++++++++++++++++")
        log.info("Response code: {0}".format(r.status_code))
        self.statuscode = r.status_code
        if self.statuscode == 200:
            log.info("Sucessfully acknowledge the message as read")
        else:
            log.error("Can not acknowledge message as read")
        return

    def get_message_aws4(self, companyId, lineId, externalNumber):

        # ************* SEND THE REQUEST *************
        endpoint = (
            "https://"
            + self.domain_name
            + "/v1/companies/"
            + companyId
            + "/lines/"
            + lineId
            + "/conversations/"
            + externalNumber
            + "/messages"
        )
        print("GET MESSAGE++++++++++++++++++++++++++++++++++++")
        log.info("URL = {0}".format(endpoint))

        # Need to get response for Get Message
        r = requests.get(endpoint, auth=self.auth)

        print("GET MESSAGE RESPONSE++++++++++++++++++++++++++++++++++++")
        log.info("Response code: {0}".format(r.status_code))
        self.statuscode = r.status_code
        if self.statuscode == 200:
            log.info("Sucessfully get message")
            log.info("response = {0}".format(r.json()))
        else:
            log.error("Can not get message")
            log.info("response = {0}".format(r.json()))
        return r.json()

    def get_conversation_aws4(self, companyId, lineId, externalNumber, agentId):

        request_parameters = {"agentId": agentId}

        # ************* SEND THE REQUEST *************
        endpoint = (
            "https://"
            + self.domain_name
            + "/v1/companies/"
            + companyId
            + "/lines/"
            + lineId
            + "/conversations/"
            + externalNumber
        )
        print("GET CONVERSATION++++++++++++++++++++++++++++++++++++")
        log.info("URL = {0}".format(endpoint))

        # Need to get response for Get Conversation
        r = requests.get(endpoint, params=request_parameters, auth=self.auth)

        print("GET CONVERSATION RESPONSE++++++++++++++++++++++++++++++++++++")
        log.info("Response code: {0}".format(r.status_code))
        self.statuscode = r.status_code
        if self.statuscode == 200:
            log.info("Sucessfully get conversation")
            log.info("response = {0}".format(r.json()))
        else:
            log.error("Can not get conversation")
            log.info("response = {0}".format(r.json()))
        return r.json()

    def list_conversation_aws4(self, lineId, agentId):

        request_parameters = {
            "agentId": agentId,
            "lineId": lineId,
        }

        # ************* SEND THE REQUEST *************
        endpoint = "https://" + self.domain_name + "/v1/conversations"
        print("LIST CONVERSATION++++++++++++++++++++++++++++++++++++")
        log.info("URL = {0}".format(endpoint))

        # Need to get response for List Conversation
        r = requests.get(endpoint, params=request_parameters, auth=self.auth)

        print("LIST CONVERSATION RESPONSE++++++++++++++++++++++++++++++++++++")
        log.info("Response code: {0}".format(r.status_code))
        self.statuscode = r.status_code
        if self.statuscode == 200:
            log.info("Sucessfully list conversation")
            log.info("response = {0}".format(r.json()))
        else:
            log.error("Can not list conversation")
            log.info("response = {0}".format(r.json()))
        return r.json()
