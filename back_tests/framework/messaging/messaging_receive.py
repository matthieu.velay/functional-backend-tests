from twilio.request_validator import RequestValidator
import requests
import os
import logging

log = logging.getLogger("TwilioReceiveMessage")

session = requests.Session()


class MessagingReceive(object):
    def __init__(self, to, from_, body, messagesid, nummedia):
        """
        __init__ method need to be documented
        """
        # Initialize
        self.jsonresp = None
        self.statuscode = None
        # constractor
        self.receive_sms_twilio(to, from_, body, messagesid, nummedia)

    def receive_sms_twilio(self, to, from_, body, messagesid, nummedia):
        # Your Auth Token from twilio.com/user/account saved as an environment variable
        # Remember never to hard code your auth token in code, browser Javascript, or distribute it in mobile apps
        auth_token = os.getenv("TWILIO_TOKEN")
        twilio_sid = os.getenv("TWILIO_SID")
        validator = RequestValidator(auth_token)
        # Replace this URL with your unique URL
        domain_name = "messaging.aircall-staging.com"

        request_parameters = {
            "AccountSid": twilio_sid,
            "ApiVersion": "2010-04-01",
            "Body": body,
            "From": from_,
            "FromCity": "CULVER CITY",
            "FromCountry": "US",
            "FromState": "CA",
            "FromZip": "90293",
            "MessageSid": messagesid,
            "NumMedia": nummedia,
            "NumSegments": "1",
            "SmsMessageSid": messagesid,
            "SmsSid": messagesid,
            "SmsStatus": "received",
            "To": to,
            "ToCity": "",
            "ToCountry": "US",
            "ToState": "FL",
            "ToZip": "",
        }

        # ************* SEND THE REQUEST *************
        endpoint = "https://" + domain_name + "/v1/messages/receive/twilio"
        print("\nBEGIN REQUEST++++++++++++++++++++++++++++++++++++")
        log.info("URL = {0}".format(endpoint))
        log.info("Payload = {0}".format(request_parameters))

        # Need to send it as json for setting content-type as application/json
        signature = validator.compute_signature(endpoint, request_parameters)
        signature_check = validator.validate(endpoint, request_parameters, signature)
        log.info("Signature check: {0}".format(signature_check))
        headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "X-Twilio-Signature": signature,
        }
        r = requests.post(endpoint, headers=headers, data=request_parameters)

        print("\nRESPONSE++++++++++++++++++++++++++++++++++++")
        log.info("Response code: {0}".format(r.status_code))
        log.info("Twilio Signature: {0}".format(signature))
        self.statuscode = r.status_code
        if self.statuscode == 200:
            self.jsonresp = {"status_code": self.statuscode}
        else:
            self.jsonresp = r.json()
        log.info("Headers: {0}".format(headers))
        log.info("response = {0}".format(self.jsonresp))
        return self.jsonresp
