import boto3

import requests
import logging

from back_tests.framework.common.utils import get_year, get_month

log = logging.getLogger("DynamoDBUConnection")

session = requests.Session()


class MessagingDynamoDB(object):
    def __init__(self):
        """
        __init__ method need to be documented
        """
        # Initialize
        self.nbOfMessage = None
        self.dynamodb = None
        # constractor
        self.dynamodb_conn()

    def dynamodb_conn(self):
        """
        Try to connect DynamoDB with Agent
        """
        try:
            self.dynamodb = boto3.resource("dynamodb", "us-west-2")

        except Exception as msg:
            log.error(f"Oops, could not connect to DynamoDB.{msg}")
            raise (msg)

    def get_dynamodb_agentlimit(self, agentId):
        """
        Get current NumberOfMessages from DynamoDB for Agent
        """
        try:
            table = self.dynamodb.Table("staging-messaging-agent-counter-projection")
            response = table.get_item(
                Key={
                    "agentId-year-month": "{0}#{1}#{2}".format(
                        agentId, get_year(), get_month()
                    )
                }
            )
            item = response["Item"]
            self.nbOfMessage = item["numberOfMessages"]
            log.info(
                "Current numberOfMessage for Agent {0} is: {1}".format(
                    agentId, self.nbOfMessage
                )
            )
        except Exception as msg:
            log.error(f"Oops, could not get: {msg}")
            raise (msg)

    def update_dynamodb_agentlimit(self, agentId, numberofmessages):
        """
        Update current NumberOfMessages from DynamoDB for Agent
        """
        try:
            table = self.dynamodb.Table("staging-messaging-agent-counter-projection")
            response = table.update_item(
                Key={
                    "agentId-year-month": "{0}#{1}#{2}".format(
                        agentId, get_year(), get_month()
                    )
                },
                UpdateExpression="SET numberOfMessages = :val1",
                ExpressionAttributeValues={":val1": numberofmessages},
                ReturnValues="UPDATED_NEW",
            )
            log.info(
                "Succesfully Updated the numberOfMessages in DynamoDB for Agent {0} to {1}".format(
                    agentId, numberofmessages
                )
            )
        except Exception as msg:
            log.error(f"Oops, could not update: {msg}")
            raise (msg)
