import logging
import requests
import os
import time
import string
import json

log = logging.getLogger("UserManagement")

session = requests.Session()

# TODO: optional base url stag - prod
BASE_URL = "https://id.aircall-staging.com"


class UserManagementEmailUpdate(object):
    def __init__(self):
        """
        __init__ method need to be documented
        """
        self.base_url = BASE_URL
        self.headers = {"Content-Type": "application/json", "User-Agent": "Mozilla/5.0"}

        # Initialize
        self.response_status = None

    def usm_email_update(self, userId, email, auth_token):

        url = self.base_url + "/auth/v1/users/" + str(userId)
        headers = self.headers
        headers["authorization"] = "Bearer " + auth_token

        payload = {"email": email}

        response = session.put(url, headers=headers, data=json.dumps(payload))
        # json_response_usm = response.json()

        log.info("Response code: {0}".format(response.status_code))
        self.response_status = response.status_code

        if self.response_status == 200:
            log.info("Sucessfully email is updated")
        else:
            log.error("email update is failed")

        return response
