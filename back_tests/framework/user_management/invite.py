import logging
import requests
import os
import time
import string
import json

log = logging.getLogger("UserManagement")

session = requests.Session()


# TODO: optional base url stag - prod
BASE_URL = "https://id.aircall-staging.com"


class UserManagementInvite(object):
    def __init__(self):
        """
        __init__ method need to be documented
        """
        self.base_url = BASE_URL
        self.headers = {"Content-Type": "application/json", "User-Agent": "Mozilla/5.0"}

        # Initialize
        self.resend_response_status = None

    def sendInvitation(self, firstname, lastname, isAdmin, language, email, auth_token):

        url = self.base_url + "/auth/v1/users/invite"
        headers = self.headers
        headers["authorization"] = "Bearer " + auth_token
        payload = {
            "firstName": firstname,
            "lastName": lastname,
            "isAdmin": isAdmin,
            "language": language,
            "email": email,
        }

        response = session.post(url, headers=headers, data=json.dumps(payload))
        json_response_usm = response.json()

        if "id" in json_response_usm:
            self.id = json_response_usm.get("id")
            log.info("id is :  {0}".format(self.id))
            self.email_invite = json_response_usm.get("email")
            log.info("invited email is :  {0}".format(self.email_invite))
        else:
            raise Exception("Invitation send is fail")
        return response

    def resendInvitation(self, auth_token, userId):

        url = self.base_url + "/auth/v1/users/" + str(userId) + "/invitation/send"
        headers = self.headers
        headers["authorization"] = "Bearer " + auth_token

        response = session.post(url, headers=headers)

        log.info("Response code: {0}".format(response.status_code))
        self.resend_response_status = response.status_code

        return response
