import logging
import requests
import os
import time
import string
import json

log = logging.getLogger("UserManagement")

session = requests.Session()

# TODO: optional base url stag - prod
BASE_URL = "https://id.aircall-staging.com"


class UserManagement(object):
    def __init__(self, login, password):
        """
        __init__ method need to be documented
        """
        self.login = login
        self.password = password
        self.base_url = BASE_URL
        self.headers = {"Content-Type": "application/json", "User-Agent": "Mozilla/5.0"}

        # Initialize
        self.idToken = None
        self.updatedEmail = None
        self.usm_sign_in(login, password)

    def usm_sign_in(self, login, password):

        url = self.base_url + "/auth/v1/users/session"

        headers = self.headers

        payload = {"password": password, "email": login}

        response = session.post(url, headers=headers, data=json.dumps(payload))
        json_response_usm = response.json()

        log.info("Response code: {0}".format(response.status_code))

        if "accessToken" in json_response_usm:
            self.idToken = json_response_usm.get("idToken")
            log.debug("idToken is catched successfully.")
        else:
            raise Exception("Cognito Token Fail")
        return response

    def get_current_user(self, userId, auth_token):

        url = self.base_url + "/auth/v1/admin/users/" + str(userId)
        headers = self.headers
        headers["authorization"] = "Bearer " + auth_token

        response = session.get(url, headers=headers)
        json_response_usm = response.json()

        log.info("Response code: {0}".format(response.status_code))
        log.info("Response is: {0}".format(response))

        if "emailVerified" in json_response_usm:
            self.updatedEmail = json_response_usm.get("email")
            log.debug("updated email is :  {0}".format(self.updatedEmail))
        else:
            raise Exception("Retrieve current user is failed")
        return response

    def usm_sign_out(self, auth_token):

        url = self.base_url + "/auth/v1/users/session"
        headers = self.headers
        headers["authorization"] = "Bearer " + auth_token

        response = session.delete(url, headers=headers)

        log.info("Response code: {0}".format(response.status_code))
        self.sign_out_response_status = response.status_code

        if response.status_code == 204:
            log.info("Sucessfully sign out")
        else:
            log.error("Sign out is failed")
        return response
