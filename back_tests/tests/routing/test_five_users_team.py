import logging
import time
import random

import pytest

from back_tests.framework.common.twilio import twilio

from back_tests.framework.common.utils import multithreaded

from pytest_testrail.plugin import pytestrail

DELAY_BETWEEN_CALLS = 1  # seconds
STOP_RINGING_TIMEOUT = 2  # arbitrary
TIMEOUT_FIRST_CALL = 10

log = logging.getLogger("TEST")


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [
        [
            "aircall41@aircall.io",
            "aircall42@aircall.io",
            "aircall43@aircall.io",
            "aircall44@aircall.io",
            "aircall45@aircall.io",
        ]
    ],
    indirect=True,
)
@pytest.mark.parametrize(
    "nr_queued_calls",
    (
        pytest.param(2, marks=pytest.mark.testrail(ids=("C10782741",))),
        pytest.param(3, marks=pytest.mark.testrail(ids=("C10782742",))),
    ),
)
def test_five_agents_available(
    nr_queued_calls,
    incoming_calls,
    check_number_five_agents_simultaneous,
    bulk_creation_available_agent,
    wait_until_agent_ringing,
    wait_until_task_not_in_queue,
    wait_until_queue_octopus_is_empty,
):
    """
    Distrib: Five Agents available (simultaneous 1 min)

    * Scenario:
      1. Five Agents available
      2. Loop on <nr_queued_calls>:
        a. External caller <index> calls
        b. External caller <index> hangs up

    * Acceptance criteria
      2. for each loop:
        a. The 5 Agents are ringing on call <index>
    """
    receive_call_number = check_number_five_agents_simultaneous

    # 1. Login 5 agents
    phones = phone, _, _, _, _ = bulk_creation_available_agent
    company_id = phone.company_id

    # 2. Trigger <nr_queued_calls> inbound calls
    call_sid_list = incoming_calls(receive_call_number, nr_queued_calls, 4, 2)

    # Loop on all incoming calls
    for nr_calls, call_sid in enumerate(call_sid_list, 1):

        # For initialization, wait for first incoming call to be received
        start = time.time()
        current_call_id = phone.task_id
        while (
            len(set(phone.received)) != nr_calls
            and time.time() - start < TIMEOUT_FIRST_CALL
        ):
            current_call_id = phone.task_id

        # Check all the agents are ringing
        args = [(agent.user_id, company_id, 30) for agent in phones]
        ringing = multithreaded(len(phones), args, wait_until_agent_ringing)
        assert all(ringing)

        # Let's ring few seconds
        time.sleep(random.randint(1, 5))

        # External caller hangs up
        assert twilio.waiting_until_call_status(call_sid, "in-progress", 10)
        twilio.hang_up(call_sid)
        assert twilio.waiting_until_call_status(call_sid, "completed", 10)

        # Ensure <task_id> is no more in octopus before the next call check
        assert wait_until_task_not_in_queue(current_call_id, company_id)

    # Final check - ensure all the calls were correctly dispatched
    assert len(set(phone.received)) == nr_queued_calls, phone.received


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [
        [
            "aircall41@aircall.io",
            "aircall42@aircall.io",
            "aircall43@aircall.io",
            "aircall44@aircall.io",
            "aircall45@aircall.io",
        ]
    ],
    indirect=True,
)
@pytestrail.case("C10782748")
def test_five_agents_ringing_one_accept(
    agents_ringing,
    check_number_five_agents_simultaneous,
    bulk_creation_available_agent,
    wait_until_agent_ringing,
    wait_until_queue_octopus_is_empty,
    external_call_number,
):
    """
    Distrib: Five Agents (simultaneous 1 min)

    * Scenario
      1. Login 5 agents
      2. Receive inbound call
      3. Agent 1 answers the call

    * Acceptance criteria
      2. Five agents "ringing"
      3. No more agent "ringing" within STOP_RINGING_TIMEOUT
    """
    receive_call_number = check_number_five_agents_simultaneous

    # 1. Login 5 agents
    phones = phone, _, _, _, _ = bulk_creation_available_agent

    company_id = phone.company_id

    # 2. Trigger an inbound call
    _ = twilio.inbound_call(external_call_number, receive_call_number)

    # Five agents "ringing"
    for agent in phones:
        assert wait_until_agent_ringing(agent.user_id, company_id)

    # 4. One agent answers the call
    assert phone.answer_call(phone.task_id) == 200

    # TODO improve timeout, what is the acceptance criteria ?
    time.sleep(STOP_RINGING_TIMEOUT)

    log.info("Check none agent is ringing")
    agents_ringing_responses = agents_ringing(phones)

    assert all(not ringing for ringing in agents_ringing_responses), log.error(
        "{0}".format(
            list(zip([agent.user_id for agent in phones], agents_ringing_responses))
        )
    )
