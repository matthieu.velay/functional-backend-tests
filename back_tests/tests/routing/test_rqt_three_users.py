"""
Respect Queuing Time Functional tests
https://aircall-product.atlassian.net/browse/CORE-936
"""

import logging
import time
from collections import namedtuple

import pytest
from pytest_testrail.plugin import pytestrail

from back_tests.framework.common.twilio import twilio
from back_tests.framework.common.utils import is_in_range

log = logging.getLogger("TEST")


RingingTime = namedtuple("ringing_time", ["default", "remaining", "mini"])
RQT_TIME = RingingTime(25, 10, 20)  # seconds
ACCEPTANCE_PERCENTAGE = 0.2
ACCEPTANCE_PER_REMAINING = 1


# https://aircall-product.atlassian.net/browse/CORE-1027
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall10@aircall.io", "aircall11@aircall.io", "aircall12@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C10797513")
def test_three_agents_one_call_random20(
    check_ringing_time,
    ringing_order,
    check_number_three_agents_one_team_random20,
    bulk_creation_available_agent,
    incoming_calls,
):
    """
    * Distrib: Team A (Agent 1 & Agent 2 & Agent 3) -  Random - 20 sec queuing time

    * Scenario:
    - Agent 1 & 2 & 3 Available
    - trigger 1 call
    - Agent 1 does not pick up
    - Agent 2 does not pick up
    - Agent 3 does not pick up


    * Acceptance criteria
    - One of the Agent rings during 20 seconds call A
    - Call A ends
    """
    receive_call_number = check_number_three_agents_one_team_random20

    phones = bulk_creation_available_agent

    # Trigger 1 call
    (call_sid,) = incoming_calls(
        receive_call_number=receive_call_number, nr_queued_calls=1, loop=2, delay=0
    )

    assert twilio.waiting_until_call_status(
        call_sid=call_sid, exp_status="completed", timeout=40, loop_timeout=3
    )

    # Phones order in Random distrib
    first_phone, _, _ = ringing_order(phones)

    check_ringing_time(
        first_phone, [RQT_TIME.mini], [ACCEPTANCE_PERCENTAGE]
    )  # One Agent ringing


# https://aircall-product.atlassian.net/browse/CORE-1031
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall10@aircall.io", "aircall11@aircall.io", "aircall12@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C10797514")
def test_three_agents_fifo_calls(
    check_ringing_time,
    ringing_order,
    check_number_three_agents_one_team_random60,
    bulk_creation_available_agent,
    incoming_calls,
):
    """
    * Distrib: Team A (Agent 1 & Agent 2 & Agent 3) -  Random - 1 min queuing time

    * Scenario:
    - Agent 1 & 2 & 3 Available
    - trigger 2 call(s) with 5 seconds delay between each
    - Agent 1 does not pick up
    - Agent 2 does not pick up
    - Agent 3 does not pick up


    * Acceptance criteria
    - Agent 1 rings during 25 seconds call A
    - Agent 2 rings during 25 seconds call B
    - Agent 3 rings during 25 seconds call A
    - Agent 1 rings during 25 seconds call B
    - Agent 2 rings during 10 seconds call A
    - Agent 3 rings during 10 seconds call B

    - Call A ends
    - Call B ends
    """
    receive_call_number = check_number_three_agents_one_team_random60

    phones = bulk_creation_available_agent

    # Trigger 2 calls with 5 seconds delay
    sid1, sid2 = incoming_calls(
        receive_call_number=receive_call_number, nr_queued_calls=2, loop=3, delay=5
    )

    assert twilio.waiting_until_call_status(
        call_sid=sid1, exp_status="completed", timeout=100, loop_timeout=3
    )
    assert twilio.waiting_until_call_status(
        call_sid=sid2, exp_status="completed", timeout=100, loop_timeout=3
    )

    # Phones order in Random distrib
    first_phone, second_phone, third_phone = ringing_order(phones)

    # Agents expected ringing time
    ringing_time_expected_l = (
        [RQT_TIME.default, RQT_TIME.default],
        [RQT_TIME.default, RQT_TIME.remaining],
        [RQT_TIME.default, RQT_TIME.remaining],
    )
    acceptance_percentage_l = (
        [ACCEPTANCE_PERCENTAGE, ACCEPTANCE_PERCENTAGE],
        [
            ACCEPTANCE_PERCENTAGE,
            ACCEPTANCE_PER_REMAINING,
        ],  # Be less demanding on remaining ringing time
        [
            ACCEPTANCE_PERCENTAGE,
            ACCEPTANCE_PER_REMAINING,
        ],  # Be less demanding on remaining ringing time
    )
    for agent_l, acceptance_per, phone in zip(
        ringing_time_expected_l,
        acceptance_percentage_l,
        [first_phone, second_phone, third_phone],
    ):
        check_ringing_time(phone, agent_l, acceptance_per)

    # Check FIFO consistency here
    call1, call4 = first_phone.received
    call2, call5 = second_phone.received
    call3, call6 = third_phone.received

    assert call1 == call3 == call5, "call1:{0} - call3:{1} - call5: {2}".format(
        call1, call3, call5
    )
    assert call2 == call4 == call6, "call2:{0} - call4:{1} - call6: {2}".format(
        call2, call4, call6
    )


# https://aircall-product.atlassian.net/browse/CORE-1028
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall10@aircall.io", "aircall11@aircall.io", "aircall12@aircall.io"]],
    indirect=True,
)
@pytest.mark.parametrize(
    "distrib_fixture",
    (
        pytest.param(
            "check_number_three_agents_one_team_random60",
            marks=pytest.mark.testrail(ids=("C10797516",)),
        ),
        pytest.param(
            "check_number_one_team_three_agents_simult60_rqt",
            marks=pytest.mark.testrail(ids=("C10817699",)),
        ),
    ),
)
def test_all_agents_offline(
    distrib_fixture, bulk_creation_available_agent, incoming_calls, request
):
    """
    * Distrib:
    - case 1: Team A (Agent 1 & Agent 2 & Agent 3) -  Random - 1 min queuing time
    - case 2: Team A (Agent 1 & Agent 2 & Agent 3) -  Simult - 1 min queuing time


    * Scenario:
    - All agents offline
    - trigger one call A

    * Acceptance criteria
    - Call A ends after 1 min
    """
    receive_call_number = request.getfixturevalue(distrib_fixture)

    phone1, phone2, phone3 = bulk_creation_available_agent

    phone1.logout()
    phone2.logout()
    phone3.logout()

    (call_sid,) = incoming_calls(
        receive_call_number=receive_call_number, nr_queued_calls=1, loop=3, delay=0
    )

    assert twilio.waiting_until_call_status(
        call_sid=call_sid, exp_status="completed", timeout=80, loop_timeout=2
    )

    # Check twilio call duration
    call_info = twilio.get_call(call_sid)
    call_duration = int(call_info.duration)

    assert is_in_range(call_duration, 60, ACCEPTANCE_PERCENTAGE)
