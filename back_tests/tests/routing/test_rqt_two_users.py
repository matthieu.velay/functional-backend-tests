"""
Respect Queuing Time Functional tests
https://aircall-product.atlassian.net/browse/CORE-936
"""

import logging
import time
from collections import namedtuple

import pytest
from pytest_testrail.plugin import pytestrail

from back_tests.framework.common.twilio import twilio
from back_tests.framework.common.utils import is_in_range

log = logging.getLogger("TEST")


RingingTime = namedtuple("ringing_time", ["default", "remaining", "mini"])
RQT_TIME = RingingTime(25, 10, 20)  # seconds
ACCEPTANCE_PERCENTAGE = 0.2
ACCEPTANCE_PER_REMAINING = 1


# https://aircall-product.atlassian.net/browse/CORE-1024
# https://aircall-product.atlassian.net/browse/CORE-1025
@pytest.mark.parametrize(
    "bulk_creation_available_agent", [["aircall20@aircall.io"]], indirect=True
)
@pytest.mark.parametrize(
    "nr_incoming_calls",
    (
        pytest.param(1, marks=pytest.mark.testrail(ids=("C10797509",))),
        pytest.param(2, marks=pytest.mark.testrail(ids=("C10797510",))),
    ),
)
def test_one_agent_no_pickup(
    check_ringing_time,
    check_number_one_team_one_agent_random_rqt,
    bulk_creation_available_agent,
    nr_incoming_calls,
    incoming_calls,
):
    """
    * Distrib: Team A (Agent 1) -  Random - 1 min queuing time

    * Scenario:
    - Agent 1 Available
    - trigger call(s)
        -  case 1: one external call (A)
        -  case 2: two external calls (A and B) simultaneously triggered
    - Agent 1 does not pick up

    * Acceptance criteria
    - Agent 1 rings during 25 seconds call A
    - Agent 1 rings during 25 seconds call A
    - Agent 1 rings during 10 seconds call A
    - Call A ends
    """
    # Use the parametrize distribution
    receive_call_number = check_number_one_team_one_agent_random_rqt

    # 1. Login with agent A credentials
    (phone,) = bulk_creation_available_agent

    call_sid, *_ = incoming_calls(
        receive_call_number=receive_call_number,
        nr_queued_calls=nr_incoming_calls,
        loop=3,
        delay=2,
    )
    agent1_l = [RQT_TIME.default, RQT_TIME.default, RQT_TIME.remaining]
    acc_per1_l = [
        ACCEPTANCE_PERCENTAGE,
        ACCEPTANCE_PERCENTAGE,
        ACCEPTANCE_PER_REMAINING,
    ]

    assert twilio.waiting_until_call_status(
        call_sid=call_sid, exp_status="completed", timeout=80, loop_timeout=3
    )

    check_ringing_time(phone, agent1_l, acc_per1_l)

    # Check twilio call duration
    call_info = twilio.get_call(call_sid)
    call_duration = int(call_info.duration)

    assert is_in_range(call_duration, 60, ACCEPTANCE_PERCENTAGE)


# https://aircall-product.atlassian.net/browse/CORE-1026
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall20@aircall.io", "aircall21@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C10797511")
def test_two_agents_one_call(
    check_ringing_time,
    ringing_order,
    check_number_two_agents_random,
    bulk_creation_available_agent,
    incoming_calls,
):
    """
    * Distrib: Team A (Agent 1 & Agent 2) -  Random - 1 min queuing time

    * Scenario:
    - Agent 1 & 2 Available
    - trigger one call A
    - Agent 1 does not pick up
    - Agent 2 does not pick up

    * Acceptance criteria
    - Agent 1 rings during 25 seconds call A
    - Agent 2 rings during 25 seconds call A
    - (Agent 1 OR Agent 2) rings during 10 seconds call A
    - Call A ends
    """
    receive_call_number = check_number_two_agents_random

    phones = bulk_creation_available_agent

    # Trigger 1 call
    (call_sid,) = incoming_calls(
        receive_call_number=receive_call_number, nr_queued_calls=1, loop=3, delay=0
    )

    assert twilio.waiting_until_call_status(
        call_sid=call_sid, exp_status="completed", timeout=80, loop_timeout=3
    )

    # Phones order in Random distrib
    first_phone, second_phone = ringing_order(phones)

    # (Agent 1 OR Agent 2) rings during 10 seconds, we need to handle both cases
    if len(first_phone.received) == 2:
        agent1_l, agent2_l = (
            [RQT_TIME.default, RQT_TIME.remaining],
            [RQT_TIME.default],
        )
        acc_per1_l, acc_per2_l = (
            [ACCEPTANCE_PERCENTAGE, ACCEPTANCE_PER_REMAINING],
            [ACCEPTANCE_PERCENTAGE],
        )
        call1, call2 = first_phone.received
        (call3,) = second_phone.received
    elif len(second_phone.received) == 2:
        agent1_l, agent2_l = (
            [RQT_TIME.default],
            [RQT_TIME.default, RQT_TIME.remaining],
        )
        acc_per1_l, acc_per2_l = (
            [ACCEPTANCE_PERCENTAGE],
            [ACCEPTANCE_PERCENTAGE, ACCEPTANCE_PER_REMAINING],
        )
        (call1,) = first_phone.received
        (
            call2,
            call3,
        ) = second_phone.received
    else:
        assert False, "One agent should receive the call twice"

    check_ringing_time(first_phone, agent1_l, acc_per1_l)  # First Agent
    check_ringing_time(second_phone, agent2_l, acc_per2_l)  # Second Agent

    assert call1 == call3 == call2

    # https://aircall-product.atlassian.net/browse/CORE-1030


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall20@aircall.io", "aircall21@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C10797512")
def test_two_agents_fifo_calls(
    check_ringing_time,
    ringing_order,
    check_number_two_agents_random,
    bulk_creation_available_agent,
    incoming_calls,
):
    """
    * Distrib: Team A (Agent 1 & Agent 2) -  Random - 1 min queuing time

    * Scenario:
    - Agent 1 & 2 Available
    - trigger 3 call(s) with 5 seconds delay between each
    - Agent 1 does not pick up
    - Agent 2 does not pick up

    * Acceptance criteria
    - Agent 1 rings during 25 seconds call A
    - Agent 2 rings during 25 seconds call B
    - Agent 1 rings during 25 seconds call A
    - Agent 2 rings during 25 seconds call B
    - Agent 1 rings during 10 seconds call A
    - Agent 2 rings during 10 seconds call B
    - Agent 1 rings during 10 seconds call C

    - Call A ends
    - Call B ends
    - Call C ends
    """
    receive_call_number = check_number_two_agents_random

    phones = bulk_creation_available_agent

    # Trigger 3 calls with 5 seconds delay
    calls_sid = incoming_calls(
        receive_call_number=receive_call_number, nr_queued_calls=3, loop=3, delay=5
    )

    # Phones order in Random distrib
    first_phone, second_phone = ringing_order(phones)

    # Assert all the calls end
    for call_sid in calls_sid:
        assert twilio.waiting_until_call_status(
            call_sid=call_sid, exp_status="completed", timeout=80, loop_timeout=3
        )

    # Agents expected ringing time
    agent1_l, agent2_l = (
        [RQT_TIME.default, RQT_TIME.default, RQT_TIME.remaining, RQT_TIME.remaining],
        [RQT_TIME.default, RQT_TIME.default, RQT_TIME.remaining],
    )

    acc_per1_l, acc_per2_l = (
        [
            ACCEPTANCE_PERCENTAGE,
            ACCEPTANCE_PERCENTAGE,
            ACCEPTANCE_PER_REMAINING,
            ACCEPTANCE_PER_REMAINING,
        ],
        [ACCEPTANCE_PERCENTAGE, ACCEPTANCE_PERCENTAGE, ACCEPTANCE_PER_REMAINING],
    )
    # Check ringing time with pusher notifications
    check_ringing_time(first_phone, agent1_l, acc_per1_l)  # Agent 1
    check_ringing_time(second_phone, agent2_l, acc_per2_l)  # Agent 2

    # Check FIFO consistency
    call1, call2, call3, call4 = first_phone.received
    assert call1 == call2 == call3 != call4

    call5, call6, call7 = second_phone.received
    assert call5 == call6 == call7 != call4

    # https://aircall-product.atlassian.net/browse/CORE-1028


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall20@aircall.io", "aircall21@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C10797515")
def test_one_agent_declines(
    ringing_order,
    check_number_two_agents_random,
    bulk_creation_available_agent,
    incoming_calls,
):
    """
    * Distrib: Team A (Agent 1 & Agent 2) -  Random - 1 min queuing time

    * Scenario:
    - Agent 1 available & Agent 2 offline
    - trigger one call A
    - Agent 1 does not pick up during the first 25 seconds
    - Agent 1 declines the call 5 seconds after the call rings again

    * Acceptance criteria
    - Agent 1 rings during 25 seconds call A
    - Agent 1 rings during 5 seconds call A
    - Agent stops ringing after the decline
    - Call A ends after 1 min
    """
    receive_call_number = check_number_two_agents_random

    phone1, phone2 = phones = bulk_creation_available_agent

    phone2.logout()

    # Trigger 1 call
    (call_sid,) = incoming_calls(
        receive_call_number=receive_call_number, nr_queued_calls=1, loop=3, delay=0
    )

    # Phones order in Random distrib
    first_phone, _ = ringing_order(phones)
    assert first_phone == phone1

    # Wait 30 seconds
    time.sleep(30)

    assert phone1.reject_call(phone1.task_id) == 204

    assert twilio.waiting_until_call_status(
        call_sid=call_sid, exp_status="completed", timeout=65, loop_timeout=10
    )

    # Check twilio call duration
    call_info = twilio.get_call(call_sid)
    call_duration = int(call_info.duration)

    assert is_in_range(call_duration, 60, ACCEPTANCE_PERCENTAGE)
