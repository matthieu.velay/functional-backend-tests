import logging

import pytest

from back_tests.framework.common.twilio import twilio
from back_tests.framework.common.utils import is_in_range, multithreaded

log = logging.getLogger("TEST")

from pytest_testrail.plugin import pytestrail

RINGING_TIME_RANDOM = 25  # seconds
RINGING_TIME_SIMULT = 60  # seconds
ACCEPTANCE_PERCENTAGE = 0.2

# https://aircall-product.atlassian.net/browse/CORE-1000
# https://aircall-product.atlassian.net/browse/CORE-1003
# https://aircall-product.atlassian.net/browse/CORE-1004
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall40@aircall.io"]],
    indirect=True,
)
@pytest.mark.parametrize(
    "distrib_fixture,ringing_time_expected, loop",
    (
        pytest.param(
            "check_number_one_agent",
            RINGING_TIME_RANDOM,
            2,
            marks=pytest.mark.testrail(ids=("C10782724",)),
        ),
        pytest.param(
            "check_number_one_team_one_agent_random60",
            RINGING_TIME_RANDOM,
            2,
            marks=pytest.mark.testrail(ids=("C10782727",)),
        ),
        pytest.param(
            "check_number_one_team_one_agent_simult",
            RINGING_TIME_SIMULT,
            3,
            marks=pytest.mark.testrail(ids=("C10782728",)),
        ),
    ),
)
def test_one_agent_no_pickup(
    distrib_fixture,
    ringing_time_expected,
    loop,
    measure_ringing_time,
    bulk_creation_available_agent,
    external_call_number,
    request,
):
    """
    * Distrib
    Case 1: Only one agent assigned to number
    Case 2: One team with one agent assigned to a number (random)
    Case 3: One team with one agent assigned to a number (simultaneous)

    * Scenario
      1. agent A available
      2. Receive inbound call
      3. Agent A does not pick up the call

    * Acceptance criteria
      - Agent is in ringing state in Octopus during ringing_time seconds +- ACCEPTANCE_PERCENTAGE
    """
    # Use the parametrize distribution
    receive_call_number = request.getfixturevalue(distrib_fixture)

    # 1. Login with agent A credentials
    (phone,) = bulk_creation_available_agent

    # 2. Trigger an inbound call
    _ = twilio.inbound_call(
        from_=external_call_number, to_=receive_call_number, loop_=loop
    )

    # Measure ringing time
    ringing_time, call_id = measure_ringing_time(
        phone, RINGING_TIME_SIMULT, ACCEPTANCE_PERCENTAGE
    )
    log.info("Ringing time: {0} seconds".format(ringing_time))

    assert is_in_range(ringing_time, ringing_time_expected, ACCEPTANCE_PERCENTAGE)

    # Check call is received, rejected (Pusher checks)
    assert [call_id] == phone.received == phone.rejected


# https://aircall-product.atlassian.net/browse/CORE-1002
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall40@aircall.io"]],
    indirect=True,
)
@pytest.mark.parametrize(
    "status",
    (
        pytest.param("offline", marks=pytest.mark.testrail(ids=("C10782726",))),
        pytest.param("do_not_disturb", marks=pytest.mark.testrail(ids=("C10782740",))),
    ),
)
def test_one_agent_unavailable_no_ring(
    check_number_one_agent,
    bulk_creation_available_agent,
    wait_until_agent_not_ringing_during,
    external_call_number,
    status,
):
    """
    * Distrib
    Only one agent assigned to number

    * Scenario
      1. Agent A is "offline" or "do not disturb"
      2. Receive inbound call
      3. Call is not dispatched

    * Acceptance criteria
      - Agent is not in octopus during the whole call
    """
    receive_call_number = check_number_one_agent

    # 1. Login with agent A and B credentials
    (phone,) = bulk_creation_available_agent
    user_id = phone.user_id
    log.info("STATUS: {0}".format(status))
    if status == "do_not_disturb":
        phone.set_availability(False)
    elif status == "offline":
        phone.logout()

    company_id = phone.company_id

    # 2. Trigger an inbound call
    _ = twilio.inbound_call(external_call_number, receive_call_number)

    # Agent should not ring
    assert wait_until_agent_not_ringing_during(
        user_id=user_id, company_id=company_id, timeout=RINGING_TIME_RANDOM
    )

    # Check no call received nor rejected (Pusher checks)
    assert not phone.received and not phone.rejected


# https://aircall-product.atlassian.net/browse/CORE-1005
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall40@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C10782729")
def test_one_agent_in_two_teams_no_pickup(
    check_number_one_agent_two_teams,
    measure_ringing_time,
    bulk_creation_available_agent,
    external_call_number,
):
    """
    * Distrib: 1 User (Agent 1) in distribution items:
      - Team A - Random 20 sec
      - Team B - Simultaneous 1 min

    * Scenario
      1. agent 1 available
      2. Receive inbound call
      3. Agent A does not pick up the call

    * Acceptance criteria
      - Agent 1 is ringing during 25 seconds
      - Agent 1 is ringing during 1 min
    """

    receive_call_number = check_number_one_agent_two_teams
    log.info(receive_call_number)

    # 1. Login with agent 1
    (phone,) = bulk_creation_available_agent

    # 2. Trigger an inbound call
    _ = twilio.inbound_call(external_call_number, receive_call_number, 3)

    # Measure ringing time for both teams
    ringing_time_random, call_id = measure_ringing_time(
        phone, RINGING_TIME_RANDOM, ACCEPTANCE_PERCENTAGE
    )
    ringing_time_simult, _ = measure_ringing_time(
        phone, RINGING_TIME_SIMULT, ACCEPTANCE_PERCENTAGE
    )

    # Assert ringing times are correct
    log.info("Ringing time random: {0}".format(ringing_time_random))
    log.info("Ringing time simult: {0}".format(ringing_time_simult))

    assert is_in_range(ringing_time_random, RINGING_TIME_RANDOM, ACCEPTANCE_PERCENTAGE)
    assert is_in_range(ringing_time_simult, RINGING_TIME_SIMULT, ACCEPTANCE_PERCENTAGE)

    # Check call is received, rejected (Pusher checks)
    assert [call_id] * 2 == phone.received == phone.rejected


# https://aircall-product.atlassian.net/browse/CORE-1006
# https://aircall-product.atlassian.net/browse/CORE-1007
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall40@aircall.io", "aircall41@aircall.io"]],
    indirect=True,
)
@pytest.mark.parametrize(
    "status_second_agent",
    (
        pytest.param("available", marks=pytest.mark.testrail(ids=("C10782730",))),
        pytest.param("offline", marks=pytest.mark.testrail(ids=("C10782731",))),
    ),
)
def test_two_agents_in_two_teams_no_pickup(
    check_number_two_agents_two_teams,
    status_second_agent,
    measure_ringing_time,
    bulk_creation_available_agent,
    external_call_number,
    first_ringing,
):
    """
    * Distrib: 2 User (Agent 1, Agent 2) in distribution items:
      - Team A - Random 1 min
      - Team B - Simultaneous 1 min

    * Scenario
      1. agent 1 and 2 available
      2. Receive inbound call
      3. Agents do not pick up the call

    * Acceptance criteria
      - one Agent is ringing during 25 seconds
      - then the other agent is ringing during 25 seconds
      - then both agents are ringing during 1 min
    """

    receive_call_number = check_number_two_agents_two_teams
    log.info(receive_call_number)

    # 1. Login with agent 1 and 2
    (phone, phone2) = phones = bulk_creation_available_agent

    if status_second_agent == "offline":
        phone2.logout()

    # 2. Trigger an inbound call
    _ = twilio.inbound_call(external_call_number, receive_call_number, 4)

    if status_second_agent == "available":
        # Phones order in Random distrib
        first_phone = first_ringing(phones)
        second_phone = phone if phone2 is first_phone else phone2
        ringing_phones = (first_phone, second_phone)
    else:
        ringing_phones = (phone,)

    # Check first team (Random)
    measures = [
        measure_ringing_time(ringing_phone, RINGING_TIME_RANDOM, ACCEPTANCE_PERCENTAGE)
        for ringing_phone in ringing_phones
    ]

    for (ringing_time, call_id) in measures:
        log.info("Ringing time: {0}".format(ringing_time))
        assert is_in_range(ringing_time, RINGING_TIME_RANDOM, ACCEPTANCE_PERCENTAGE)

    # Check second team (Simultaneously)
    args = [
        (phone, RINGING_TIME_SIMULT, ACCEPTANCE_PERCENTAGE) for phone in ringing_phones
    ]
    measures = multithreaded(len(ringing_phones), args, measure_ringing_time)

    for (ringing_time, call_id) in measures:
        log.info("Ringing time: {0}".format(ringing_time))
        assert is_in_range(ringing_time, RINGING_TIME_SIMULT, ACCEPTANCE_PERCENTAGE)

    # Check call is received, rejected (Pusher checks)
    assert [call_id] * 2 == phone.received == phone.rejected


# https://aircall-product.atlassian.net/browse/CORE-1009
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall40@aircall.io", "aircall41@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C10782733")
def test_two_agents_in_two_teams_one_decline(
    check_number_two_agents_two_teams,
    measure_ringing_time,
    bulk_creation_available_agent,
    external_call_number,
    first_ringing,
):
    """
    * Distrib: 2 User (Agent 1, Agent 2) in distribution items:
      - Team A - Random 1 min
      - Team B - Simultaneous 1 min

    * Scenario
      1. agent 1 and 2 available
      2. Receive inbound call
      3. Agents 1 declines the call

    * Acceptance criteria
      - Agent 1 is ringing then declines before the 25 seconds
      - then Agent 2 is ringing during 25 seconds
      - then only Agent 2 is ringing during 1 min
    """

    receive_call_number = check_number_two_agents_two_teams
    log.info(receive_call_number)

    # 1. Login with agent 1 and 2
    (phone, phone2) = phones = bulk_creation_available_agent

    # 2. Trigger an inbound call
    _ = twilio.inbound_call(external_call_number, receive_call_number, 4)

    # Phones order in Random distrib
    first_phone = first_ringing(phones)
    second_phone = phone if phone2 is first_phone else phone2

    # Agent 1 rejects the call
    first_phone.reject_call(first_phone.task_id)

    # Check first team (Random)
    ringing_time, call_id = measure_ringing_time(
        second_phone, RINGING_TIME_RANDOM, ACCEPTANCE_PERCENTAGE
    )
    log.info("Ringing time: {0}".format(ringing_time))
    assert is_in_range(ringing_time, RINGING_TIME_RANDOM, ACCEPTANCE_PERCENTAGE)

    # Check second team (Simultaneous)
    ringing_time, _ = measure_ringing_time(
        second_phone, RINGING_TIME_SIMULT, ACCEPTANCE_PERCENTAGE
    )

    log.info("Ringing time: {0}".format(ringing_time))
    assert is_in_range(ringing_time, RINGING_TIME_SIMULT, ACCEPTANCE_PERCENTAGE)

    # Check call is received, rejected (Pusher checks)
    assert [call_id] == first_phone.received == first_phone.rejected
    assert [call_id] * 2 == second_phone.received == second_phone.rejected
