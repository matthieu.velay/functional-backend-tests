import logging
import time
import random

import pytest
from pytest_testrail.plugin import pytestrail

from back_tests.framework.common.twilio import twilio
from back_tests.framework.common.utils import multithreaded

# Waiting time for the sync between Web (dashboard) and Tree
MAGIC_WAIT_WEB_TREE_SYNC = 10  # seconds

log = logging.getLogger("TEST")


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall35@aircall.io", "aircall36@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C11034013")
def test_sync_web_tree(
    check_updated_number,
    external_call_number,
    bulk_creation_available_agent,
    wait_until_agent_ringing,
    update_distrib,
):
    """
    Distrib:
    Agent A (only) then Agent B (only)

    * Scenario:
    - Agent A in <number> distrib
    - Make an inbound call to <number>
    - Update <number> distribution, set Agent B (only)
    - Make an inbound call to <number>

    * Acceptance criteria:
    - Agent A received the first call
    - Agent B received the second call
    """
    number = check_updated_number
    phone, phone2 = bulk_creation_available_agent
    company_id = phone.company_id

    def inbound_call_and_wait_for_user_to_ring(user_id):
        twilio_call_sid = twilio.inbound_call(external_call_number, number, 3)
        # Wait for the call to be "in-progress"
        assert twilio.waiting_until_call_status(twilio_call_sid, "in-progress", 10)

        assert wait_until_agent_ringing(user_id, company_id)

        twilio.hang_up(twilio_call_sid)
        # Wait for the call to be "completed"
        assert twilio.waiting_until_call_status(twilio_call_sid, "completed", 10)

    inbound_call_and_wait_for_user_to_ring(phone.user_id)

    _ = update_distrib(phone.user_id, phone2.user_id)
    time.sleep(MAGIC_WAIT_WEB_TREE_SYNC)

    inbound_call_and_wait_for_user_to_ring(phone2.user_id)
