import logging
import time

import pytest

from back_tests.framework.common.phone import Phone
from back_tests.framework.common.twilio import twilio

from pytest_testrail.plugin import pytestrail

WRAP_UP_TIME = 90  # seconds

log = logging.getLogger("TEST")


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall13@aircall.io", "aircall14@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C10782749")
def test_two_agents_ringing(
    check_number_two_agents_simultaneous,
    bulk_creation_available_agent,
    wait_until_agent_ringing,
    external_call_number,
):
    """
    * Scenario
      1. Login with agent A and B credentials
      2. Receive inbound call
      3. Agent A answers the call

    * Acceptance criteria
      1. Check octopus queue is EMPTY

      2. Check octopus queue:
        - agents A and B should be in the queue
        - agents A and B should be in RINGING state

      3. Check octopus queue:
        - queue should be EMPTY
    """
    receive_call_number = check_number_two_agents_simultaneous

    # 1. Login with agent A and B credentials
    phone, phone2 = bulk_creation_available_agent

    company_id = phone.company_id

    # 2. Trigger an inbound call
    _ = twilio.inbound_call(external_call_number, receive_call_number)

    # Check octopus queue is not empty is relevant
    assert wait_until_agent_ringing(phone.user_id, company_id)
    assert wait_until_agent_ringing(phone2.user_id, company_id)

    # 4. Both agents try to answer the call, only the first one succeeds
    assert phone.answer_call(phone.task_id) == 200
    assert phone2.answer_call(phone2.task_id) == 400


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall13@aircall.io", "aircall14@aircall.io", "aircall15@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C9415809")
def test_three_agents(
    check_number_three_agents,
    bulk_creation_available_agent,
    wait_until_agent_ringing,
    ring_then_no_ring,
    external_call_number,
):
    """
    * Scenario
      1. Login with agent A, B, C credentials
      2. Receive inbound call
      3. Agent A logout
      4. Agent B rejects the call
      5. Agent C is ringing

    * Acceptance criteria
      1. Check octopus queue is EMPTY
      2. Check octopus queue:
        - agents A in the queue
        - agents A in RINGING state
      3. Check octopus queue:
        - agents B in the queue
        - agents B in RINGING state
      4. Check octopus queue:
        - agents C in the queue
        - agents C in RINGING state
      5. Check octopus queue:
        - queue should be EMPTY
    """
    receive_call_number = check_number_three_agents

    # 1. Login with agent A and B credentials
    phone, phone2, phone3 = bulk_creation_available_agent
    company_id = phone.company_id

    # 3. Trigger an inbound call
    _ = twilio.inbound_call(external_call_number, receive_call_number)

    assert wait_until_agent_ringing(phone.user_id, company_id)
    assert phone.logout() == 204

    assert wait_until_agent_ringing(phone2.user_id, company_id)
    assert phone2.reject_call(phone2.task_id) == 204

    assert wait_until_agent_ringing(phone3.user_id, company_id)

    ring_then_no_ring(phone3.user_id, company_id)


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall13@aircall.io", "aircall14@aircall.io", "aircall15@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C10782743")
def test_offline_then_available(
    check_number_three_agents_one_team,
    bulk_creation_available_agent,
    ring_then_no_ring,
    wait_until_voicemail_recorded,
    external_call_number,
):
    """
    Distrib:
    Agent A -> Agent B -> Agent C -> Team 1 (Agent A, B and C)

    * Scenario:
    - Agent A offline
    - Agent B & C available
    - Team 1 is composed of Agents A, B & C
    1. Receive inbound call
    2. Set Agent available after Agent B does not pickup

    * Acceptance criteria:
    - Agent B is ringing and does not pick up
    - Agent C is ringing and does not pick up
    - Agent A is ringing and does not pickup
    - Octopus queue is empty
    - Voicemail is recorded
    """
    receive_call_number = check_number_three_agents_one_team

    # 1. Agent A offline, Agents B & C available
    phone1, phone2, phone3 = bulk_creation_available_agent
    company_id = phone1.company_id
    user_id = phone1.user_id

    # Ensure Agent A is offline
    login, password = phone1.email, phone1.password
    phone1.logout()

    # 3. Trigger an inbound call
    _ = twilio.inbound_call(external_call_number, receive_call_number, 4)

    # Check call is ringing on agent B
    ring_then_no_ring(phone2.user_id, company_id)

    # Agent A becomes available
    phone1 = Phone(login, password)
    phone1.set_availability(True)

    # Check call is ringing on agent C
    ring_then_no_ring(phone3.user_id, company_id)

    # Check call is ringing on Team 1 (Agent A)
    ring_then_no_ring(user_id, company_id)

    # Check call goes to voicemail
    assert wait_until_voicemail_recorded(phone1, phone1.task_id)


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall13@aircall.io", "aircall14@aircall.io"]],
    indirect=True,
)
# TODO Reactivate "wrap_up" or "in_call" as soon as we have Pulse access"
@pytest.mark.parametrize(
    "status",
    (
        pytest.param("available", marks=pytest.mark.testrail(ids=("C10782745",))),
        pytest.param("offline", marks=pytest.mark.testrail(ids=("C10782746",))),
        pytest.param("do_not_disturb", marks=pytest.mark.testrail(ids=("C10782747",))),
    ),
)
def test_simple_availability(
    status,
    check_number_two_agents_simultaneous,
    bulk_creation_available_agent,
    external_call_number,
    ring_then_no_ring,
    wait_until_agent_ringing,
    wait_until_voicemail_recorded,
    wait_agent_not_in_queue_during,
):
    """
    Distrib:
    Agent A & Agent B

    * Scenario:
    - Agent A available
    1. Agent B set to <status>
    2. Receive an inbound call

    * Acceptance criteria
    - if <status> is available => phone is "ringing" on Agent B
    - otherwise phone is not ringing on Agent B
    """
    receive_call_number = check_number_two_agents_simultaneous

    # Agent A available
    phone1, phone2 = bulk_creation_available_agent
    company_id = phone1.company_id

    user_id = phone2.user_id

    # Set Agent B <status>
    if status == "available":
        pass
    elif status == "offline":
        phone2.logout()
    elif status == "do_not_disturb":
        phone2.set_availability(False)

    # 2. Trigger an inbound call
    _ = twilio.inbound_call(external_call_number, receive_call_number)

    assert wait_until_agent_ringing(phone1.user_id, company_id)

    if status == "available":
        ring_then_no_ring(user_id, company_id)
    elif status in ("offline", "do_not_disturb"):
        assert phone1.reject_call(phone1.task_id) == 204
        assert wait_agent_not_in_queue_during(user_id, company_id, 30)
        assert wait_until_voicemail_recorded(phone1, phone1.task_id)


@pytest.mark.parametrize(
    "bulk_creation_available_agent", [["aircall13@aircall.io"]], indirect=True
)
@pytest.mark.parametrize(
    "digit",
    (
        pytest.param("1", marks=pytest.mark.testrail(ids=("C10782744",))),
        pytest.param("2", marks=pytest.mark.testrail(ids=("C11033860",))),
    ),
)
def test_ivr(
    check_number_two_agents_simultaneous,
    ivr_number,
    digit,
    bulk_creation_available_agent,
    ring_then_no_ring,
    external_call_number,
):
    """
    Distrib:
    a. Externall -> IVR -> press #1 -> Aircall number -> Agent A
    b. Externall -> IVR -> press #2 -> External number

    * Scenario:
    1. External call IVR number
    2. External press <digit>

    * Acceptance criteria:
    a. External should be redirected to Agent A (ringing)
    b. External should be redirected to External number (ensure twilio status remains "in-progress")
    """
    _ = check_number_two_agents_simultaneous  # only check the distrib, no need to get the number

    if digit == "1":
        # Agent A available
        (phone1,) = bulk_creation_available_agent
        company_id = phone1.company_id

        user_id = phone1.user_id

    # 2. Trigger an inbound call
    twilio_call_sid = twilio.inbound_call(external_call_number, ivr_number, 3)

    # Wait for the call to be "in-progress"
    assert twilio.waiting_until_call_status(twilio_call_sid, "in-progress", 10)

    # 3. External press <digit>
    twilio.press_digits(digit, twilio_call_sid)

    if digit == "1":
        # 4. External should be redirected to Agent A (ringing)
        ring_then_no_ring(user_id, company_id)
    elif digit == "2":
        # 4. Ensure call is still in progress during 30 seconds after the redirection
        assert twilio.waiting_call_status_remains(twilio_call_sid, "in-progress", 30)


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall13@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C10782749")
def test_blacklisted_number(
    bulk_creation_available_agent,
    check_number_two_agents_simultaneous,
    external_blacklisted_number,
    wait_until_agent_ringing,
):
    """
    Distrib: Agent A

    * Scenario
       - blacklisted number triggers an inbound call


    * Acceptance criteria
       - Agent must NOT ring
    """
    receive_call_number = check_number_two_agents_simultaneous

    # 1. Login with agent A
    (phone,) = bulk_creation_available_agent
    company_id = phone.company_id

    # 2. Trigger an inbound call
    twilio_call_sid = twilio.inbound_call(
        external_blacklisted_number, receive_call_number
    )

    assert not wait_until_agent_ringing(phone.user_id, company_id, 30)
