import logging
import time

import pytest

from back_tests.framework.routing.public_api import public_api
from back_tests.framework.common.twilio import twilio

from pytest_testrail.plugin import pytestrail

from back_tests.framework.common.utils import compute_gap_between_times

log = logging.getLogger("TEST")


@pytest.mark.webhook
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [
        [
            "aircall10@aircall.io",
            "aircall11@aircall.io",
            "aircall12@aircall.io",
            "aircall40@aircall.io",
        ]
    ],
    indirect=True,
)
@pytest.mark.parametrize(
    "call_status,transfer_to",
    (
        pytest.param(
            "not_answered", "user", marks=pytest.mark.testrail(ids=("C10916043",))
        ),
        pytest.param("ended", "user", marks=pytest.mark.testrail(ids=("C10917479",))),
        pytest.param(
            "not_answered", "team", marks=pytest.mark.testrail(ids=("C10916044",))
        ),
        pytest.param("ended", "team", marks=pytest.mark.testrail(ids=("C10917480",))),
        pytest.param(
            "not_answered", "external", marks=pytest.mark.testrail(ids=("C10916045",))
        ),
        pytest.param(
            "ended", "external", marks=pytest.mark.testrail(ids=("C10917481",))
        ),
    ),
)
def test_transfer_to_user_or_team_or_number(
    reset_calls_server,
    activate_webhook,
    bulk_creation_available_agent,
    receive_call_number_transfer_one_agent,
    external_call_number_to_transfer,
    external_call_number,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    team_to_transfer,
    wait_until_agent_ringing,
    wait_until_transfer_succeed,
    wait_until_call_created,
    wait_until_call_answered,
    wait_until_queue_octopus_is_empty,
    check_ringing_time,
    call_status,
    transfer_to,
):
    """
    Test company transfers to a user / Team A or external
    All agents of team A are available
    * Scenario
      Test company has set up a webhook
      External caller to User 1
      Call is transfered to user 2 / Team A or external

    * Acceptance criteria
      if call not answered:
          - user 2 is ringing during 25 seconds
          - All agents of team A are in ringing state during 25 seconds
          - external number is reached (check twilio call duration)
        test company receives a status code 204

      if call ended
        call ends
        test company receives a status code 400
    """

    payload = None
    phones_transfer = None

    (
        phone_transfer1,
        phone_transfer2,
        phone_transfer3,
        phone,
    ) = bulk_creation_available_agent
    company_id = phone.company_id

    # External call
    twilio_call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_transfer_one_agent, 5
    )
    assert twilio.waiting_until_call_status(twilio_call_sid, "in-progress", 20)

    # Waiting for the webhook call.created
    assert wait_until_call_created()
    current_call_id = daemon_flask_server.call_created[-1]["data"]["id"]

    # Build the payload
    if transfer_to == "user":
        payload = {"user_id": phone_transfer1.user_id}
        phones_transfer = (phone_transfer1,)
    elif transfer_to == "team":
        payload = {"team_id": team_to_transfer}
        phones_transfer = phone_transfer1, phone_transfer2, phone_transfer3
    elif transfer_to == "external":
        payload = {"number": external_call_number_to_transfer}
        phones_transfer = None

    if call_status == "ended":
        # Hang up from external
        twilio.hang_up(twilio_call_sid)
        assert twilio.waiting_until_call_status(twilio_call_sid, "completed", 20)

        assert wait_until_queue_octopus_is_empty(company_id)

        ret = public_api.transfers(current_call_id, payload)
        assert ret.status_code == 400

    elif call_status == "not_answered":
        assert wait_until_transfer_succeed(current_call_id, payload)

        if transfer_to in ("user", "team"):
            for phone_transfer in phones_transfer:
                assert wait_until_agent_ringing(
                    phone_transfer.user_id, phone_transfer.company_id
                )
            assert twilio.waiting_until_call_status(twilio_call_sid, "completed", 60)

            # Check ringing time is valid
            for phone_transfer in phones_transfer:
                check_ringing_time(phone_transfer, [25], [0.1])

            if transfer_to == "external":
                assert wait_until_call_answered()


@pytest.mark.webhook
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall40@aircall.io", "aircall41@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C10917478")
def test_transfer_to_user_not_available(
    reset_calls_server,
    activate_webhook,
    bulk_creation_available_agent,
    receive_call_number_transfer_one_agent,
    external_call_number,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    wait_until_call_created,
    wait_until_transfer_succeed,
):
    """
    Test company transfers to a user
    User 2 is NOT available
    * Scenario
      Test company has set up a webhook
      External caller to User 1
      Call is transferred to User 2

    * Acceptance criteria
      Call should end
      test company receives a status code 204
    """
    phone, phone_transfer_to = bulk_creation_available_agent
    # User 2 not available
    phone_transfer_to.set_availability(False)

    # External call
    twilio_call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_transfer_one_agent, 5
    )
    assert twilio.waiting_until_call_status(twilio_call_sid, "in-progress", 20)

    # Waiting for the webhook call.created
    assert wait_until_call_created()
    current_call_id = daemon_flask_server.call_created[0]["data"]["id"]

    # Transfer to user 2
    payload = {"user_id": phone_transfer_to.user_id}
    assert wait_until_transfer_succeed(current_call_id, payload)

    # Call should end directly
    assert twilio.waiting_until_call_status(
        call_sid=twilio_call_sid, exp_status="completed", timeout=10
    )


@pytest.mark.webhook
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall20@aircall.io"]],
    indirect=True,
)
@pytest.mark.parametrize(
    "payload,error_code",
    (
        pytest.param({}, 400, marks=pytest.mark.testrail(ids=("C10916054",))),
        pytest.param(
            {"wrong_field": "979"}, 400, marks=pytest.mark.testrail(ids=("C10917563",))
        ),
        pytest.param(
            {"user_id": "979"}, 404, marks=pytest.mark.testrail(ids=("C10917564",))
        ),
        pytest.param(
            {"team_id": "41815"}, 404, marks=pytest.mark.testrail(ids=("C10917565",))
        ),
        pytest.param(
            {"number": "this is not a number"},
            400,
            marks=pytest.mark.testrail(ids=("C10917566",)),
        ),
        pytest.param(
            {"user_id": "41813", "dispatching_strategy": "random"},
            400,
            marks=pytest.mark.testrail(ids=("C11031465",)),
        ),
        pytest.param(
            {"external_number": "+33698661234", "dispatching_strategy": "simultaneous"},
            400,
            marks=pytest.mark.testrail(ids=("C11031466",)),
        ),
    ),
)
def test_transfer_with_wrong_payload(
    reset_calls_server,
    activate_webhook,
    bulk_creation_available_agent,
    receive_call_number_transfer_one_agent,
    external_call_number,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    wait_until_call_created,
    payload,
    error_code,
):
    """
    Test company makes a transfer with a wrong payload or invalid user/team/number
    * Scenario
      Test company has set up a webhook
      External caller to User 1
      Call is transfered with a wrong payload or invalid user/team/number

    * Acceptance criteria
      test company receives a status code 4xx
    """
    _ = bulk_creation_available_agent

    # External call
    twilio_call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_transfer_one_agent, 5
    )
    assert twilio.waiting_until_call_status(twilio_call_sid, "in-progress", 20)

    # Waiting for the webhook call.created
    assert wait_until_call_created()
    current_call_id = daemon_flask_server.call_created[-1]["data"]["id"]

    ret = public_api.transfers(current_call_id, payload)

    assert ret.status_code == error_code, ret.text

    twilio.hang_up(twilio_call_sid)
    assert twilio.waiting_until_call_status(twilio_call_sid, "completed", 20)


@pytest.mark.webhook
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall13@aircall.io", "aircall40@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C10916049")
def test_transfer_to_ivr(
    reset_calls_server,
    activate_webhook,
    bulk_creation_available_agent,
    receive_call_number_transfer_one_agent,
    external_call_number,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    ivr_number,
    wait_until_agent_ringing,
    wait_until_transfer_succeed,
    wait_until_call_created,
    wait_until_call_answered,
):
    """
    Test company makes a transfer to an IVR number
    * Scenario
      Test company has set up a webhook
      External caller to User 1
      Call is transfered to an IVR number

    * Acceptance criteria
      External is well redirected to IVR and able to press 1
      And redirected to Option 1
      Test company receives a status code 204
    """
    phone_ivr, _ = bulk_creation_available_agent

    # External call
    twilio_call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_transfer_one_agent, 5
    )
    assert twilio.waiting_until_call_status(twilio_call_sid, "in-progress", 20)

    # Waiting for the webhook call.created
    assert wait_until_call_created()
    current_call_id = daemon_flask_server.call_created[-1]["data"]["id"]

    payload = {"number": ivr_number}
    log.info(payload)
    assert wait_until_transfer_succeed(current_call_id, payload)

    # Wait for the call to be answered by IVR
    assert wait_until_call_answered()

    # Redirect to phone_ivr (Option 1)
    twilio.press_digits("1", twilio_call_sid)

    assert wait_until_agent_ringing(phone_ivr.user_id, phone_ivr.company_id)


@pytest.mark.webhook
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall40@aircall.io", "aircall41@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C10916057")
def test_transfer_to_team_with_previous_user(
    check_number_two_agents_simultaneous,
    reset_calls_server,
    activate_webhook,
    bulk_creation_available_agent,
    team_to_transfer_with_previous_agent,
    check_ringing_time,
    receive_call_number_transfer_one_agent,
    external_call_number,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    wait_until_agent_ringing,
    wait_until_agent_not_ringing_during,
    wait_until_transfer_succeed,
    wait_until_call_created,
):
    """
    Test company makes a transfer to a team with previous agent inside
    * Scenario
      Test company has set up a webhook
      External caller to User 1
      Call is transfered to Team A (User 1 and User 2)

    * Acceptance criteria
      Test company receives a status code 204
      Call only rings on user 2
    """
    phone1, phone2 = bulk_creation_available_agent

    # External call
    twilio_call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_transfer_one_agent, 5
    )
    assert twilio.waiting_until_call_status(twilio_call_sid, "in-progress", 20)

    # Waiting for the webhook call.created
    assert wait_until_call_created()
    current_call_id = daemon_flask_server.call_created[-1]["data"]["id"]

    payload = {"team_id": team_to_transfer_with_previous_agent}
    assert wait_until_transfer_succeed(current_call_id, payload)

    assert wait_until_agent_ringing(phone2.user_id, phone2.company_id)

    assert wait_until_agent_not_ringing_during(phone1.user_id, phone1.company_id, 20)

    assert twilio.waiting_until_call_status(twilio_call_sid, "completed", 20)

    check_ringing_time(phone2, [25], [0.1])


@pytest.mark.webhook
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall40@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C10916058")
def test_transfer_to_same_agent(
    reset_calls_server,
    activate_webhook,
    bulk_creation_available_agent,
    check_ringing_time,
    receive_call_number_transfer_one_agent,
    external_call_number,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    wait_until_transfer_succeed,
    wait_until_call_created,
):
    """
    Test company makes a transfer to the same agent
    * Scenario
      Test company has set up a webhook
      External caller to User 1
      Call is transfered to User 1

    * Acceptance criteria
      Test company receives a status code 204
      Call ends directly after the response
    """
    (phone,) = bulk_creation_available_agent

    # External call
    twilio_call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_transfer_one_agent, 5
    )
    assert twilio.waiting_until_call_status(twilio_call_sid, "in-progress", 20)

    # Waiting for the webhook call.created
    assert wait_until_call_created()
    current_call_id = daemon_flask_server.call_created[-1]["data"]["id"]

    payload = {"user_id": phone.user_id}
    assert wait_until_transfer_succeed(current_call_id, payload)

    # Call should end quickly
    assert twilio.waiting_until_call_status(
        call_sid=twilio_call_sid, exp_status="completed", timeout=15
    )


@pytest.mark.webhook
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall20@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C10916055")
def test_transfer_to_team_with_4_users(
    reset_calls_server,
    activate_webhook,
    bulk_creation_available_agent,
    team_4_users,
    check_ringing_time,
    receive_call_number_transfer_one_agent,
    external_call_number,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    wait_until_call_created,
):
    """
    Test company makes a transfer to a team with previous agent inside
    * Scenario
      Test company has set up a webhook
      External caller to User 1
      Call is transfered to Team A with 4 agents (limitation in Staging is 3)

    * Acceptance criteria
      Test company receives a status code 400
    """
    _ = bulk_creation_available_agent

    # External call
    twilio_call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_transfer_one_agent, 5
    )
    assert twilio.waiting_until_call_status(twilio_call_sid, "in-progress", 20)

    # Waiting for the webhook call.created
    assert wait_until_call_created()
    current_call_id = daemon_flask_server.call_created[-1]["data"]["id"]

    payload = {"team_id": team_4_users}
    ret = public_api.transfers(current_call_id, payload)

    assert ret.status_code == 400, ret.text


@pytest.mark.webhook
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["aircall20@aircall.io", "aircall21@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C10925713")
def test_transfer_to_number_out_opening_hours(
    reset_calls_server,
    activate_webhook,
    bulk_creation_available_agent,
    receive_call_number_transfer_one_agent,
    external_call_number,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    wait_until_transfer_succeed,
    wait_until_call_created,
    out_of_opening_hours_number,
    wait_until_agent_not_ringing_during,
):
    """
    Test company makes a transfer to out of opening hours number
    * Scenario
      Test company has set up a webhook
      External caller to User 1
      Call is transfered to out of opening hours number

    * Acceptance criteria
      Test company receives a status code 200
      Call ends directly after the response
    """
    (phone1, phone2) = bulk_creation_available_agent

    # External call
    twilio_call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_transfer_one_agent, 5
    )
    assert twilio.waiting_until_call_status(twilio_call_sid, "in-progress", 20)

    # Waiting for the webhook call.created
    assert wait_until_call_created()
    current_call_id = daemon_flask_server.call_created[-1]["data"]["id"]

    payload = {"number": out_of_opening_hours_number}
    assert wait_until_transfer_succeed(current_call_id, payload)
    assert wait_until_agent_not_ringing_during(phone2.user_id, phone2.company_id, 20)

    # Call should end quickly
    assert twilio.waiting_until_call_status(
        call_sid=twilio_call_sid, exp_status="completed", timeout=15
    )


@pytest.mark.webhook
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [
        [
            "aircall10@aircall.io",
            "aircall11@aircall.io",
            "aircall12@aircall.io",
            "aircall40@aircall.io",
        ]
    ],
    indirect=True,
)
@pytest.mark.parametrize(
    "dispatching_strategy",
    (
        pytest.param("random", marks=pytest.mark.testrail(ids=("C11031462",))),
        pytest.param("simultaneous", marks=pytest.mark.testrail(ids=("C11031463",))),
        pytest.param(None, marks=pytest.mark.testrail(ids=("C11031464",))),
    ),
)
def test_transfer_to_team_with_dispatching_strategy(
    reset_calls_server,
    activate_webhook,
    bulk_creation_available_agent,
    receive_call_number_transfer_one_agent,
    external_call_number_to_transfer,
    external_call_number,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    team_to_transfer,
    ringing_order,
    wait_until_agent_ringing,
    wait_until_transfer_succeed,
    wait_until_call_created,
    wait_until_call_answered,
    wait_until_queue_octopus_is_empty,
    check_ringing_time,
    dispatching_strategy,
):
    """
    Test company transfers to a Team A using dispatching strategy option
    All agents of team A are available
    * Scenario
      Test company has set up a webhook
      External caller to User 1
      Call is transfered to Team A using dispatching strategy option

    * Acceptance criteria
    if dispatching strategy is set:
      Call is not answered thus dispatching strategy is applied to team A (random / simultaneous)
    if dispatching strategy is not set:
      Simultaneous strategy is applied

    Test company receives a status code 204

    """
    phones_transfer = bulk_creation_available_agent

    if dispatching_strategy in ("random", "simultaneous"):
        payload = {
            "team_id": team_to_transfer,
            "dispatching_strategy": dispatching_strategy,
        }
    else:
        payload = {"team_id": team_to_transfer}

    # External call
    twilio_call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_transfer_one_agent, 5
    )
    assert twilio.waiting_until_call_status(twilio_call_sid, "in-progress", 20)

    # Waiting for the webhook call.created
    assert wait_until_call_created()
    current_call_id = daemon_flask_server.call_created[-1]["data"]["id"]

    assert wait_until_transfer_succeed(current_call_id, payload)

    assert twilio.waiting_until_call_status(twilio_call_sid, "completed", 120)

    timings = [phone.received_w_time[0][1] for phone in phones_transfer[:-1]]
    timings = sorted(timings)
    ringing_gaps = compute_gap_between_times(timings)

    if dispatching_strategy == "random":
        # Ensure ringing timestamps are spaced out from 20 to 30 seconds
        assert all(20 < gap < 30 for gap in ringing_gaps), ringing_gaps

    else:  # Simultaneous strategy
        # Ensure ringing timestamps are "simulatenous"
        assert all(0 < gap < 3 for gap in ringing_gaps), ringing_gaps

    for phone_transfer in phones_transfer[:-1]:
        check_ringing_time(phone_transfer, [25], [0.2])


@pytest.mark.webhook
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [
        [
            "aircall10@aircall.io",
            "aircall11@aircall.io",
            "aircall12@aircall.io",
            "aircall20@aircall.io",
        ]
    ],
    indirect=True,
)
@pytestrail.case("C11032087")
def test_transfer_twice(
    reset_calls_server,
    activate_webhook,
    bulk_creation_available_agent,
    check_ringing_time,
    receive_call_number_transfer_one_agent,
    external_call_number,
    external_call_number_to_transfer,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    team_to_transfer,
    wait_until_transfer_succeed,
    wait_until_call_created,
):
    """
    Test company makes a transfer to a team then to an external number
    * Scenario
        Test company has set up a webhook
        External caller to User 1
        Call is transferred to team 1
        Call is transferred to an external number

    * Acceptance criteria
        Test company receives a status code 204 (first transfer)
        Test company receives a status code 204 (second transfer)
        2nd transfer must fail and the call must still ring on team 1
    """
    phones_transfer = bulk_creation_available_agent

    # External call
    twilio_call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_transfer_one_agent, 3
    )
    assert twilio.waiting_until_call_status(twilio_call_sid, "in-progress", 20)

    # Waiting for the webhook call.created
    assert wait_until_call_created()
    current_call_id = daemon_flask_server.call_created[-1]["data"]["id"]

    payload = {"team_id": team_to_transfer}
    assert wait_until_transfer_succeed(current_call_id, payload)

    log.info("2nd transfer to {0}".format(external_call_number_to_transfer))
    payload = {"number": external_call_number_to_transfer}

    ret = public_api.transfers(current_call_id, payload)

    assert ret.status_code == 204, ret.text

    for phone_transfer in phones_transfer[:-1]:
        check_ringing_time(phone_transfer, [25], [0.2])

    assert twilio.waiting_until_call_status(twilio_call_sid, "completed", 120)


@pytest.mark.webhook
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [
        [
            "aircall10@aircall.io",
            "aircall11@aircall.io",
            "aircall12@aircall.io",
            "aircall20@aircall.io",
        ]
    ],
    indirect=True,
)
@pytestrail.case("C11032086")
def test_transfer_and_hangup(
    reset_calls_server,
    activate_webhook,
    bulk_creation_available_agent,
    check_ringing_time,
    receive_call_number_transfer_one_agent,
    external_call_number,
    external_call_number_to_transfer,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    team_to_transfer,
    transfer_to_one_agent_id,
    wait_until_transfer_succeed,
    wait_until_call_created,
    wait_until_call_status,
):
    """
    Context:
    https://aircall-product.atlassian.net/browse/TEL-819
    API transfer won't go through and Calls stuck as Initial

    Test company makes a transfer to a team then to an external number,
    the callee hangs up before the transfer is done

    * Scenario
      Test company has set up a webhook
      External caller to User 1
      Call is transferred to an external number
      External caller hang up before the transfer is done

    * Acceptance criteria
      Test company receives a status code 204
      Checking the call info on CC, the call state must be `done`
    """
    phones_transfer = bulk_creation_available_agent

    # External call
    twilio_call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_transfer_one_agent, 3
    )
    assert twilio.waiting_until_call_status(twilio_call_sid, "in-progress", 20)

    # Waiting for the webhook call.created
    assert wait_until_call_created()
    current_call_id = daemon_flask_server.call_created[-1]["data"]["id"]

    # Transfer to an external number
    payload = {"number": external_call_number_to_transfer}
    ret = public_api.transfers(current_call_id, payload)
    assert ret.status_code == 204, ret.text

    # Quick wait and hang up
    time.sleep(1)
    twilio.hang_up(twilio_call_sid)

    # Ensure call state is done
    assert wait_until_call_status(current_call_id, transfer_to_one_agent_id, "done")

    assert twilio.waiting_until_call_status(twilio_call_sid, "completed", 120)
