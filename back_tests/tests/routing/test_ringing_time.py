"""
Be able to customize ringing time in CC
https://aircall-product.atlassian.net/browse/TEL-1007
"""

import logging

import pytest
from pytest_lazyfixture import lazy_fixture
from pytest_testrail.plugin import pytestrail

from back_tests.framework.common.phone import Phone
from back_tests.framework.common.twilio import twilio


log = logging.getLogger("TEST")


@pytest.mark.parametrize(
    "custom_ringing_time_number,expected_timing",
    [
        pytest.param(
            lazy_fixture("custom_ringing_time_number_w_RQT"),
            (25, 15, 60),
            marks=pytest.mark.testrail(ids=("C11051253",)),
        ),
        pytest.param(
            lazy_fixture("custom_ringing_time_number_wo_RQT"),
            (25, 15, 60),
            marks=pytest.mark.testrail(ids=("C11051254",)),
        ),
    ],
)
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [
        [
            "ringing_time_0@aircall.io",
            "ringing_time_15@aircall.io",
            "ringing_time_60@aircall.io",
            "ringing_time_90@aircall.io",
        ]
    ],
    indirect=True,
)
def test_ringing_time_nominal(
    bulk_creation_available_agent,
    check_ringing_time,
    wait_until_agent_ringing,
    wait_until_agent_not_ringing,
    custom_ringing_time_number,
    external_call_number,
    expected_timing,
):
    """
    * Distrib:
    Agent 1 - 0 second (25 seconds default value thus)
    Agent 2 - 15 seconds
    Agent 3 - 60 seconds
    Team (Agent1, Agent2, Agent4) - 20 seconds simultaneously

    * Scenario:
    - Agent 1 & 2 & 3 & 4 Available
    - trigger 1 call

    * Acceptance criteria
    - Agent 1 rings during 25 seconds
    - Agent 2 rings during 15 seconds
    - Agent 3 rings during 60 seconds
    - Agent1, Agent2, Agent4 ring during 20 seconds
    """

    # Login with all agents
    phones = phone_0, phone_15, phone_60, phone_90 = bulk_creation_available_agent
    user_ids = [phone.user_id for phone in phones]
    company_id = phone_0.company_id

    time_0, time_15, time_60 = expected_timing

    error_percentage = [
        0.3,
        0.3,
    ]  # We set 30% as error rate to avoid too many flaky results

    # Trigger an inbound call
    call_sid = twilio.inbound_call(external_call_number, custom_ringing_time_number, 5)

    # The call is reaching the agent one by one
    for user_id in user_ids[:-1]:  # Last agent is only in the second distribution item
        assert wait_until_agent_ringing(user_id, company_id)
        assert wait_until_agent_not_ringing(
            user_id=user_id, company_id=company_id, timeout=90
        )

    # The call is reaching the Team item
    assert (
        wait_until_agent_ringing(phone_0.user_id, company_id)
        and wait_until_agent_ringing(phone_15.user_id, company_id)
        and wait_until_agent_ringing(phone_90.user_id, company_id)
    )

    # Wait for the call to be completed
    assert twilio.waiting_until_call_status(
        call_sid=call_sid, exp_status="completed", timeout=180, loop_timeout=1
    )

    # Check ringing times
    check_ringing_time(phone_0, [time_0, 20], error_percentage)
    check_ringing_time(phone_15, [time_15, 20], error_percentage)
    check_ringing_time(phone_60, [time_60], error_percentage)
    check_ringing_time(phone_90, [20], error_percentage)
