import logging
import pytest

from back_tests.framework.user_management.invite import UserManagementInvite
from back_tests.framework.user_management.email_update import UserManagementEmailUpdate

from pytest_testrail.plugin import pytestrail

log = logging.getLogger("TESTUSM")

pytestmark = pytest.mark.usermanagement_test


@pytest.mark.parametrize(
    "bulk_usm_sign_in",
    [["nur.sener+1204202103@aircall.io"]],
    indirect=True,
)
@pytestrail.case("C11051679")
def test_user_invite_updates(
    user_management_invite_firstname,
    user_management_invite_lastname,
    user_management_invite_isAdmin,
    user_management_invite_language,
    bulk_usm_sign_in,
    create_user_management_invite_email,
):
    """
    * Scenario
      1. Sign in with admin user which is defined as the Authorized company in USE_NEW_AUTHENTICATION_SERVICE FF
      2. Catch idToken
      3. Send invitation
      4. Update Email
      5. Sign out

    * Acceptance criteria
      1. Sign in successfully with cognito user
      2. Invite a user with cognito idToken
      3. Update email with cognito idToken
      4. Sign out successfully
    """
    (UserManagement1,) = bulk_usm_sign_in

    # Login with cognito user

    UserManagementInvite1 = UserManagementInvite()
    UserManagementInvite1.sendInvitation(
        user_management_invite_firstname,
        user_management_invite_lastname,
        user_management_invite_isAdmin,
        user_management_invite_language,
        create_user_management_invite_email(5),
        UserManagement1.idToken,
    )

    userId = UserManagementInvite1.id
    log.info("response of invitation : {0}".format(userId))

    UserManagementInvite1.resendInvitation(UserManagement1.idToken, userId)
    assert UserManagementInvite1.resend_response_status == 204

    UserManagementEmailUpdate1 = UserManagementEmailUpdate()
    UserManagementEmailUpdate1.usm_email_update(
        userId, create_user_management_invite_email(3), UserManagement1.idToken
    )
    assert UserManagementEmailUpdate1.response_status == 200
