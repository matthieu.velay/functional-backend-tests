import logging
import time

import pytest

from back_tests.framework.common.phone import Phone
from back_tests.framework.messaging.messaging_internalapi import Messaging

from pytest_testrail.plugin import pytestrail
from pytest_lazyfixture import lazy_fixture

log = logging.getLogger("TEST")


@pytest.mark.parametrize(
    "line_id, number1, text, error_msg ",
    (
        pytest.param(
            lazy_fixture("messaging_us_line_id"),
            lazy_fixture("messaging_external_us_number"),
            lazy_fixture("empty_text"),
            lazy_fixture("internalapi_empty_text_error_msg"),
            marks=[
                pytest.mark.testrail(ids=("C10925644",)),
                pytest.mark.smoke_test_internalapi,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_us_line_id"),
            lazy_fixture("messaging_external_us_number"),
            lazy_fixture("text_with_space"),
            lazy_fixture("internalapi_text_space_error_msg"),
            marks=[
                pytest.mark.testrail(ids=("C10925645",)),
                pytest.mark.smoke_test_internalapi,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_us_line_id"),
            lazy_fixture("empty_ext_num"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("internalapi_empty_ext_num_error_msg"),
            marks=[
                pytest.mark.testrail(ids=("C10925646",)),
                pytest.mark.smoke_test_internalapi,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_us_line_id"),
            lazy_fixture("messaging_wrong_external_number"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("internalapi_wrong_ext_num_error_msg"),
            marks=[
                pytest.mark.testrail(ids=("C10925647",)),
                pytest.mark.smoke_test_internalapi,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_us_line_id"),
            lazy_fixture("messaging_same_number_with_us_line_id"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("internalapi_agent_itself_error_msg"),
            marks=[
                pytest.mark.testrail(ids=("C10925649",)),
                pytest.mark.smoke_test_internalapi,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_non_covered_area_line_id"),
            lazy_fixture("messaging_external_us_number"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("internalapi_uncovered_country_error_msg"),
            marks=[
                pytest.mark.testrail(ids=("C10925652",)),
                pytest.mark.smoke_test_internalapi,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_gb_line_id"),
            lazy_fixture("messaging_external_us_number"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("internalapi_between_internationl_messages_error_msg"),
            marks=[
                pytest.mark.testrail(ids=("C10953084",)),
                pytest.mark.smoke_test_internalapi,
            ],
        ),
    ),
    ids=[
        "sms-internalapi-empty-text",
        "sms-internalapi-text-with-space",
        "sms-internalapi-text-empty-external-number",
        "sms-internalapi-text-wrong-external-number",
        "sms-internalapi-to-same-number",
        "sms-internalapi-uncovered-country",
        "sms-internalapi-internatiol-between-countries",
    ],
)
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["messagingtest3@aircall.io"]],
    indirect=True,
)
def test_sms_internalapi_errors(
    request,
    line_id,
    number1,
    text,
    error_msg,
    bulk_creation_available_agent,
):
    """
    * Scenario
      1. Login with agent A which is inside the Authorized company in enable_sms FF
      2. Catch cognito Token
      3. Send SMS with errors

    * Acceptance criteria
      1. Send SMS successfully with agent authorization token and cognito token

      2. error must received

      3. Confirm error message consistency
    """

    # Login with Agent A and set it available
    (phone1,) = bulk_creation_available_agent

    # catch cognito token for Agent A
    messaging2 = Messaging(phone1.auth_token)

    # Send SMS with ExternalNumber, LineID and Text
    messaging2.sendMessage(number1, line_id, text)

    # Check consistency of error message with assertion
    assert (
        messaging2.error_message == error_msg
        or messaging2.client_error_message == error_msg
    )


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["messagingtest3@aircall.io"]],
    indirect=True,
)
@pytest.mark.smoke_test_internalapi
@pytestrail.case("C10925648")
def test_sms_outbound_wrong_lineid(
    bulk_creation_available_agent, messaging_external_us_number, messaging_wrong_line_id
):
    """
    * Scenario
      1. Login with agent A which is inside the Authorized company in enable_sms FF
      2. Catch cognito Token
      3. Send SMS with wrong LineID
      4. Compare Error Message

    * Acceptance criteria
      1. Send SMS successfully with agent authorization token and cognito token

      2. error must received

      3. Confirm error message consistency
    """
    # Login with Agent A and set it available
    (phone1,) = bulk_creation_available_agent

    # catch cognito token for Agent A
    messaging2 = Messaging(phone1.auth_token)

    # Send SMS with ExternalNumber, LineID and Text
    messaging2.sendMessage(
        messaging_external_us_number,
        messaging_wrong_line_id,
        "Test Automation message for outbound SMS with wrong Line Id",
    )

    # Check consistency of error message with assertion
    assert messaging2.client_error_message == (
        "Agent {0} is not assigned to the line 13445".format(phone1.user_id)
    )


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["messagingtest3@aircall.io"]],
    indirect=True,
)
@pytest.mark.smoke_test_internalapi
@pytestrail.case("C10925653")
def test_sms_outbound_1601_char(
    bulk_creation_available_agent,
    messaging_external_us_number,
    messaging_us_line_id,
    create_huge_string,
    internalapi_chars_1601_messages_error_msg,
):
    """
    * Scenario
      1. Login with agent A which is inside the Authorized company in enable_sms FF
      2. Catch cognito Token
      3. Send SMS with more than 1600 char
      4. Compare Error Message

    * Acceptance criteria
      1. Send SMS successfully with agent authorization token and cognito token

      2. error must received

      3. Confirm error message consistency
    """
    # Login with Agent A and set it available
    (phone1,) = bulk_creation_available_agent

    # catch cognito token for Agent A
    messaging2 = Messaging(phone1.auth_token)

    # Send SMS with ExternalNumber, LineID and Text
    messaging2.sendMessage(
        messaging_external_us_number, messaging_us_line_id, create_huge_string(1601)
    )

    # Check consistency of error message with assertion
    assert messaging2.client_error_message == internalapi_chars_1601_messages_error_msg


@pytest.mark.parametrize(
    "bulk_creation_available_agent", [["messagingtest6@aircall.io"]], indirect=True
)
@pytest.mark.smoke_test_internalapi
@pytestrail.case("C10925651")
def test_sms_outbound_unauth_company(
    bulk_creation_available_agent,
    messaging_external_us_number,
    messaging_unauth_line_id,
    internalapi_unauth_company_error_msg,
):
    """
    * Scenario
      1. Login with agent A which is inside the Authorized company in enable_sms FF
      2. Catch cognito Token
      3. Send SMS with unauthorized company
      4. Compare Error Message

    * Acceptance criteria
      1. Send SMS successfully with agent authorization token and cognito token

      2. error must received

      3. Confirm error message consistency
    """
    # Login with Agent A and set it available
    (phone1,) = bulk_creation_available_agent

    # catch cognito token for Agent A
    messaging2 = Messaging(phone1.auth_token)

    # Send SMS with ExternalNumber, LineID and Text
    messaging2.sendMessage(
        messaging_external_us_number,
        messaging_unauth_line_id,
        "Test Automation message for outbound SMS with Unauthorised Company ID",
    )

    # Check consistency of error message with assertion
    assert messaging2.client_error_message == internalapi_unauth_company_error_msg


@pytest.mark.flaky(reruns=2, reruns_delay=2)
@pytest.mark.parametrize(
    "number1, line_id1, number2, line_id2",
    (
        pytest.param(
            lazy_fixture("messaging_external_us_number1"),
            lazy_fixture("messaging_us_line_id"),
            lazy_fixture("messaging_external_us_number"),
            lazy_fixture("messaging_us_line_id1"),
            marks=[
                pytest.mark.testrail(ids=("C10925643",)),
                pytest.mark.smoke_test_internalapi,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_gb_number1"),
            lazy_fixture("messaging_gb_line_id"),
            lazy_fixture("messaging_gb_number"),
            lazy_fixture("messaging_gb_line_id1"),
            marks=[
                pytest.mark.testrail(ids=("C10953080",)),
                pytest.mark.smoke_test_internalapi,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_canadian_number1"),
            lazy_fixture("messaging_canadian_line_id"),
            lazy_fixture("messaging_canadian_number"),
            lazy_fixture("messaging_canadian_line_id1"),
            marks=[
                pytest.mark.testrail(ids=("C11033876",)),
                pytest.mark.smoke_test_internalapi,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_australian_number1"),
            lazy_fixture("messaging_australian_line_id"),
            lazy_fixture("messaging_australian_number"),
            lazy_fixture("messaging_australian_line_id1"),
            marks=[
                pytest.mark.testrail(ids=("C11033877",)),
                pytest.mark.smoke_test_internalapi,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_french_number1"),
            lazy_fixture("messaging_french_line_id"),
            lazy_fixture("messaging_french_number"),
            lazy_fixture("messaging_french_line_id1"),
            marks=[
                pytest.mark.testrail(ids=("C11033878",)),
                pytest.mark.sanity_test_internalapi,
            ],
        ),
    ),
    ids=[
        "sms-outbound-US-to-US",
        "sms-outbound-GB-to-GB",
        "sms-outbound-CA-to-CA",
        "sms-outbound-AU-to-AU",
        "sms-outbound-FR-to-FR",
    ],
)
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["messagingtest3@aircall.io", "messagingtest4@aircall.io"]],
    indirect=True,
)
def test_sms_outbound(
    request,
    number1,
    line_id1,
    number2,
    line_id2,
    bulk_creation_available_agent,
    messaging_backend_company_id,
):
    """
    * Scenario
      1. Login with agent A which is inside the Authorized company in enable_sms FF
      2. Catch cognito Token
      3. Send SMS
      4. Retrieve SMS status with ConverstaionID
      5. Compare message_id send and receive
      6. Logout agent A

    * Acceptance criteria
      1. Send SMS successfully with agent authorization token and cognito token

      2. Messages status updated to delivered

      3. Confirm message_id consistency to ensure same message received
    """

    # Login with Agent A and set it available
    phone1, phone2 = bulk_creation_available_agent

    # catch cognito token for Agent A
    messaging1 = Messaging(phone1.auth_token)
    messaging2 = Messaging(phone2.auth_token)

    # Send SMS with ExternalNumber, LineID and Text
    messaging1.sendMessage(
        number1,
        line_id1,
        "Test Automation message for outbound SMS",
    )

    log.info("-----SENDER-----")
    # Retrieve SMS with Conversation ID, expected status and timeout time
    messaging1.getConversation(
        messaging1.message_id, messaging1.conversation_id, "delivered", 60
    )

    # Check consistency of message status with assertion
    assert messaging1.message_status == "delivered"

    # Retrieve SMS history with querysize
    messaging1.getListConversationsQuery(2, messaging1.message_id, 60)

    # Check consistency of message id with assertion
    assert messaging1.message_id == messaging1.history_id

    log.info("-----RECEIVER-----")
    messaging2.conversation_id = "{0}|{1}|{2}".format(
        number2, line_id2, messaging_backend_company_id
    )

    messaging2.getConversationMessagingEndpoint(
        messaging2.conversation_id, "received", 60
    )

    # Check consistency of message status with assertion
    assert messaging2.message_status == "received"

    # Retrieve SMS history with querysize
    messaging2.getListConversationsQuery(2, messaging2.retrieve_message_id, 60)

    # Check consistency of message id with assertion
    assert messaging2.retrieve_message_id == messaging2.history_id

    log.info("-----BEFORE ACK-----")
    messaging2.listUnReadConversations(1, messaging2.retrieve_message_id, 60)

    assert messaging2.unread_message_direction_before == "received"
    unread_conversation_num = messaging2.unread_conversation_num_before

    messaging2.acknowledgeMessage(
        messaging2.conversation_id, messaging2.retrieve_message_id
    )

    log.info("-----AFTER ACK-----")
    messaging2.listReadConversations(1, messaging2.retrieve_message_id, 0, 60)

    assert messaging2.unread_conversation_num == unread_conversation_num - 1
    assert messaging2.unread_message_num == 0


@pytest.mark.flaky(reruns=2, reruns_delay=2)
@pytest.mark.parametrize(
    "number1, line_id1, number2, line_id2",
    (
        pytest.param(
            lazy_fixture("messaging_external_us_number1"),
            lazy_fixture("messaging_us_line_id"),
            lazy_fixture("messaging_external_us_number"),
            lazy_fixture("messaging_us_line_id1"),
            marks=[
                pytest.mark.testrail(ids=("C10925650",)),
                pytest.mark.smoke_test_internalapi,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_gb_number1"),
            lazy_fixture("messaging_gb_line_id"),
            lazy_fixture("messaging_gb_number"),
            lazy_fixture("messaging_gb_line_id1"),
            marks=[
                pytest.mark.testrail(ids=("C10953081",)),
                pytest.mark.smoke_test_internalapi,
            ],
        ),
    ),
    ids=["sms-backend-1600-chars-US", "sms-backend-1600-chars-GB"],
)
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["messagingtest3@aircall.io", "messagingtest4@aircall.io"]],
    indirect=True,
)
def test_sms_outbound_with_1600char(
    request,
    number1,
    line_id1,
    number2,
    line_id2,
    bulk_creation_available_agent,
    create_huge_string,
    messaging_backend_company_id,
):
    """
    * Scenario
      1. Login with agent A which is inside the Authorized company in enable_sms FF
      2. Catch cognito Token
      3. Send SMS with 1600(max) char
      4. Retrieve SMS status with ConverstaionID
      5. Compare message_id send and receive
      6. Logout agent A

    * Acceptance criteria
      1. Send SMS successfully with agent authorization token and cognito token

      2. Messages status updated to delivered

      3. Confirm message_id consistency to ensure same message received
    """

    # Login with Agent A and set it available
    phone1, phone2 = bulk_creation_available_agent

    # catch cognito token for Agent A
    messaging3 = Messaging(phone1.auth_token)
    messaging4 = Messaging(phone2.auth_token)

    # Send SMS with ExternalNumber, LineID and Text
    messaging3.sendMessage(number1, line_id1, create_huge_string(1600))

    log.info("-----SENDER-----")
    # Retrieve SMS with Conversation ID, expected status and timeout time
    messaging3.getConversation(
        messaging3.message_id, messaging3.conversation_id, "delivered", 60
    )

    # Check consistency of message status with assertion
    assert messaging3.message_status == "delivered"

    # Retrieve SMS history with querysize
    messaging3.getListConversationsQuery(2, messaging3.message_id, 60)

    # Check consistency of message id with assertion
    assert messaging3.message_id == messaging3.history_id

    log.info("-----RECEIVER-----")
    messaging4.conversation_id = "{0}|{1}|{2}".format(
        number2, line_id2, messaging_backend_company_id
    )

    messaging4.getConversationMessagingEndpoint(
        messaging4.conversation_id, "received", 60
    )

    # Check consistency of message status with assertion
    assert messaging4.message_status == "received"

    # Retrieve SMS history with querysize
    messaging4.getListConversationsQuery(2, messaging4.retrieve_message_id, 60)

    # Check consistency of message id with assertion
    assert messaging4.retrieve_message_id == messaging4.history_id

    log.info("-----BEFORE ACK-----")
    messaging4.listUnReadConversations(1, messaging4.retrieve_message_id, 60)

    assert messaging4.unread_message_direction_before == "received"
    unread_conversation_num = messaging4.unread_conversation_num_before

    messaging4.acknowledgeMessage(
        messaging4.conversation_id, messaging4.retrieve_message_id
    )

    log.info("-----AFTER ACK-----")
    messaging4.listReadConversations(1, messaging4.retrieve_message_id, 0, 60)

    assert messaging4.unread_conversation_num == unread_conversation_num - 1
    assert messaging4.unread_message_num == 0


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["messagingtest3@aircall.io", "messagingtest4@aircall.io"]],
    indirect=True,
)
@pytest.mark.smoke_test_internalapi
@pytestrail.case("C11033879")
def test_sms_retrieve_with_lineandnumber(
    bulk_creation_available_agent,
    messaging_external_us_number1,
    messaging_us_line_id,
    messaging_backend_company_id,
    messaging_external_us_number,
    messaging_us_line_id1,
):
    """
    * Scenario
      1. Login with agent A which is inside the Authorized company in enable_sms FF
      2. Catch cognito Token
      3. Send SMS
      4. Retrieve SMS status with ConverstaionID
      5. Compare message_id send and receive
      6. Logout agent A

    * Acceptance criteria
      1. Send SMS successfully with agent authorization token and cognito token

      2. Messages status updated to delivered

      3. Confirm message_id consistency to ensure same message received
    """

    # Login with Agent A and set it available
    phone1, phone2 = bulk_creation_available_agent

    # catch cognito token for Agent A
    messaging1 = Messaging(phone1.auth_token)
    messaging2 = Messaging(phone2.auth_token)

    # Send SMS with ExternalNumber, LineID and Text
    messaging1.sendMessage(
        messaging_external_us_number1,
        messaging_us_line_id,
        "Test Automation message for outbound SMS",
    )

    log.info("-----SENDER-----=")
    # Retrieve SMS with Conversation ID, expected status and timeout time
    messaging1.getConversation(
        messaging1.message_id, messaging1.conversation_id, "delivered", 60
    )

    # Check consistency of message status with assertion
    assert messaging1.message_status == "delivered"

    log.info("-----RECEIVER-----")
    messaging2.conversation_id = "{0}|{1}|{2}".format(
        messaging_external_us_number,
        messaging_us_line_id1,
        messaging_backend_company_id,
    )

    messaging2.getConversationMessagingEndpoint(
        messaging2.conversation_id, "received", 60
    )

    # Check consistency of message status with assertion
    assert messaging2.message_status == "received"

    log.info("-----SENDER ByLineAndPhoneNumber-----=")
    # Retrieve SMS history with querysize
    messaging1.getConversationByLineAndPhoneNumber(
        messaging_us_line_id,
        messaging_external_us_number1,
        messaging1.message_id,
        20,
    )

    # Check consistency of message id with assertion
    assert messaging1.message_id == messaging1.query_message_id
    assert messaging_us_line_id == messaging1.line_id
    assert messaging_external_us_number1 == messaging1.phone_number

    log.info("-----RECEIVER ByLineAndPhoneNumber-----")
    # Retrieve SMS history with querysize
    messaging2.getConversationByLineAndPhoneNumber(
        messaging_us_line_id1,
        messaging_external_us_number,
        messaging2.retrieve_message_id,
        20,
    )

    # Check consistency of message id with assertion
    assert messaging2.retrieve_message_id == messaging2.query_message_id
    assert messaging_us_line_id1 == messaging2.line_id
    assert messaging_external_us_number == messaging2.phone_number
