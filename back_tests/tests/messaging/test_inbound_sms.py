import logging

import pytest
from twilio.base.exceptions import TwilioRestException

from back_tests.framework.common.twilio import twilio

log = logging.getLogger("TEST")

pytestmark = pytest.mark.messaging_twilio


def test_sent_sms_twilio(twilio_from_number, twilio_to_number):
    """
    * Scenario
      1. Send SMS with Twilio SDK

    * Acceptance criteria
      1. Status must be sent after sending SMS

    """
    # Send SMS from Twilio with Text, From and To
    twilio_sms_sid = twilio.inbound_sms(
        "First Test Python Automation", twilio_from_number, twilio_to_number
    )
    assert twilio.status_sms(twilio_sms_sid, "sent", 20)


def test_empty_body_sms_twilio(twilio_from_number, twilio_to_number):
    """
    * Scenario
      1. Send Empty SMS with Twilio SDK

    * Acceptance criteria
      1. Twilio returns error

    """
    with pytest.raises(TwilioRestException) as exc1:
        twilio.error_sms("", twilio_from_number, twilio_to_number)


def test_spaces_body_sms_twilio(twilio_from_number, twilio_to_number):
    """
    * Scenario
      1. Send " " SMS with Twilio SDK

    * Acceptance criteria
      1. Twilio returns error

    """
    with pytest.raises(TwilioRestException) as exc2:
        twilio.error_sms("   ", twilio_from_number, twilio_to_number)


def test_wrong_tonumber_sms_twilio(twilio_from_number, twilio_wrong_to_number):
    """
    * Scenario
      1. Send SMS with Twilio SDK to Wrong Number

    * Acceptance criteria
      1. Twilio returns error

    """
    with pytest.raises(TwilioRestException) as exc3:
        twilio.error_sms(
            "Wrong to Number SMS", twilio_from_number, twilio_wrong_to_number
        )


def test_wrong_fromnumber_sms_twilio(twilio_wrong_from_number, twilio_to_number):
    """
    * Scenario
      1. Send SMS with Twilio SDK from Wrong Number

    * Acceptance criteria
      1. Twilio returns error

    """
    with pytest.raises(TwilioRestException) as exc4:
        twilio.error_sms(
            "Wrong to Number SMS", twilio_wrong_from_number, twilio_to_number
        )
