import logging

import pytest

from back_tests.framework.messaging.messaging_endpoint import MessagingEndpoint
from back_tests.framework.messaging.messaging_receive import MessagingReceive
from back_tests.framework.messaging.messaging_update_status import MessagingUpdateStatus
from back_tests.framework.messaging.messaging_internalapi import Messaging

from pytest_testrail.plugin import pytestrail
from pytest_lazyfixture import lazy_fixture

log = logging.getLogger("TEST")


@pytest.mark.parametrize(
    "agent_id, line_id, company_id, number1, text, error_msg, status_code",
    (
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_us_line_id"),
            lazy_fixture("messaging_backend_company_id"),
            lazy_fixture("messaging_external_us_number"),
            lazy_fixture("empty_text"),
            lazy_fixture("empty_text_error_msg"),
            pytest.param(400),
            marks=[
                pytest.mark.testrail(ids=("C10817662",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_us_line_id"),
            lazy_fixture("messaging_backend_company_id"),
            lazy_fixture("messaging_external_us_number"),
            lazy_fixture("text_with_space"),
            lazy_fixture("text_space_error_msg"),
            pytest.param(400),
            marks=[
                pytest.mark.testrail(ids=("C10817670",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_us_line_id"),
            lazy_fixture("messaging_backend_company_id"),
            lazy_fixture("empty_ext_num"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("empty_ext_num_error_msg"),
            pytest.param(400),
            marks=[
                pytest.mark.testrail(ids=("C10817661",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_us_line_id"),
            lazy_fixture("messaging_backend_company_id"),
            lazy_fixture("messaging_wrong_external_number"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("wrong_ext_num_error_msg"),
            pytest.param(400),
            marks=[
                pytest.mark.testrail(ids=("C10817669",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_wrong_line_id"),
            lazy_fixture("messaging_backend_company_id"),
            lazy_fixture("messaging_external_us_number"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("wrong_line_id_error_msg"),
            pytest.param(404),
            marks=[
                pytest.mark.testrail(ids=("C10817668",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_us_line_id"),
            lazy_fixture("messaging_backend_company_id"),
            lazy_fixture("messaging_same_number_with_us_line_id"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("agent_itself_error_msg"),
            pytest.param(400),
            marks=[
                pytest.mark.testrail(ids=("C10952902",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_unauth_agent_id"),
            lazy_fixture("messaging_unauth_line_id"),
            lazy_fixture("messaging_backend_unauth_company_id"),
            lazy_fixture("messaging_external_us_number"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("unauth_company_error_msg"),
            pytest.param(403),
            marks=[
                pytest.mark.testrail(ids=("C10817667",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_non_covered_area_line_id"),
            lazy_fixture("messaging_backend_company_id"),
            lazy_fixture("messaging_external_us_number"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("uncovered_country_error_msg"),
            pytest.param(400),
            marks=[
                pytest.mark.testrail(ids=("C10952903",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_gb_line_id"),
            lazy_fixture("messaging_backend_company_id"),
            lazy_fixture("messaging_external_us_number"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("between_internationl_messages_error_msg"),
            pytest.param(400),
            marks=[
                pytest.mark.testrail(ids=("C10953087",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
    ),
    ids=[
        "sms-backend-empty-text",
        "sms-backend-text-with-space",
        "sms-backend-text-empty-external-number",
        "sms-backend-text-wrong-external-number",
        "sms-backend-text-wrong-line-id",
        "sms-backend-to-same-number",
        "sms-backend-unauth-company",
        "sms-backend-uncovered-country",
        "sms-backend-internatiol-between-countries",
    ],
)
def test_sms_backend_errors(
    request, agent_id, line_id, company_id, number1, text, error_msg, status_code
):
    """
    * Scenario
      1. Send SMS v1/messages/send with errors
      2. Check status code
      3. Check response

    * Acceptance criteria
      1. Send SMS fails

      2. Messages status code 400/403/404

      3. Confirm error message
    """
    # Trigger Send SMS
    messagingendpoint1 = MessagingEndpoint()
    messagingendpoint1.send_sms_aws4(agent_id, company_id, int(line_id), number1, text)

    # Check consistency of message status&error message with assertion
    assert messagingendpoint1.message_status == status_code.values[0]
    assert messagingendpoint1.jsonresp == error_msg


@pytest.mark.parametrize(
    "agent_id, line_id, number1",
    (
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_us_line_id"),
            lazy_fixture("messaging_external_us_number"),
            marks=[
                pytest.mark.testrail(ids=("C10817672",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_gb_line_id"),
            lazy_fixture("messaging_gb_number"),
            marks=[
                pytest.mark.testrail(ids=("C10953154",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
    ),
    ids=["sms-backend-1601-chars-US", "sms-backend-1601-chars-GB"],
)
def test_sms_1601_chars(
    request,
    agent_id,
    line_id,
    number1,
    messaging_backend_company_id,
    create_huge_string,
    chars_1601_messages_error_msg,
):
    """
    * Scenario
      1. Send SMS v1/messages/send with more than max char
      2. Check status code
      3. Check response

    * Acceptance criteria
      1. Send SMS fails

      2. Messages status code 400

      3. Confirm error message
    """

    # Trigger Send SMS
    messagingendpoint1 = MessagingEndpoint()
    messagingendpoint1.send_sms_aws4(
        agent_id,
        messaging_backend_company_id,
        int(line_id),
        number1,
        create_huge_string(1601),
    )

    # Check consistency of message status&error message with assertion
    assert messagingendpoint1.message_status == 400
    assert messagingendpoint1.jsonresp == chars_1601_messages_error_msg


@pytest.mark.flaky(reruns=2, reruns_delay=2)
@pytest.mark.parametrize(
    "agent_id,agent_id2,line_id1,number1,line_id2,number2,text",
    (
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_backend_agent_id2"),
            lazy_fixture("messaging_us_line_id"),
            lazy_fixture("messaging_external_us_number1"),
            lazy_fixture("messaging_us_line_id1"),
            lazy_fixture("messaging_external_us_number"),
            lazy_fixture("messsaging_text"),
            marks=[
                pytest.mark.testrail(ids=("C10817621",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_backend_agent_id2"),
            lazy_fixture("messaging_gb_line_id"),
            lazy_fixture("messaging_gb_number1"),
            lazy_fixture("messaging_gb_line_id1"),
            lazy_fixture("messaging_gb_number"),
            lazy_fixture("messsaging_text"),
            marks=[
                pytest.mark.testrail(ids=("C10953085",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_backend_agent_id2"),
            lazy_fixture("messaging_canadian_line_id"),
            lazy_fixture("messaging_canadian_number1"),
            lazy_fixture("messaging_canadian_line_id1"),
            lazy_fixture("messaging_canadian_number"),
            lazy_fixture("messsaging_text"),
            marks=[
                pytest.mark.testrail(ids=("C11033863",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_backend_agent_id2"),
            lazy_fixture("messaging_australian_line_id"),
            lazy_fixture("messaging_australian_number1"),
            lazy_fixture("messaging_australian_line_id1"),
            lazy_fixture("messaging_australian_number"),
            lazy_fixture("messsaging_text"),
            marks=[
                pytest.mark.testrail(ids=("C11033864",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_backend_agent_id2"),
            lazy_fixture("messaging_french_line_id"),
            lazy_fixture("messaging_french_number1"),
            lazy_fixture("messaging_french_line_id1"),
            lazy_fixture("messaging_french_number"),
            lazy_fixture("messsaging_text"),
            marks=[
                pytest.mark.testrail(ids=("C11033865",)),
                pytest.mark.sanity_test_messaging,
            ],
        ),
    ),
    ids=[
        "sms-backend-US",
        "sms-backend-GB",
        "sms-backend-CA",
        "sms-backend-AU",
        "sms-backend-FR",
    ],
)
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["messagingtest1@aircall.io", "messagingtest2@aircall.io"]],
    indirect=True,
)
def test_sms_backend(
    request,
    agent_id,
    agent_id2,
    line_id1,
    number1,
    line_id2,
    number2,
    text,
    bulk_creation_available_agent,
    messaging_backend_company_id,
):
    """
    * Scenario
      1. Send SMS v1/messages/send
      2. Check status code
      3. Check response
      4. Retrieve SMS status with ConversationID
      5. Compare message_id send and receive
      6. Logout agent A

    * Acceptance criteria
      1. Send SMS successfully

      2. Messages status code 200

      3. Confirm response has conversationId
    """

    # Trigger Send SMS
    messagingendpoint1 = MessagingEndpoint()
    messagingendpoint1.send_sms_aws4(
        agent_id,
        messaging_backend_company_id,
        int(line_id1),
        number1,
        text,
    )

    # Check consistency of message status& conversation ID with assertion
    assert messagingendpoint1.message_status == 200
    assert messagingendpoint1.conversationId == "{0}|{1}|{2}".format(
        number1, line_id1, messaging_backend_company_id
    )

    # Login with Agent A and Agent B also set them available
    phone1, phone2 = bulk_creation_available_agent

    # catch cognito token for Agent A and Agent B for using in GraphQL queries
    messaging1 = Messaging(phone1.auth_token)
    messaging2 = Messaging(phone2.auth_token)

    log.info("-----SENDER-----")
    # Retrieve SMS with Conversation ID, expected status and timeout time for being sure that SMS is delivered
    messaging1.getConversationMessagingEndpoint(
        messagingendpoint1.conversationId, "delivered", 60
    )

    # Check consistency of message status with assertion
    assert messaging1.message_status == "delivered"

    # Retrieve SMS history with querysize for comparing if latest message id is updated with the last sms
    messaging1.getListConversationsQuery(2, messaging1.retrieve_message_id, 60)

    # Check consistency of message id with assertion
    assert messaging1.retrieve_message_id == messaging1.history_id

    log.info("-----RECEIVER-----")
    # Create conversation ID for for Agent B for querying messag ID(Need to create it because it is different conversation ID for receiver than Sender)
    messaging2.conversation_id = "{0}|{1}|{2}".format(
        number2, line_id2, messaging_backend_company_id
    )

    # Retrieve SMS with Conversation ID, expected status and timeout time for being sure that SMS is received
    messaging2.getConversationMessagingEndpoint(
        messaging2.conversation_id, "received", 60
    )

    # Check consistency of message status with assertion
    assert messaging2.message_status == "received"

    # Retrieve SMS history with querysize for comparing if latest message id is updated with the last sms
    messaging2.getListConversationsQuery(2, messaging2.retrieve_message_id, 60)

    # Check consistency of message id with assertion
    assert messaging2.retrieve_message_id == messaging2.history_id

    log.info("-----BEFORE ACK-----")
    messaging2.listUnReadConversations(1, messaging2.retrieve_message_id, 60)
    # Check Number of unread Conversation and Messages before Acknowledgement
    assert messaging2.unread_message_direction_before == "received"
    unread_conversation_num = messaging2.unread_conversation_num_before
    # Acknowledge Read message with latest Message ID
    messagingendpoint1.ack_sms_aws4(
        agent_id2,
        messaging_backend_company_id,
        int(line_id2),
        number2,
        messaging2.retrieve_message_id,
    )

    log.info("-----AFTER ACK-----")
    # Check Number of unread Conversation and Messages after Acknowledgement
    messaging2.listReadConversations(1, messaging2.retrieve_message_id, 0, 60)
    # Check consistency of unread Conversation and Message numbers
    assert messaging2.unread_conversation_num == unread_conversation_num - 1
    assert messaging2.unread_message_num == 0


@pytest.mark.flaky(reruns=2, reruns_delay=2)
@pytest.mark.parametrize(
    "agent_id,agent_id2,line_id1,number1,line_id2,number2",
    (
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_backend_agent_id2"),
            lazy_fixture("messaging_us_line_id"),
            lazy_fixture("messaging_external_us_number1"),
            lazy_fixture("messaging_us_line_id1"),
            lazy_fixture("messaging_external_us_number"),
            marks=[
                pytest.mark.testrail(ids=("C10817671",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_backend_agent_id2"),
            lazy_fixture("messaging_gb_line_id"),
            lazy_fixture("messaging_gb_number1"),
            lazy_fixture("messaging_gb_line_id1"),
            lazy_fixture("messaging_gb_number"),
            marks=[
                pytest.mark.testrail(ids=("C10953086",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_backend_agent_id2"),
            lazy_fixture("messaging_canadian_line_id"),
            lazy_fixture("messaging_canadian_number1"),
            lazy_fixture("messaging_canadian_line_id1"),
            lazy_fixture("messaging_canadian_number"),
            marks=[
                pytest.mark.testrail(ids=("C11033866",)),
                pytest.mark.sanity_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_backend_agent_id2"),
            lazy_fixture("messaging_australian_line_id"),
            lazy_fixture("messaging_australian_number1"),
            lazy_fixture("messaging_australian_line_id1"),
            lazy_fixture("messaging_australian_number"),
            marks=[
                pytest.mark.testrail(ids=("C11033867",)),
                pytest.mark.sanity_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_backend_agent_id2"),
            lazy_fixture("messaging_french_line_id"),
            lazy_fixture("messaging_french_number1"),
            lazy_fixture("messaging_french_line_id1"),
            lazy_fixture("messaging_french_number"),
            marks=[
                pytest.mark.testrail(ids=("C11033868",)),
                pytest.mark.sanity_test_messaging,
            ],
        ),
    ),
    ids=[
        "sms-backend-1600-chars-US",
        "sms-backend-1600-chars-GB",
        "sms-backend-1600-chars-CA",
        "sms-backend-1600-chars-AU",
        "sms-backend-1600-chars-FR",
    ],
)
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["messagingtest1@aircall.io", "messagingtest2@aircall.io"]],
    indirect=True,
)
def test_sms_backend_1600_chars(
    request,
    agent_id,
    agent_id2,
    line_id1,
    number1,
    line_id2,
    number2,
    bulk_creation_available_agent,
    messaging_backend_company_id,
    create_huge_string,
):
    """
    * Scenario
      1. Send SMS v1/messages/send with max char
      2. Check status code
      3. Check response
      4. Retrieve SMS status with ConversationID
      5. Compare message_id send and receive
      6. Logout agent A

    * Acceptance criteria
      1. Send SMS successfully

      2. Messages status code 200

      3. Confirm response has conversationId
    """

    # Trigger Send SMS
    messagingendpoint1 = MessagingEndpoint()
    messagingendpoint1.send_sms_aws4(
        agent_id,
        messaging_backend_company_id,
        int(line_id1),
        number1,
        create_huge_string(1600),
    )

    # Check consistency of message status with assertion
    assert messagingendpoint1.message_status == 200
    assert messagingendpoint1.conversationId == "{0}|{1}|{2}".format(
        number1, line_id1, messaging_backend_company_id
    )

    # Login with Agent A and set it available
    phone1, phone2 = bulk_creation_available_agent

    # catch cognito token for Agent A
    messaging1 = Messaging(phone1.auth_token)
    messaging2 = Messaging(phone2.auth_token)

    log.info("-----SENDER-----")
    # Retrieve SMS with Conversation ID, expected status and timeout time
    messaging1.getConversationMessagingEndpoint(
        messagingendpoint1.conversationId, "delivered", 60
    )

    # Check consistency of message status with assertion
    assert messaging1.message_status == "delivered"

    # Retrieve SMS history with querysize
    messaging1.getListConversationsQuery(2, messaging1.retrieve_message_id, 60)

    # Check consistency of message id with assertion
    assert messaging1.retrieve_message_id == messaging1.history_id

    log.info("-----RECEIVER-----")
    messaging2.conversation_id = "{0}|{1}|{2}".format(
        number2, line_id2, messaging_backend_company_id
    )

    messaging2.getConversationMessagingEndpoint(
        messaging2.conversation_id, "received", 60
    )

    # Check consistency of message status with assertion
    assert messaging2.message_status == "received"

    # Retrieve SMS history with querysize
    messaging2.getListConversationsQuery(2, messaging2.retrieve_message_id, 60)

    # Check consistency of message id with assertion
    assert messaging2.retrieve_message_id == messaging2.history_id

    log.info("-----BEFORE ACK-----")
    messaging2.listUnReadConversations(1, messaging2.retrieve_message_id, 60)
    # Check Number of unread Conversation and Messages before Acknowledgement
    assert messaging2.unread_message_direction_before == "received"
    unread_conversation_num = messaging2.unread_conversation_num_before
    # Acknowledge Read message with latest Message ID
    messagingendpoint1.ack_sms_aws4(
        agent_id2,
        messaging_backend_company_id,
        int(line_id2),
        number2,
        messaging2.retrieve_message_id,
    )

    log.info("-----AFTER ACK-----")
    # Check Number of unread Conversation and Messages after Acknowledgement
    messaging2.listReadConversations(1, messaging2.retrieve_message_id, 0, 60)
    # Check consistency of unread Conversation and Message numbers
    assert messaging2.unread_conversation_num == unread_conversation_num - 1
    assert messaging2.unread_message_num == 0


@pytest.mark.flaky(reruns=2, reruns_delay=2)
@pytest.mark.parametrize(
    "agent_id,agent_id2,line_id1,number1,line_id2,number2",
    (
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_backend_agent_id2"),
            lazy_fixture("messaging_us_line_id"),
            lazy_fixture("messaging_external_us_number1"),
            lazy_fixture("messaging_us_line_id1"),
            lazy_fixture("messaging_external_us_number"),
            marks=[
                pytest.mark.testrail(ids=("C11051668",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_backend_agent_id2"),
            lazy_fixture("messaging_gb_line_id"),
            lazy_fixture("messaging_gb_number1"),
            lazy_fixture("messaging_gb_line_id1"),
            lazy_fixture("messaging_gb_number"),
            marks=[
                pytest.mark.testrail(ids=("C11051669",)),
                pytest.mark.smoke_test_messaging,
            ],
        ),
    ),
    ids=[
        "sms-backend-smiley-chars-US",
        "sms-backend-smiley-chars-GB",
    ],
)
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["messagingtest1@aircall.io", "messagingtest2@aircall.io"]],
    indirect=True,
)
def test_sms_backend_emojis(
    request,
    agent_id,
    agent_id2,
    line_id1,
    number1,
    line_id2,
    number2,
    bulk_creation_available_agent,
    messaging_backend_company_id,
    create_emoji,
):
    """
    * Scenario
      1. Send SMS v1/messages/send with smileys
      2. Check status code
      3. Check response
      4. Retrieve SMS status with conversationID
      5. Compare message_id send and receive
      6. Logout agent A

    * Acceptance criteria
      1. Send SMS successfully

      2. Messages status code 200

      3. Confirm response has conversationID
    """

    # Trigger Send SMS
    messagingendpoint1 = MessagingEndpoint()
    messagingendpoint1.send_sms_aws4(
        agent_id,
        messaging_backend_company_id,
        int(line_id1),
        number1,
        create_emoji(":smile::thumbsup::rocket:"),
    )

    # Check consistency of message status with assertion
    assert messagingendpoint1.message_status == 200
    assert messagingendpoint1.conversationId == "{0}|{1}|{2}".format(
        number1, line_id1, messaging_backend_company_id
    )

    # Login with Agent A and set it available
    phone1, phone2 = bulk_creation_available_agent

    # catch cognito token for Agent A
    messaging1 = Messaging(phone1.auth_token)
    messaging2 = Messaging(phone2.auth_token)

    log.info("-----SENDER-----")
    # Retrieve SMS with Conversation ID, expected status and timeout time
    messaging1.getConversationMessagingEndpoint(
        messagingendpoint1.conversationId, "delivered", 60
    )

    # Check consistency of message status with assertion
    assert messaging1.message_status == "delivered"

    # Retrieve SMS history with querysize
    messaging1.getListConversationsQuery(2, messaging1.retrieve_message_id, 60)

    # Check consistency of message id with assertion
    assert messaging1.retrieve_message_id == messaging1.history_id

    log.info("-----RECEIVER-----")
    messaging2.conversation_id = "{0}|{1}|{2}".format(
        number2, line_id2, messaging_backend_company_id
    )

    messaging2.getConversationMessagingEndpoint(
        messaging2.conversation_id, "received", 60
    )

    # Check consistency of message status with assertion
    assert messaging2.message_status == "received"

    # Retrieve SMS history with querysize
    messaging2.getListConversationsQuery(2, messaging2.retrieve_message_id, 60)

    # Check consistency of message id with assertion
    assert messaging2.retrieve_message_id == messaging2.history_id

    log.info("-----BEFORE ACK-----")
    messaging2.listUnReadConversations(1, messaging2.retrieve_message_id, 60)
    # Check Number of unread Conversation and Messages before Acknowledgement
    assert messaging2.unread_message_direction_before == "received"
    unread_conversation_num = messaging2.unread_conversation_num_before
    # Acknowledge Read message with latest Message ID
    messagingendpoint1.ack_sms_aws4(
        agent_id2,
        messaging_backend_company_id,
        int(line_id2),
        number2,
        messaging2.retrieve_message_id,
    )

    log.info("-----AFTER ACK-----")
    # Check Number of unread Conversation and Messages after Acknowledgement
    messaging2.listReadConversations(1, messaging2.retrieve_message_id, 0, 60)
    # Check consistency of unread Conversation and Message numbers
    assert messaging2.unread_conversation_num == unread_conversation_num - 1
    assert messaging2.unread_message_num == 0


@pytest.mark.parametrize(
    "agent_id,line_id,number1,text,error_msg,status_code",
    (
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_canadian_line_id"),
            lazy_fixture("messaging_external_us_number"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("between_internationl_messages_error_msg"),
            pytest.param(400),
            marks=[
                pytest.mark.testrail(ids=("C11033870",)),
                pytest.mark.sanity_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_backend_agent_id"),
            lazy_fixture("messaging_us_line_id"),
            lazy_fixture("messaging_canadian_number"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("between_internationl_messages_error_msg"),
            pytest.param(400),
            marks=[
                pytest.mark.testrail(ids=("C11033869",)),
                pytest.mark.sanity_test_messaging,
            ],
        ),
    ),
    ids=[
        "sms-backend-CA-to-US",
        "sms-backend-US-to-CA",
    ],
)
def test_sms_backend_between_us_ca(
    request,
    agent_id,
    line_id,
    number1,
    text,
    error_msg,
    status_code,
    messaging_backend_company_id,
):
    """
    * Scenario
      1. Send SMS v1/messages/send with empty body
      2. Check status code
      3. Check response

    * Acceptance criteria
      1. Send SMS fails

      2. Messages status code 400

      3. Confirm error message
    """

    # Trigger Send SMS
    messagingendpoint1 = MessagingEndpoint()
    messagingendpoint1.send_sms_aws4(
        agent_id,
        messaging_backend_company_id,
        int(line_id),
        number1,
        text,
    )

    # Check consistency of message status with assertion
    assert messagingendpoint1.message_status == status_code.values[0]
    assert messagingendpoint1.jsonresp == error_msg


@pytest.mark.parametrize(
    "to_number,from_number,sms_body,nummedia0,error_msg,status_code",
    (
        pytest.param(
            lazy_fixture("messaging_external_us_number"),
            lazy_fixture("messaging_external_us_number1"),
            lazy_fixture("empty_text"),
            lazy_fixture("twilio_nummedia0"),
            lazy_fixture("receive_empty_message_error_msg"),
            400,
            marks=[
                pytest.mark.testrail(ids=("C11051490",)),
                pytest.mark.sanity_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("empty_ext_num"),
            lazy_fixture("messaging_external_us_number1"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("twilio_nummedia0"),
            lazy_fixture("receive_empty_ToNum_error_msg"),
            400,
            marks=[
                pytest.mark.testrail(ids=("C11051491",)),
                pytest.mark.sanity_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_external_us_number"),
            lazy_fixture("empty_ext_num"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("twilio_nummedia0"),
            lazy_fixture("receive_empty_FromNum_error_msg"),
            400,
            marks=[
                pytest.mark.testrail(ids=("C11051492",)),
                pytest.mark.sanity_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("twilio_wrong_from_number"),
            lazy_fixture("messaging_external_us_number1"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("twilio_nummedia0"),
            lazy_fixture("receive_invalid_ToNum_error_msg"),
            400,
            marks=[
                pytest.mark.testrail(ids=("C11051493",)),
                pytest.mark.sanity_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_external_us_number"),
            lazy_fixture("twilio_wrong_to_number"),
            lazy_fixture("messsaging_text"),
            lazy_fixture("twilio_nummedia0"),
            lazy_fixture("receive_invalid_FromNum_error_msg"),
            400,
            marks=[
                pytest.mark.testrail(ids=("C11051494",)),
                pytest.mark.sanity_test_messaging,
            ],
        ),
    ),
    ids=[
        "receive-sms-empty-text",
        "receive-sms-with-empty-from-number",
        "receive-sms-with-empty-to-number",
        "receive-sms-with-wrong-from-number",
        "receive-sms-with-wrong-to-number",
    ],
)
def test_receive_sms_errors(
    request,
    to_number,
    from_number,
    sms_body,
    nummedia0,
    error_msg,
    status_code,
    create_random_messagesid,
):
    """
    * Scenario
      1. Send SMS v1/messages/send with errors
      2. Check status code
      3. Check response

    * Acceptance criteria
      1. Send SMS fails

      2. Messages status code 400/403/404

      3. Confirm error message
    """

    # Assign MEssage id for using it in /v1/messages/{messageId}/status/twilio (TO-DO)
    receieve_message_sid = create_random_messagesid(32)
    # Trigger Send SMS
    MessagingReceive1 = MessagingReceive(
        to_number,
        from_number,
        sms_body,
        receieve_message_sid,
        nummedia0,
    )

    # Check consistency of message status&error message with assertion
    assert MessagingReceive1.statuscode == status_code
    assert MessagingReceive1.jsonresp == error_msg


@pytest.mark.parametrize(
    "to_number,from_number",
    (
        pytest.param(
            lazy_fixture("twilio_to_number"),
            lazy_fixture("twilio_from_number"),
            marks=[
                pytest.mark.testrail(ids=("C11033871",)),
                pytest.mark.sanity_test_messaging,
            ],
        ),
        pytest.param(
            lazy_fixture("messaging_gb_number"),
            lazy_fixture("messaging_gb_number1"),
            marks=[
                pytest.mark.testrail(ids=("C11033872",)),
                pytest.mark.sanity_test_messaging,
            ],
        ),
    ),
    ids=["receive-sms-Twilio-US-to-US", "receive-sms-Twilio-GB-to-GB"],
)
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [["messagingtest1@aircall.io"]],
    indirect=True,
)
def test_receive_sms(
    request,
    to_number,
    from_number,
    bulk_creation_available_agent,
    twilio_receive_sms_body,
    create_random_messagesid,
    twilio_nummedia0,
    messaging_us_line_id,
    twilio_from_number_without_plus,
):
    """
    * Scenario
      1. Receive SMS /v1/messages/receive/twilio
      2. Check response

    * Acceptance criteria
      1. Receive SMS Success

      2. Messages status code 200
    """

    # Assign MEssage id for using it in /v1/messages/{messageId}/status/twilio (TO-DO)
    receieve_message_sid = create_random_messagesid(32)
    # Trigger Receive SMS
    MessagingReceive1 = MessagingReceive(
        to_number,
        from_number,
        twilio_receive_sms_body,
        receieve_message_sid,
        twilio_nummedia0,
    )

    # Check consistency of message status with assertion
    assert MessagingReceive1.statuscode == 200

    # Login with Agent A and set it available
    (phone1,) = bulk_creation_available_agent

    # catch cognito token for Agent A
    messaging1 = Messaging(phone1.auth_token)

    messaging1.getMessageId(messaging_us_line_id, twilio_from_number_without_plus)
    # Because of Uodate status currently working only for Send/Outbound messages we can not update status
    MessagingUpdateStatus1 = MessagingUpdateStatus(
        to_number, from_number, "pending", receieve_message_sid, receieve_message_sid
    )

    # Because of sendind SID not UUID request will fail
    assert MessagingUpdateStatus1.resp == 400
