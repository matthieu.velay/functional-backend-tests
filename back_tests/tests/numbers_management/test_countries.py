import logging
import pytest

from back_tests.framework.numbers_management.countries import countries_client

log = logging.getLogger("TEST-COUNTRIES")


@pytest.mark.parametrize(
    "license, expected_response_code",
    [("to_do", 200), ("in_progress", 200), ("no", 200), ("licensed", 200)],
)
@pytest.mark.parametrize("licenseObtainedDate", [True, False])
def test_countries_license_update(
    license,
    expected_response_code,
    licenseObtainedDate,
    date_time_iso_format,
    turkey_country_code_uuid,
):
    """
    scenario: 1
    GIVEN I’m a Numbers Management admin
    WHEN I update the license status of a country
    THEN I expect the license status has been updated

    --------------------------------------------------

    scenario: 2
    GIVEN I’m a Numbers Management admin
    WHEN I update the license status of a country to “Licensed”
    THEN I need to specify the “Licensed date” IF NOT an error should be raised
    """
    payload = {"license": license}
    if licenseObtainedDate:
        payload["licenseObtainedDate"] = date_time_iso_format

    response = countries_client.countries_license_update(
        turkey_country_code_uuid, payload
    )
    response_json = response.json()

    if license == "licensed" and licenseObtainedDate is False:
        # Scenario 2
        assert response.status_code == 400
        assert (
            response_json["message"] == "licenseObtainedDate should exist when licensed"
        )
    elif license == "licensed" and licenseObtainedDate is True:
        # Scenario 2
        assert response.status_code == expected_response_code
        assert response_json["license"] == license
        # assert response_json["licenseObtainedDate"] ==  check with dev' iso time
    else:
        # Scenario 1
        assert response.status_code == expected_response_code
        assert response_json["licenseObtainedDate"] is None

    log.info("Time response of endpoint : {}".format(response.elapsed.total_seconds()))


# TODO: refacto like error case carriers
@pytest.mark.parametrize(
    "payload, expected_response_code",
    [
        ({"license": "WRONG_PAYLOAD"}, 400),
        ({}, 400),
        ({"license": "WRONG_PAYLOAD", "licenseObtainedDate": "WRONG_PAYLOAD"}, 400),
        ({"licenseObtainedDate": "WRONG_PAYLOAD"}, 400),
        ({"license": "to_do", "licenseObtainedDate": "WRONG_PAYLOAD"}, 400),
        (
            {
                "license": "licensed",
                "licenseObtainedDate": "2021-04-01 11:07:36.022141",
            },
            400,
        ),
    ],
)
def test_countries_license_update_corner_case(
    payload, expected_response_code, turkey_country_code_uuid
):
    """
    GIVEN I’m a Numbers Management admin
    WHEN I update the license status with a wrong payload
    THEN I expect raise an error
    """
    response = countries_client.countries_license_update(
        turkey_country_code_uuid, payload
    )
    response_json = response.json()
    assert response.status_code == expected_response_code
    assert response_json["message"] == "Request failed validation"
    log.info("Time response of endpoint : {}".format(response.elapsed.total_seconds()))


def test_countries_license_update_licensed_back_to_do(
    turkey_country_code_uuid, date_time_iso_format
):
    """
    GIVEN I’m a Numbers Management admin
    WHEN I change the license status of a country from “Licensed” to another status
    THEN the “Licensed date” has to be removed
    """
    payload_licensed = {
        "license": "licensed",
        "licenseObtainedDate": date_time_iso_format,
    }
    response_licensed = countries_client.countries_license_update(
        turkey_country_code_uuid, payload_licensed
    )
    response_licensed_json = response_licensed.json()
    assert response_licensed.status_code == 200
    assert response_licensed_json["license"] == "licensed"
    # assert response_json["licenseObtainedDate"] ==  check with dev' iso time

    payload_to_do = {"license": "to_do"}

    response_to_do = countries_client.countries_license_update(
        turkey_country_code_uuid, payload_to_do
    )
    response_to_do_json = response_to_do.json()
    assert response_to_do.status_code == 200
    assert response_to_do_json["license"] == "to_do"
    assert response_to_do_json["licenseObtainedDate"] is None

    # assert response_json["licenseObtainedDate"] ==  check with dev' iso time


# TODO: NOT FINISHED I WAIT CREATE COUNTRY ENDPOINT
def test_get_all_information_related_to_country(turkey_country_code_uuid):
    response = countries_client.countries_get_by_id(turkey_country_code_uuid)
    print(response.json())
    print(response.status_code)
