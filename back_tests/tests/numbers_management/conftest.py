import pytest
import os


ENV = os.getenv("ENV")
if ENV == "staging":
    os.environ["DOMAIN_NAME"] = "numbers.aircall-staging.com"
    os.environ[
        "CLUSTER_ARN"
    ] = "arn:aws:rds:us-west-2:886862221201:cluster:staging-numbers-management-auroracluster-1cw8x3p07iqkv"
    os.environ["SECRET_NAME"] = "/staging/numbers-management/aurora"
else:
    os.environ["DOMAIN_NAME"] = "numbers.aircall-sandbox.com"
    os.environ[
        "CLUSTER_ARN"
    ] = "arn:aws:rds:us-west-2:825322956196:cluster:alpha-numbers-management-auroracluster-1v7p1enn043h9"
    os.environ["SECRET_NAME"] = "/alpha/numbers-management/aurora"


pytest_plugins = [
    "back_tests.fixtures.numbers_management.fixtures_factory",
    "back_tests.fixtures.numbers_management.fixtures_carriers_payload",
    "back_tests.fixtures.numbers_management.fixtures_numbers_management_helpers",
    "back_tests.fixtures.numbers_management.fixtures_countries_payload",
    "back_tests.fixtures.numbers_management.fixtures_area_code_and_countries",
]
