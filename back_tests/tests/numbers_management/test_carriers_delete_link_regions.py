import logging
import pytest

from back_tests.framework.numbers_management.carriers import carriers_client

log = logging.getLogger("TEST-CARRIERS")

# TODO: since it's one lambda per endpoint,
#  I have to redo the error management tests for each. I will think about making a factory test error handling.
@pytest.mark.parametrize(
    "case, expected_response_code",
    [
        ("WRONG_CARRIER_UUID", 404),
        ("NO_REGION_LIST", 400),
        ("WRONG_REGION_LIST", 200),
        ("REGION_ALREADY_DELETE", 200),
        ("WRONG_REGION_UUID", 200),
    ],
)
def test_delete_regions_link_corner_cases(
    case,
    expected_response_code,
    carrier_link_area_code,
    generate_uuid,
    fra_area_code_uuid,
    list_fr_us_area_code_uuid,
):
    """
    GIVEN a Numbers Management admin
    WHEN <case_mentioned>
    THEN an error should be raised
    """

    if case == "WRONG_CARRIER_UUID":
        carrier_link_area_code = generate_uuid
        payload = {"regionList": [fra_area_code_uuid]}

    elif case == "REGION_ALREADY_DELETE":
        payload = {"regionList": [fra_area_code_uuid]}
        _ = carriers_client.carriers_remove_regions_params(
            carrier_link_area_code, payload
        )

    elif case == "WRONG_REGION_UUID":
        payload = {"regionList": [generate_uuid]}

    elif case == "NO_REGION_LIST":
        payload = {"regionList": []}

    elif case == "WRONG_REGION_LIST":
        payload = {"regionList": list_fr_us_area_code_uuid + [generate_uuid]}

    response = carriers_client.carriers_remove_regions_params(
        carrier_link_area_code, payload
    )
    assert response.status_code == expected_response_code

    if case == "REGION_ALREADY_DELETE" or case == "WRONG_REGION_UUID":
        assert response.json()["resultCount"] == 0
    elif case == "WRONG_REGION_LIST":
        assert response.json()["resultCount"] == len(list_fr_us_area_code_uuid)


def test_remove_link_between_regions_and_carriers(
    carrier_link_area_code, fra_area_code_uuid
):
    """
    GIVEN a Numbers Management admin
    WHEN I remove a area code link to a carrier
    THEN I expected that area code is no longer linked to the carrier
    """
    payload = {"regionList": [fra_area_code_uuid]}

    response = carriers_client.carriers_remove_regions_params(
        carrier_link_area_code, payload
    )

    assert response.status_code == 200

    db_check = carriers_client.carriers_get_link_area_code(
        carrier_link_area_code, fra_area_code_uuid
    )

    assert len(db_check) == 0


def test_bulk_remove_link_between_regions_and_carriers(
    carrier_link_area_code, list_fr_us_area_code_uuid
):
    """
    GIVEN I’m a Numbers Management admin
    WHEN I can choose to remove a list area code link to a carrier
    THEN I expected that all area code inside the list is no longer linked to the carrier
    """

    payload = {"regionList": list_fr_us_area_code_uuid}

    response = carriers_client.carriers_remove_regions_params(
        carrier_link_area_code, payload
    )
    assert response.status_code == 200

    # EACH ELEMENT INSIDE THE LIST HAS BEEN REMOVE
    for area_code_uuid in list_fr_us_area_code_uuid:

        db_check = carriers_client.carriers_get_link_area_code(
            carrier_link_area_code, area_code_uuid
        )

        assert len(db_check) == 0
