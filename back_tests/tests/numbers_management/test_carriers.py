import logging
import pytest
from pytest_lazyfixture import lazy_fixture


from back_tests.framework.numbers_management.carriers import carriers_client

log = logging.getLogger("TEST-CARRIERS")


@pytest.mark.parametrize(
    "payload, expected_response_code",
    [
        (lazy_fixture("create_carriers_payload_basics"), 201),
        (lazy_fixture("create_carriers_payload_without_name"), 400),
        (lazy_fixture("create_carriers_payload_empty_payload"), 400),
    ],
)
def test_create_carriers_name_input(payload, expected_response_code):
    """
    GIVEN I’m a Numbers Management admin
    WHEN I add a new Carrier <name of the test>
    THEN I expected the carriers is created with related information

    -----------------------------------------------------------------

    GIVEN I’m a Numbers Management admin
    WHEN I add a new Carrier <name of fixture and expected result>
    THEN if some mandatory fields are missing an error should be raised
    """

    response = carriers_client.carriers_create(payload)
    assert response.status_code == expected_response_code

    if "name" in payload:
        db_check = carriers_client.carriers_get_by_name(payload["name"])
        assert db_check

    if expected_response_code == 201:

        response_json = response.json()
        assert response_json["name"] == payload["name"]

    log.info("response time of endpoint : {}".format(response.elapsed.total_seconds()))


@pytest.mark.parametrize(
    "payload, expected_response_code",
    [
        (lazy_fixture("create_carriers_with_no_numbers_offerings"), 201),
        (
            lazy_fixture("create_carriers_with_partner_and_company_numbers_offerings"),
            201,
        ),
        (lazy_fixture("create_carriers_payload_without_numbers_offering"), 400),
        (lazy_fixture("create_carriers_with_wrong_numbers_offerings"), 400),
    ],
)
def test_create_carriers_numbers_offerings_input(payload, expected_response_code):
    """
    GIVEN I’m a Numbers Management admin
    WHEN I add a new Carrier <name of the test>
    THEN I expected the carriers is created with related information

    -----------------------------------------------------------------

    GIVEN I’m a Numbers Management admin
    WHEN I add a new Carrier <name of fixture and expected result>
    THEN if some mandatory fields are missing an error should be raised
    """

    response = carriers_client.carriers_create(payload)
    assert response.status_code == expected_response_code
    db_check = carriers_client.carriers_get_by_name(payload["name"])

    if expected_response_code == 201:

        response_json = response.json()
        assert response_json["name"] == payload["name"]
        assert db_check
        # TODO: list enum

    else:
        assert not db_check

    log.info("response time of endpoint : {}".format(response.elapsed.total_seconds()))


@pytest.mark.parametrize(
    "payload, expected_response_code",
    [
        (lazy_fixture("create_carriers_payload_with_api_offering"), 201),
        (lazy_fixture("create_carriers_payload_with_several_api_offering"), 201),
        (lazy_fixture("create_carriers_payload_with_wrong_api_offering"), 400),
    ],
)
def test_create_carriers_api_offering_input(payload, expected_response_code):
    """
    GIVEN I’m a Numbers Management admin
    WHEN I add a new Carrier <name of the test>
    THEN I expected the carriers is created with related information

    -----------------------------------------------------------------

    GIVEN I’m a Numbers Management admin
    WHEN I add a new Carrier <name of fixture and expected result>
    THEN if some mandatory fields are missing an error should be raised
    """

    response = carriers_client.carriers_create(payload)
    assert response.status_code == expected_response_code

    db_check = carriers_client.carriers_get_by_name(payload["name"])

    if expected_response_code == 201:

        response_json = response.json()
        assert response_json["name"] == payload["name"]
        assert db_check
        # TODO: list enum
        assert response_json["apiOffers"][0]["offer"] == payload["apiOffers"][0]

    else:
        assert not db_check

    log.info("response time of endpoint : {}".format(response.elapsed.total_seconds()))


@pytest.mark.parametrize(
    "payload, expected_response_code",
    [
        (lazy_fixture("create_carriers_payload_with_contacts"), 201),
        (lazy_fixture("create_carriers_payload_minimal_requirement_contacts"), 201),
        (lazy_fixture("create_carriers_payload_wrong_contacts"), 400),
    ],
)
def test_create_carriers_contacts_input(payload, expected_response_code):
    """
    GIVEN I’m a Numbers Management admin
    WHEN I add a new Carrier <name of the test>
    THEN I expected the carriers is created with related information

    -----------------------------------------------------------------

    GIVEN I’m a Numbers Management admin
    WHEN I add a new Carrier <name of fixture and expected result>
    THEN if some mandatory fields are missing an error should be raised
    """

    response = carriers_client.carriers_create(payload)
    assert response.status_code == expected_response_code

    db_check = carriers_client.carriers_get_by_name(payload["name"])

    if expected_response_code == 201:

        response_json = response.json()
        assert response_json["name"] == payload["name"]
        for i in range(len(response_json["contacts"])):
            assert (
                response_json["contacts"][i]["name"] == payload["contacts"][i]["name"]
            )
            assert (
                response_json["contacts"][i]["email"] == payload["contacts"][i]["email"]
            )
        assert db_check

    else:
        assert not db_check

    log.info("response time of endpoint : {}".format(response.elapsed.total_seconds()))


@pytest.mark.parametrize(
    "payload, expected_response_code",
    [
        (lazy_fixture("create_carriers_payload_with_trunks"), 201),
        (lazy_fixture("create_carriers_payload_with_wrong_trunks"), 400),
        (lazy_fixture("create_carriers_payload_with_too_long_integer_trunks"), 400),
    ],
)
def test_create_carriers_trunks_input(payload, expected_response_code):
    """
    GIVEN I’m a Numbers Management admin
    WHEN I add a new Carrier <name of the test>
    THEN I expected the carriers is created with related information

    -----------------------------------------------------------------

    GIVEN I’m a Numbers Management admin
    WHEN I add a new Carrier <name of fixture and expected result>
    THEN if some mandatory fields are missing an error should be raised
    """
    response = carriers_client.carriers_create(payload)
    assert response.status_code == expected_response_code
    db_check = carriers_client.carriers_get_by_name(payload["name"])

    if expected_response_code == 201:

        response_json = response.json()
        assert response_json["name"] == payload["name"]

    log.info("response time of endpoint : {}".format(response.elapsed.total_seconds()))


# TODO:  i can't check on db !
#  Difficulties ---> requests DB
#  should be done on next PR
@pytest.mark.parametrize(
    "payload, expected_response_code",
    [
        (lazy_fixture("create_carriers_payload_without_name"), 400),
        (lazy_fixture("create_carriers_payload_empty_payload"), 400),
        ("WRONG_CARRIER_UUID", 400),
    ],
    ids=[
        "CARRIERS_WITHOUT_NAME",
        "EMPTY_PAYLOAD",
        "WRONG_CARRIER_UUID",
    ],
)
def test_updated_carriers_corner_case(
    payload, expected_response_code, factory_create_carriers, generate_uuid
):
    """
    GIVEN a Numbers Management admin
    WHEN <case_mentioned>
    THEN an error should be raised
    """

    carrier_uuid = factory_create_carriers["carrierUUID"]

    # WRONG UUID
    if payload == "WRONG_CARRIER_UUID":
        carrier_uuid = generate_uuid
        response = carriers_client.carriers_update(carrier_uuid, payload)

    # EMPTY PAYLOAD SHOULD BE CHECK CARRIERS IS NOT EMPTY
    elif payload == {}:
        response = carriers_client.carriers_update(carrier_uuid, payload)

    elif "name" not in payload:
        response = carriers_client.carriers_update(carrier_uuid, payload)

    assert response.status_code == expected_response_code


# TODO: GET NOT AVAILABLE ---> requests DB for check updated
@pytest.mark.parametrize(
    "case, payload, expected_response_code",
    [
        (
            "NUMBER_OFFERING_RESET",
            lazy_fixture("create_carriers_with_partner_and_company_numbers_offerings"),
            200,
        ),
        (
            "API_OFFERING_REMOVE_OPTION",
            lazy_fixture("create_carriers_payload_with_several_api_offering"),
            200,
        ),
        (
            "API_OFFERING_ADDING_OPTION",
            lazy_fixture("create_carriers_payload_with_api_offering"),
            200,
        ),
        (
            "CONTACTS_UPDATE_INFORMATION",
            lazy_fixture("create_carriers_payload_with_contacts"),
            200,
        ),
    ],
    ids=[
        "NUMBER_OFFERING_RESET",
        "API_OFFERING_REMOVE_OPTION",
        "API_OFFERING_ADDING_OPTION",
        "CONTACTS_UPDATE_INFORMATION",
    ],
)
# TODO: ADDING CONTACTS TEST
def test_update_carriers_nominal_case(case, payload, expected_response_code):
    """
    GIVEN a Numbers Management admin
    WHEN I edit an existing Carrier
    THEN I expected the carrier has been updated
    """
    factory_create_carriers_response = carriers_client.carriers_create(payload)
    factory_create_carriers_response = factory_create_carriers_response.json()
    carrier_uuid = factory_create_carriers_response["carrierUUID"]

    if case == "NUMBER_OFFERING_RESET":
        payload["numbersOfferings"] = []
        response = carriers_client.carriers_update(carrier_uuid, payload).json()
        assert response["name"] == factory_create_carriers_response["name"]
        assert response["numbersOfferings"] == payload["numbersOfferings"]
        assert (
            response["numbersOfferings"]
            != factory_create_carriers_response["numbersOfferings"]
        )

    if case == "API_OFFERING_REMOVE_OPTION" or case == "API_OFFERING_ADDING_OPTION":
        payload["apiOffers"] = ["portabilities", "emergency_service"]
        response = carriers_client.carriers_update(carrier_uuid, payload).json()
        assert response["name"] == factory_create_carriers_response["name"]
        assert len(response["apiOffers"]) == len(payload["apiOffers"])
        for index in range(len(response["apiOffers"])):
            assert response["apiOffers"][index]["offer"] in payload["apiOffers"]


# TODO:  i can't check on db !
#  Difficulties ---> requests DB for capabilities regions update
#  should be done on next PR
#  ALSO LIST OF REGIONS WILL BE MOVE ! NEED DONE A SCRIPT TO RETRIEVE THE REGION LIST
@pytest.mark.parametrize(
    "case, expected_response_code",
    [
        ("WRONG_CAPABILITIES", 400),
        ("NO_REGION_LIST", 400),
        ("WRONG_REGION_UUID", 404),
        ("REGION_ALREADY_EXISTS", 409),
        ("NO_CAPABILITIES", 400),
        ("WRONG_CARRIER_UUID", 404),
    ],
)
def test_link_area_code_corner_cases(
    case,
    expected_response_code,
    factory_create_carriers,
    generate_uuid,
    fra_area_code_uuid,
):
    """
    GIVEN a Numbers Management admin
    WHEN <case_mentioned>
    THEN an error should be raised
    """

    carrier_uuid = factory_create_carriers["carrierUUID"]

    if case == "WRONG_CARRIER_UUID":
        carrier_uuid = generate_uuid
        payload = {
            "capabilities": ["sms", "voice"],
            "regionList": [fra_area_code_uuid],
            "supportRequired": True,
            "enabled": False,
        }
        response = carriers_client.carriers_add_regions_params(carrier_uuid, payload)
        assert response.status_code == expected_response_code

    if case == "NO_CAPABILITIES":
        payload = {
            "regionList": [fra_area_code_uuid],
            "supportRequired": True,
            "enabled": False,
        }

        response = carriers_client.carriers_add_regions_params(carrier_uuid, payload)
        assert response.status_code == expected_response_code

    if case == "REGION_ALREADY_EXISTS":
        payload = {
            "capabilities": ["sms", "voice"],
            "regionList": [fra_area_code_uuid],
            "supportRequired": True,
            "enabled": False,
        }
        _ = carriers_client.carriers_add_regions_params(carrier_uuid, payload)
        response = carriers_client.carriers_add_regions_params(carrier_uuid, payload)
        assert response.status_code == expected_response_code

    if case == "WRONG_REGION_UUID":
        payload = {
            "capabilities": ["sms", "voice"],
            "regionList": [generate_uuid],
            "supportRequired": True,
            "enabled": False,
        }
        response = carriers_client.carriers_add_regions_params(carrier_uuid, payload)
        assert response.status_code == expected_response_code

    if case == "NO_REGION_LIST":
        payload = {
            "capabilities": ["sms", "voice"],
            "regionList": [],
            "supportRequired": True,
            "enabled": False,
        }
        response = carriers_client.carriers_add_regions_params(carrier_uuid, payload)
        assert response.status_code == expected_response_code

    if case == "WRONG_CAPABILITIES":
        payload = {
            "capabilities": ["WRONG_CAPABILITES", "voice"],
            "regionList": [fra_area_code_uuid],
            "supportRequired": True,
            "enabled": False,
        }
        response = carriers_client.carriers_add_regions_params(carrier_uuid, payload)
        assert response.status_code == expected_response_code


# TODO: can't get information with endpoint check manually
@pytest.mark.parametrize("support_required", [True, False])
@pytest.mark.parametrize("enabled", [True, False])
def test_link_area_codes_enabled_and_support(
    support_required, enabled, factory_create_carriers, fra_area_code_uuid
):
    """
    GIVEN Numbers Management admin
    WHEN I can choose to put Enable has <True> or <False>
    AND I can choose to put Support required has <True> or <False>
    THEN I expected the status of the carrier has been updated
    """

    carrier_uuid = factory_create_carriers["carrierUUID"]

    payload = {
        "capabilities": ["sms", "voice"],
        "regionList": [
            fra_area_code_uuid,
        ],
        "supportRequired": support_required,
        "enabled": enabled,
    }

    response = carriers_client.carriers_add_regions_params(carrier_uuid, payload)
    assert response.status_code == 201


def test_link_area_codes_several_regions(
    factory_create_carriers, list_fr_us_area_code_uuid
):
    """
    GIVEN a country
    WHEN I want to bulk link several area code of this country
    THEN I expected all area codes link to this country
    """
    carrier_uuid = factory_create_carriers["carrierUUID"]

    payload = {
        "capabilities": ["sms", "voice"],
        "regionList": list_fr_us_area_code_uuid,
        "supportRequired": True,
        "enabled": False,
    }

    response = carriers_client.carriers_add_regions_params(carrier_uuid, payload)
    assert response.status_code == 201
    assert response.json()["resultCount"] == len(list_fr_us_area_code_uuid)


@pytest.mark.parametrize("capabilities", [["sms"], ["voice"], ["sms", "voice"]])
def test_link_area_codes_capabilities(
    capabilities, factory_create_carriers, fra_area_code_uuid
):
    """
    GIVEN I’m a Numbers Management admin
    WHEN I specify the capabilities
    THEN Voice, SMS, or both Voice and SMS can be specified
    """

    carrier_uuid = factory_create_carriers["carrierUUID"]

    payload = {
        "capabilities": capabilities,
        "regionList": [
            fra_area_code_uuid,
        ],
        "supportRequired": True,
        "enabled": True,
    }

    response = carriers_client.carriers_add_regions_params(carrier_uuid, payload)
    assert response.status_code == 201


@pytest.mark.parametrize(
    "capabilities", [["sms"], ["voice"], ["sms", "voice"]], ids=["SMS", "VOICE", "BOTH"]
)
def test_updated_link_area_codes_capabilities(
    carrier_link_area_code, fra_area_code_uuid, capabilities
):
    """
    GIVEN I’m a Numbers Management admin
    WHEN I update the capabilities of an area code offered by a carrier
    THEN I can specify Voice, SMS, or both Voice and SMS
    """

    payload = {
        "capabilities": capabilities,
        "regionList": [fra_area_code_uuid],
    }

    response = carriers_client.carriers_update_regions_params(
        carrier_link_area_code, payload
    )
    assert response.status_code == 200

    db_check = carriers_client.carriers_get_link_area_code(
        carrier_link_area_code, fra_area_code_uuid
    )

    if capabilities == ["sms"]:
        assert len(db_check) == len(capabilities)
        assert (
            db_check[0][5]["stringValue"] in capabilities
        )  # CHECK IF CAPABILITIES IS SMS

    if capabilities == ["voice"]:
        assert len(db_check) == len(capabilities)
        assert (
            db_check[0][5]["stringValue"] in capabilities
        )  # CHECK IF CAPABILITIES IS SMS

    if capabilities == ["sms", "voice"]:
        assert len(db_check) == len(capabilities)
        for element in db_check:
            assert element[5]["stringValue"] in capabilities


@pytest.mark.parametrize("enabled", [False, True])
def test_updated_link_area_codes_enable(
    carrier_link_area_code, fra_area_code_uuid, enabled
):
    """
    GIVEN I’m a Numbers Management admin
    WHEN I update the enabled information of an area code offered by a carrier
    THEN I expected the information is updated
    """

    payload = {
        "regionList": [fra_area_code_uuid],
        "enabled": enabled,
    }

    response = carriers_client.carriers_update_regions_params(
        carrier_link_area_code, payload
    )
    assert response.status_code == 200

    db_check = carriers_client.carriers_get_link_area_code(
        carrier_link_area_code, fra_area_code_uuid
    )
    for element in db_check:
        assert element[1]["booleanValue"] == enabled


@pytest.mark.parametrize("supportRequired", [False, True])
@pytest.mark.parametrize("enabled", [True, False])
def test_updated_link_area_codes_support(
    carrier_link_area_code, fra_area_code_uuid, supportRequired, enabled
):
    """
    GIVEN I’m a Numbers Management admin
    WHEN I update the supportRequired information of an area code offered by a carrier
    WITH enabled is True
    THEN I expected the supportRequired information is updated

    ----------------------------------------------------------------------------------

    GIVEN I’m a Numbers Management admin
    WHEN I update the supportRequired information of an area code offered by a carrier
    WITH enabled is False
    THEN I expected raise an error
    """

    if enabled is False:
        payload = {
            "regionList": [fra_area_code_uuid],
            "enabled": enabled,
        }
        response = carriers_client.carriers_update_regions_params(
            carrier_link_area_code, payload
        )
        assert response.status_code == 200
        db_check_to_compare_later = carriers_client.carriers_get_link_area_code(
            carrier_link_area_code, fra_area_code_uuid
        )

    payload = {
        "regionList": [fra_area_code_uuid],
        "supportRequired": supportRequired,
    }

    response = carriers_client.carriers_update_regions_params(
        carrier_link_area_code, payload
    )

    db_check = carriers_client.carriers_get_link_area_code(
        carrier_link_area_code, fra_area_code_uuid
    )
    if enabled is False:
        assert response.status_code == 409
        for element_db_check in db_check:
            for element_to_compare in db_check_to_compare_later:
                assert (
                    element_db_check[2]["booleanValue"]
                    == element_to_compare[2]["booleanValue"]
                )
    else:
        assert response.status_code == 200
        for element in db_check:
            assert element[2]["booleanValue"] == supportRequired


def test_updated_link_area_codes_bulk(
    carrier_link_area_code, list_fr_us_area_code_uuid
):
    """
    GIVEN I’m a Numbers Management admin
    WHEN I update the information of an area code offered by a carrier
    THEN I expected the information is updated
    """
    payload = {
        "capabilities": ["sms"],
        "regionList": list_fr_us_area_code_uuid,
    }

    response = carriers_client.carriers_update_regions_params(
        carrier_link_area_code, payload
    )

    assert response.status_code == 200
    for area_code_uuid in list_fr_us_area_code_uuid:
        db_check = carriers_client.carriers_get_link_area_code(
            carrier_link_area_code, area_code_uuid
        )
        assert len(db_check) == len(payload["capabilities"])
        assert db_check[0][5]["stringValue"] in payload["capabilities"]


@pytest.mark.parametrize(
    "case, expected_response_code",
    [
        ("UPDATE_NON_EXISTING_AREA_CODE", 404),
        ("NO_REGION_LIST", 400),
        ("UPDATE_NO_LINKED_AREA_CODE", 404),
        ("UPDATE_LIST_AREA_CODE_WRONG", 404),
    ],
)
def test_updated_link_area_codes_corner_case(
    case,
    expected_response_code,
    generate_uuid,
    list_fr_us_area_code_uuid,
    tur_area_code_uuid_never_link,
    carrier_link_area_code,
):
    """
    GIVEN a Numbers Management admin
    WHEN I updated updated a area code link to a carrier with the <case_mentioned>
    THEN an error should be raised
    """
    if case == "UPDATE_NON_EXISTING_AREA_CODE":
        payload = {
            "capabilities": ["sms"],
            "regionList": [generate_uuid],
        }

    if case == "NO_REGION_LIST":
        payload = {
            "capabilities": ["sms"],
            "regionList": [],
        }

    if case == "UPDATE_NO_LINKED_AREA_CODE":
        payload = {
            "capabilities": ["sms"],
            "regionList": [tur_area_code_uuid_never_link],
        }

    if case == "UPDATE_LIST_AREA_CODE_WRONG":
        payload = {
            "capabilities": ["sms"],
            "regionList": list_fr_us_area_code_uuid + [generate_uuid],
        }

    response = carriers_client.carriers_update_regions_params(
        carrier_link_area_code, payload
    )

    assert response.status_code == expected_response_code
