import logging
import pytest

from pytest_lazyfixture import lazy_fixture

from back_tests.framework.numbers_management.carriers import carriers_client
from back_tests.framework.numbers_management.countries import countries_client

log = logging.getLogger("TEST-CARRIERS")


@pytest.mark.parametrize(
    "case, expected_response_code",
    [
        ("WRONG_INBOUND_SERVICES", 400),
        ("WRONG_COUNTRY_UUID", 404),
        ("COUNTRY_ALREADY_EXISTS", 201),
        ("WRONG_CARRIER_UUID", 404),
    ],
)
def test_inbound_services_corner_cases(
    case,
    expected_response_code,
    factory_create_carriers,
    generate_uuid,
    random_country_code_uuid,
):
    """
    GIVEN a Numbers Management admin
    WHEN <case_mentioned>
    THEN an error should be raised
    """

    carrier_uuid = factory_create_carriers["carrierUUID"]
    payload = {"inboundServices": ["cnam"]}

    if case == "WRONG_CARRIER_UUID":
        carrier_uuid = generate_uuid

    elif case == "WRONG_INBOUND_SERVICES":
        payload = {"inboundServices": ["WRONG_INBOUND_SERVICE"]}

    # ACCORDING TO THE SPEC, RESTRICTION SHOULD BE HANDLE BY FRONT PART
    elif case == "COUNTRY_ALREADY_EXISTS":
        _ = carriers_client.carriers_create_inbound_services_countries(
            carrier_uuid, random_country_code_uuid, payload
        )

    elif case == "WRONG_COUNTRY_UUID":
        random_country_code_uuid = generate_uuid

    response = carriers_client.carriers_create_inbound_services_countries(
        carrier_uuid, random_country_code_uuid, payload
    )
    assert response.status_code == expected_response_code


@pytest.mark.parametrize(
    "inbound_service",
    [
        ["cnam"],
        ["directory_service"],
        ["emergency_service"],
        ["cnam", "directory_service", "emergency_service"],
        [],
    ],
)
def test_create_inbound_services(
    factory_create_carriers, random_country_code_uuid, inbound_service
):
    """
    Given a carrier and a country
    When I want to link a carrier with a country
    I need to specify one or all this following information : <case_mentioned>
    Then I expected the carrier and country are linked with a inbound service information
    """
    carrier_uuid = factory_create_carriers["carrierUUID"]
    payload = {"inboundServices": inbound_service}

    response = carriers_client.carriers_create_inbound_services_countries(
        carrier_uuid, random_country_code_uuid, payload
    )
    assert response.status_code == 201

    db_check = carriers_client.carriers_get_inbound_services(
        carrier_uuid, random_country_code_uuid
    )

    if len(inbound_service) == 1:
        assert [db_check[0][3]["stringValue"]] == inbound_service

    elif len(inbound_service) == 0:
        assert len(db_check) == 0

    else:
        list_to_compare = []
        for element_db_check in db_check:
            list_to_compare.append(element_db_check[3]["stringValue"])
        assert set(list_to_compare) == set(inbound_service)


@pytest.mark.parametrize(
    "case, expected_response_code",
    [
        ("WRONG_COUNTRY_UUID", 404),
        ("COUNTRY_ALREADY_DELETE", 200),
        ("WRONG_CARRIER_UUID", 404),
    ],
)
def test_delete_inbound_services_corner_cases(
    case,
    expected_response_code,
    factory_create_carriers,
    generate_uuid,
    random_country_code_uuid,
):
    """
    GIVEN a Numbers Management admin
    WHEN <case_mentioned>
    THEN an error should be raised
    """

    carrier_uuid = factory_create_carriers["carrierUUID"]

    if case == "WRONG_CARRIER_UUID":
        carrier_uuid = generate_uuid

    # ACCORDING TO THE SPEC, RESTRICTION SHOULD BE HANDLE BY FRONT PART
    elif case == "COUNTRY_ALREADY_DELETE":
        _ = carriers_client.carriers_delete_inbound_services_countries(
            carrier_uuid, random_country_code_uuid
        )

    elif case == "WRONG_COUNTRY_UUID":
        random_country_code_uuid = generate_uuid

    response = carriers_client.carriers_delete_inbound_services_countries(
        carrier_uuid, random_country_code_uuid
    )
    assert response.status_code == expected_response_code


@pytest.mark.parametrize(
    "carrier_uuid, case",
    [
        (lazy_fixture("carrier_link_inbound_services"), "COUNTRY_ONLY"),
        (
            lazy_fixture("carrier_link_inbound_services_and_area_code"),
            "COUNTRY_AND_REGIONS",
        ),
        (lazy_fixture("carrier_link_area_code"), "REGIONS_ONLY"),
    ],
    ids=[
        "COUNTRY_ONLY",
        "COUNTRY_AND_REGIONS",
        "REGIONS_ONLY",
    ],
)
def test_delete_inbound_services_business_case(
    carrier_uuid, case, france_country_code_uuid, list_fr_us_area_code_uuid
):
    """
    Given a carrier with inbound services and area code link to this carrier
    When I remove one country offered by a Carrier
    Then I should have a confirmation message with the number of inbound services deleted

    -------------------------------------------------------------------------------------------

    Given a carrier with inbound services and area code link to this carrier
    WHEN I remove a country offered by a Carrier
    THEN all the area codes that belong to this country have to be removed from the Carrier offering too
    AND THEN I should have a confirmation message with all the removed area codes and inbound services

    """
    fr_area_code = countries_client.countries_get_related_area_code("FRA", 2)

    response = carriers_client.carriers_delete_inbound_services_countries(
        carrier_uuid, france_country_code_uuid
    )
    response_json = response.json()

    if case == "COUNTRY_ONLY":
        assert response_json["resultCount"]["carrier_regions"] == 0
        assert response_json["resultCount"]["inbound_services"] == 1

        db_check = carriers_client.carriers_get_inbound_services(
            carrier_uuid, france_country_code_uuid
        )
        assert len(db_check) == 0

    if case == "COUNTRY_AND_REGIONS":
        assert response_json["resultCount"]["carrier_regions"] == 2
        assert response_json["resultCount"]["inbound_services"] == 1

        db_check = carriers_client.carriers_get_inbound_services(
            carrier_uuid, france_country_code_uuid
        )
        assert len(db_check) == 0

        for fra_element in fr_area_code:
            db_check = carriers_client.carriers_get_link_area_code(
                carrier_uuid, fra_element[0]["stringValue"]
            )
            assert len(db_check) == 0

    if case == "REGIONS_ONLY":
        assert response_json["resultCount"]["carrier_regions"] == 2
        assert response_json["resultCount"]["inbound_services"] == 0

        for fra_element in fr_area_code:
            db_check = carriers_client.carriers_get_link_area_code(
                carrier_uuid, fra_element[0]["stringValue"]
            )
            assert len(db_check) == 0
