import logging
import pytest


from back_tests.framework.numbers_management.carriers import carriers_client

log = logging.getLogger("TEST-CARRIERS")

# TODO: since it's one lambda per endpoint,
#  I have to redo the error management tests for each. I will think about making a factory test error handling.
@pytest.mark.parametrize(
    "case, expected_response_code",
    [
        ("WRONG_CAPABILITIES", 400),
        ("NO_COUNTRY_LIST", 400),
        ("WRONG_COUNTRY_LIST", 404),
        ("WRONG_COUNTRY_UUID", 404),
        ("COUNTRY_ALREADY_EXISTS", 409),
        ("NO_CAPABILITIES", 400),
        ("WRONG_CARRIER_UUID", 404),
    ],
)
def test_outbound_traffic_corner_cases(
    case,
    expected_response_code,
    factory_create_carriers,
    generate_uuid,
    france_country_code_uuid,
    list_country_code_uuid,
):
    """
    GIVEN a Numbers Management admin
    WHEN <case_mentioned>
    THEN an error should be raised
    """

    carrier_uuid = factory_create_carriers["carrierUUID"]

    if case == "WRONG_CARRIER_UUID":
        carrier_uuid = generate_uuid
        payload = {
            "capabilities": ["sms", "voice"],
            "countryList": [france_country_code_uuid],
            "enabled": False,
        }

    elif case == "NO_CAPABILITIES":
        payload = {
            "countryList": [france_country_code_uuid],
            "enabled": False,
        }

    elif case == "COUNTRY_ALREADY_EXISTS":
        payload = {
            "capabilities": ["sms", "voice"],
            "countryList": [france_country_code_uuid],
            "enabled": False,
        }
        _ = carriers_client.carriers_specify_outbound_traffic(carrier_uuid, payload)

    elif case == "WRONG_COUNTRY_UUID":
        payload = {
            "capabilities": ["sms", "voice"],
            "countryList": [generate_uuid],
            "enabled": False,
        }

    elif case == "NO_COUNTRY_LIST":
        payload = {
            "capabilities": ["sms", "voice"],
            "countryList": [],
            "enabled": False,
        }

    elif case == "WRONG_CAPABILITIES":
        payload = {
            "capabilities": ["WRONG_CAPABILITES", "voice"],
            "countryList": [france_country_code_uuid],
            "enabled": False,
        }

    elif case == "WRONG_COUNTRY_LIST":
        payload = {
            "capabilities": ["sms", "voice"],
            "countryList": list_country_code_uuid + [generate_uuid],
            "enabled": False,
        }

    response = carriers_client.carriers_specify_outbound_traffic(carrier_uuid, payload)
    assert response.status_code == expected_response_code


@pytest.mark.parametrize("capabilities", [["sms"], ["voice"], ["sms", "voice"]])
@pytest.mark.parametrize("enabled", [False, True])
def test_outbound_traffic_enabled_capabilities(
    capabilities, factory_create_carriers, france_country_code_uuid, enabled
):
    """
    GIVEN I’m a Numbers Management admin
    WHEN I specify the capabilities for the outbound traffic of a country
    THEN I expected to be able provide the type of capabilities : Voice, SMS, or both Voice and SMS can be specified
    -----------------------------------------------------------------------
    GIVEN I’m a Numbers Management admin
    WHEN I specify the capabilities for the outbound traffic of a country
    THEN I expected to be able enabled or disabled capabilities for this country
    """

    carrier_uuid = factory_create_carriers["carrierUUID"]

    payload = {
        "capabilities": capabilities,
        "countryList": [
            france_country_code_uuid,
        ],
        "enabled": enabled,
    }

    response = carriers_client.carriers_specify_outbound_traffic(carrier_uuid, payload)
    assert response.status_code == 201

    db_check = carriers_client.carriers_get_outbound_traffic(
        carrier_uuid, france_country_code_uuid
    )

    assert len(db_check) == len(payload["capabilities"])
    if capabilities == ["sms", "voice"]:
        for element in db_check:
            assert element[6]["stringValue"] in capabilities
            assert element[2]["booleanValue"] == enabled
    else:
        assert [db_check[0][6]["stringValue"]] == capabilities
        assert db_check[0][2]["booleanValue"] == enabled
    assert db_check


def test_outbound_traffic_bulk_countries(
    factory_create_carriers, list_country_code_uuid
):
    """
    GIVEN I’m a Numbers Management admin
    WHEN I want to link countries for outbound traffic to a carrier
    THEN I can specify several countries
    AND I expected the carrier and countries is link for target outbound traffic
    """
    carrier_uuid = factory_create_carriers["carrierUUID"]

    payload = {
        "capabilities": ["sms"],
        "countryList": list_country_code_uuid,
        "enabled": True,
    }

    response = carriers_client.carriers_specify_outbound_traffic(carrier_uuid, payload)

    assert response.status_code == 201

    for country_code_uuid in list_country_code_uuid:
        db_check = carriers_client.carriers_get_outbound_traffic(
            carrier_uuid, country_code_uuid
        )
        assert len(db_check) == len(payload["capabilities"])
        assert [db_check[0][6]["stringValue"]] == payload["capabilities"]
        assert db_check[0][2]["booleanValue"] == payload["enabled"]


@pytest.mark.parametrize(
    "case, expected_response_code",
    [
        ("WRONG_CARRIER_UUID", 404),
        ("NO_REGION_LIST", 400),
        ("WRONG_REGION_LIST", 404),
        ("REGION_ALREADY_DELETE", 200),
        ("WRONG_REGION_UUID", 404),
    ],
)
def test_delete_regions_link_corner_cases(
    case,
    expected_response_code,
    carrier_link_outbound_traffic,
    generate_uuid,
    france_country_code_uuid,
    list_country_code_uuid,
):
    """
    GIVEN a Numbers Management admin
    WHEN <case_mentioned>
    THEN an error should be raised
    """

    if case == "WRONG_CARRIER_UUID":
        carrier_link_outbound_traffic = generate_uuid
        payload = {"countryList": [france_country_code_uuid]}

    elif case == "REGION_ALREADY_DELETE":
        payload = {"countryList": [france_country_code_uuid]}
        _ = carriers_client.carriers_remove_outbound_traffic(
            carrier_link_outbound_traffic, payload
        )

    elif case == "WRONG_REGION_UUID":
        payload = {"countryList": [generate_uuid]}

    elif case == "NO_REGION_LIST":
        payload = {"countryList": []}

    elif case == "WRONG_REGION_LIST":
        payload = {"countryList": list_country_code_uuid + [generate_uuid]}

    response = carriers_client.carriers_remove_outbound_traffic(
        carrier_link_outbound_traffic, payload
    )
    assert response.status_code == expected_response_code

    if case == "REGION_ALREADY_DELETE":
        assert response.json()["resultCount"] == 0


def test_remove_outbound_traffic(
    carrier_link_outbound_traffic, france_country_code_uuid
):
    """
    GIVEN a carrier with outbound traffic linked to this carrier
    WHEN I delete this outbound traffic
    THEN I expected the carrier and outbound traffic is not linked anymore
    """
    payload = {"countryList": [france_country_code_uuid]}

    response = carriers_client.carriers_remove_outbound_traffic(
        carrier_link_outbound_traffic, payload
    )
    assert response.status_code == 200

    db_check = carriers_client.carriers_get_outbound_traffic(
        carrier_link_outbound_traffic, france_country_code_uuid
    )

    assert len(db_check) == 0


def test_bulk_remove_outbound_traffic(
    carrier_link_outbound_traffic, list_country_code_uuid
):
    """
    GIVEN a carrier with outbound traffic with several countrires linked to this carrier
    WHEN I delete this outbound traffic with all countries associated
    THEN I expected the carrier and each countries-outbound traffic is not linked anymore
    """

    payload = {"countryList": list_country_code_uuid}

    response = carriers_client.carriers_remove_outbound_traffic(
        carrier_link_outbound_traffic, payload
    )
    assert response.status_code == 200

    # EACH ELEMENT INSIDE THE LIST HAS BEEN REMOVE
    for country_uuid in list_country_code_uuid:

        db_check = carriers_client.carriers_get_outbound_traffic(
            carrier_link_outbound_traffic, country_uuid
        )

        assert len(db_check) == 0
