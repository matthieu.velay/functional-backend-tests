"""
Fixtures related to users lifecycle
"""

import json
import logging
import os

import pytest
from cryptography.fernet import Fernet

from back_tests.framework.common.phone import Phone
from back_tests.framework.common.utils import multithreaded

log = logging.getLogger("FIXTURES")

KEY = Fernet(os.environ["FERNET_KEY"].encode())


def _setup_agent(login, password):
    phone = Phone(login, password)
    log.info(phone)
    phone.set_availability(True)

    return phone


def _teardown_agent(phone):
    if phone.user_id:
        phone.logout()


@pytest.fixture(scope="session")
def return_users_credentials():
    with open("./json_store/Credentials", "r") as file:
        credentials = file.read().encode()

    return json.loads(KEY.decrypt(credentials))


@pytest.fixture(scope="function")
def bulk_creation_available_agent(request, return_users_credentials):
    log.info("Setup: {0}\n".format(request))

    # Use multithreading to create phones
    users = request.param
    creds = return_users_credentials
    log.info(users)
    args = [(login, password) for login, password in creds.items() if login in users]
    args = sorted(args)
    phones = multithreaded(len(args), args, _setup_agent)

    company_id = phones[0].company_id

    yield phones

    log.info("Teardown: {0}\n".format(request))

    # Use multithreading to delete phones
    args = [(phone.user_id, company_id) for phone in phones]
    multithreaded(len(phones), phones, _teardown_agent)
