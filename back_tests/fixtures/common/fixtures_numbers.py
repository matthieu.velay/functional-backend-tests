"""
Fixtures related to numbers configuration
"""

import json
import logging
import os

import pytest

from back_tests.framework.routing.dashboard import Dashboard, compare_dispatch_groups

log = logging.getLogger("FIXTURES")

CREDENTIALS_PATH = "json_store/calls_distribution/"


def _read_json_file(filepath):
    with open(CREDENTIALS_PATH + filepath, "r") as c_file:
        c_json = json.load(c_file)
    return c_json


def _check_distrib_consistency(
    dashboard_admin, number_id, agents_distrib, rqt_check=False
):
    login, password = dashboard_admin
    dashboard = Dashboard(login, password)
    number_data = dashboard.get_number_data(number_id)
    attributes = number_data["data"]["attributes"]
    receive_call_number = attributes["number_formatted"].replace(" ", "")
    # Loop on all group in dispatch groups
    for element1, element2 in zip(
        agents_distrib.get("data"),
        dashboard.get_dispatch_groups(number_id).get("data"),
    ):
        assert compare_dispatch_groups(
            element1, element2
        ), "Distribution calls differs from reference:\n{0}\n{1}".format(
            element1, element2
        )

    if rqt_check:
        assert (
            attributes["respect_queueing_time"] is True
        ), "RQT not enabled on number {0}".format(number_id)

    dashboard.logout()

    return receive_call_number


@pytest.fixture(scope="function")
def update_distrib(dashboard_admin_post, number_ids):
    """
    Update distribution items for the `updated number`
    """
    login, password = dashboard_admin_post
    dashboard = Dashboard(login, password)
    number_id = number_ids["updated_number"]

    def add_new_user(user_id_old, user_id_new):
        """
        Remove user_id_old from the distribution
        Add user_id_new in the distribution
        """
        status_code = dashboard.add_dispatch_group(number_id, user_id_new, 2)
        assert status_code == 200, status_code
        status_code = dashboard.delete_dispatch_group(number_id, user_id_old)
        assert status_code == 200, status_code
        return

    yield add_new_user

    # Teardown
    # reset the number distribution to the original one
    _reset_updated_number_distrib(dashboard, number_id)
    dashboard.logout()


def _reset_updated_number_distrib(dashboard, number_id):
    """
    Reset updated number to the original one
    """
    # Current groups, GET from the dashboard
    dispatch_groups = dashboard.get_dispatch_groups(number_id)
    groups = dispatch_groups["data"]

    # Expected group and user_id
    expected_group = _read_json_file("UpdatedNumber.json").get("data")[0]
    user_expected_id = expected_group["relationships"]["dispatchable"]["data"]["id"]

    payload = {
        "data": {
            "type": "dispatch_group",
            "attributes": {"priority": "1"},
            "relationships": {
                "dispatchable": {"data": {"id": f"{user_expected_id}", "type": "user"}}
            },
        }
    }
    ret = dashboard.add_dispatch_group(number_id, user_expected_id, 1)

    for group in groups:
        user_id = group["relationships"]["dispatchable"]["data"]["id"]
        if str(user_expected_id) != str(user_id):
            group_id = group["id"]
            log.info(f"We need to delete this group {group}")
            status_code = dashboard.delete_dispatch_group(number_id, user_id)
            assert status_code == 200


@pytest.fixture(scope="function")
def ivr_number(number_ids, dashboard_admin):
    return _get_formatted_number(dashboard_admin, number_ids["ivr"])


def _get_formatted_number(dashboard_admin, number_id):
    login, password = dashboard_admin
    dashboard = Dashboard(login, password)
    number_data = dashboard.get_number_data(number_id)
    receive_call_number = number_data["data"]["attributes"]["number_formatted"].replace(
        " ", ""
    )
    return receive_call_number


@pytest.fixture(scope="function")
def check_number_one_agent(number_ids, dashboard_admin, one_agent_distrib):
    return _check_distrib_consistency(
        dashboard_admin, number_ids["one_agent"], one_agent_distrib
    )


@pytest.fixture(scope="function")
def check_number_two_agents_simultaneous(
    number_ids, dashboard_admin, two_agents_one_team_distrib
):
    return _check_distrib_consistency(
        dashboard_admin, number_ids["two_agents_simult"], two_agents_one_team_distrib
    )


@pytest.fixture(scope="function")
def check_number_two_agents_random(
    number_ids, dashboard_admin, two_agents_one_team_random_distrib
):
    return _check_distrib_consistency(
        dashboard_admin,
        number_ids["two_agents_random_rqt"],
        two_agents_one_team_random_distrib,
        True,
    )


@pytest.fixture(scope="function")
def check_number_one_team_one_agent_random60_rqt(
    number_ids, dashboard_admin, one_team_one_agent_random60_rqt_distrib
):
    return _check_distrib_consistency(
        dashboard_admin,
        number_ids["one_team_one_agent_random60_rqt"],
        one_team_one_agent_random60_rqt_distrib,
        True,
    )


@pytest.fixture(scope="function")
def check_number_one_team_three_agents_simult60_rqt(
    number_ids, dashboard_admin, one_team_three_agents_simult60_rqt_distrib
):
    return _check_distrib_consistency(
        dashboard_admin,
        number_ids["one_team_three_agents_simult60_rqt"],
        one_team_three_agents_simult60_rqt_distrib,
        True,
    )


@pytest.fixture(scope="function")
def check_number_one_team_one_agent_random_rqt(
    number_ids, dashboard_admin, one_team_one_agent_random_rqt_distrib
):
    return _check_distrib_consistency(
        dashboard_admin,
        number_ids["one_team_one_agent_random_rqt"],
        one_team_one_agent_random_rqt_distrib,
        True,
    )


@pytest.fixture(scope="function")
def check_number_three_agents(number_ids, dashboard_admin, three_agents_distrib):
    return _check_distrib_consistency(
        dashboard_admin, number_ids["three_agents_one_team"], three_agents_distrib
    )


@pytest.fixture(scope="function")
def check_number_three_agents_one_team(
    number_ids, dashboard_admin, three_agents_one_team_distrib
):
    return _check_distrib_consistency(
        dashboard_admin,
        number_ids["three_agents_one_team"],
        three_agents_one_team_distrib,
    )


@pytest.fixture(scope="function")
def check_number_five_agents_simultaneous(
    number_ids, dashboard_admin, five_agents_distrib
):
    return _check_distrib_consistency(
        dashboard_admin, number_ids["five_agents_simult"], five_agents_distrib
    )


@pytest.fixture(scope="function")
def check_number_one_team_one_agent_random60(
    number_ids, dashboard_admin, one_team_one_agent_random60_distrib
):
    return _check_distrib_consistency(
        dashboard_admin,
        number_ids["one_team_one_agent_random60"],
        one_team_one_agent_random60_distrib,
        False,
    )


@pytest.fixture(scope="function")
def check_number_one_team_one_agent_simult(
    number_ids, dashboard_admin, one_team_one_agent_simult_distrib
):
    return _check_distrib_consistency(
        dashboard_admin,
        number_ids["one_team_one_agent_simult"],
        one_team_one_agent_simult_distrib,
    )


@pytest.fixture(scope="function")
def check_number_one_agent_two_teams(
    number_ids, dashboard_admin, one_agent_two_teams_distrib
):
    return _check_distrib_consistency(
        dashboard_admin, number_ids["one_agents_two_teams"], one_agent_two_teams_distrib
    )


@pytest.fixture(scope="function")
def check_number_two_agents_two_teams(
    number_ids, dashboard_admin, two_agents_two_teams_distrib
):
    return _check_distrib_consistency(
        dashboard_admin,
        number_ids["two_agents_two_teams"],
        two_agents_two_teams_distrib,
    )


@pytest.fixture(scope="function")
def check_number_three_agents_one_team_random60(
    number_ids, dashboard_admin, one_team_three_agents_random60_distrib
):
    return _check_distrib_consistency(
        dashboard_admin,
        number_ids["one_team_three_agents_random60_rqt"],
        one_team_three_agents_random60_distrib,
        True,
    )


@pytest.fixture(scope="function")
def check_number_three_agents_one_team_random20(
    number_ids, dashboard_admin, one_team_three_agents_random20_distrib
):
    return _check_distrib_consistency(
        dashboard_admin,
        number_ids["one_team_three_agents_random20"],
        one_team_three_agents_random20_distrib,
        True,
    )


@pytest.fixture(scope="function")
def check_updated_number(number_ids, dashboard_admin, updated_number_distrib):
    return _check_distrib_consistency(
        dashboard_admin,
        number_ids["updated_number"],
        updated_number_distrib,
    )


@pytest.fixture(scope="function")
def receive_call_number_transfer_one_agent():
    """Call distribution :
    Agent 1
    """
    return "+34980487058"


@pytest.fixture(scope="session")
def dashboard_admin():
    return os.environ["EMAIL_ADMIN"], os.environ["PASSWORD_ADMIN"]


@pytest.fixture(scope="session")
def dashboard_admin_post(return_users_credentials):
    return (
        "dashboard-admin@aircall.io",
        return_users_credentials["dashboard-admin@aircall.io"],
    )


@pytest.fixture(scope="function")
def one_agent_distrib():
    return _read_json_file("OneAgent.json")


@pytest.fixture(scope="session")
def three_agents_distrib():
    return _read_json_file("ThreeAgents.json")


@pytest.fixture(scope="session")
def three_agents_one_team_distrib():
    return _read_json_file("ThreeAgentsOneTeam.json")


@pytest.fixture(scope="function")
def two_agents_one_team_distrib():
    return _read_json_file("TwoAgentsOneTeam.json")


@pytest.fixture(scope="function")
def two_agents_one_team_random_distrib():
    return _read_json_file("TwoAgentsOneTeamRandom.json")


@pytest.fixture(scope="session")
def five_agents_distrib():
    return _read_json_file("FiveAgents.json")


@pytest.fixture(scope="session")
def one_team_one_agent_random_rqt_distrib():
    return _read_json_file("OneTeamOneAgentRandomRQT.json")


@pytest.fixture(scope="session")
def one_team_one_agent_random60_rqt_distrib():
    return _read_json_file("OneTeamOneAgentRandom60RQT.json")


@pytest.fixture(scope="session")
def one_team_three_agents_simult60_rqt_distrib():
    return _read_json_file("OneTeamThreeAgentsSimult60RQT.json")


@pytest.fixture(scope="session")
def one_team_one_agent_random60_distrib():
    return _read_json_file("OneTeamOneAgentRandom60.json")


@pytest.fixture(scope="session")
def one_team_one_agent_random20_distrib():
    return _read_json_file("OneTeamOneAgentRandom20.json")


@pytest.fixture(scope="session")
def one_team_three_agents_random60_distrib():
    return _read_json_file("ThreeAgentsOneTeamRandom60.json")


@pytest.fixture(scope="session")
def one_team_three_agents_random20_distrib():
    return _read_json_file("ThreeAgentsOneTeamRandom20.json")


@pytest.fixture(scope="session")
def one_team_one_agent_simult_distrib():
    return _read_json_file("OneTeamOneAgentSimult.json")


@pytest.fixture(scope="session")
def one_agent_two_teams_distrib():
    return _read_json_file("OneAgentTwoTeams.json")


@pytest.fixture(scope="session")
def two_agents_two_teams_distrib():
    return _read_json_file("TwoTeamsTwoAgents.json")


@pytest.fixture(scope="session")
def updated_number_distrib():
    return _read_json_file("UpdatedNumber.json")


@pytest.fixture(scope="function")
def external_call_number():
    return "+33186760900"


@pytest.fixture(scope="function")
def external_call_number_to_transfer():
    return "+33186760900"


@pytest.fixture(scope="function")
def public_api_inbound():
    return "+34950680127"


@pytest.fixture(scope="function")
def team_to_transfer():
    return 965


@pytest.fixture(scope="function")
def team_to_transfer_with_previous_agent():
    return 1019


@pytest.fixture(scope="function")
def team_4_users():
    return 1187


@pytest.fixture(scope="function")
def custom_ringing_time_number_wo_RQT():
    return "+34927880129"


@pytest.fixture(scope="function")
def custom_ringing_time_number_w_RQT():
    return "+34902018976"


@pytest.fixture(scope="function")
def external_call_number_to_transfer():
    return "+33186760900"


@pytest.fixture(scope="function")
def external_blacklisted_number():
    return "+33698661788"


@pytest.fixture(scope="function")
def out_of_opening_hours_number():
    return "+3253280270"


@pytest.fixture(scope="function")
def transfer_to_one_agent_id():
    return 18070


@pytest.fixture(scope="session")
def number_ids():
    # Aircall number IDs
    return {
        "two_agents_simult": "17751",
        "three_agents_one_team": "17602",
        "five_agents_simult": "17976",  # +34932713570
        "one_agent": "18070",
        "one_team_one_agent_random_rqt": "18083",
        "one_team_one_agent_random60": "18804",
        "one_team_one_agent_simult": "18084",
        "one_agents_two_teams": "18085",
        "two_agents_two_teams": "18100",
        "two_agents_random_rqt": "18301",
        "one_team_three_agents_simult60_rqt": "18803",
        "one_team_one_agent_random60_rqt": "18302",
        "one_team_three_agents_random60_rqt": "18338",
        "one_team_three_agents_random20": "18350",
        "ivr": "17824",
        "updated_number": "26171",
        "ringing_time": "27433",  # +34927880129
    }
