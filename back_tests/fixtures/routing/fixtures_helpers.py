import logging
import pytest
import time

from multiprocessing.dummy import Pool

from back_tests.framework.routing.dashboard import Dashboard
from back_tests.framework.routing.octopus import octopus
from back_tests.framework.common.twilio import twilio
from back_tests.framework.common.utils import is_in_range, multithreaded


# Timing
INCOMING_TIMEOUT = 10
OCTOPUS_TIMEOUT = 60
TWILIO_CALL_TIMEOUT = 30
VOICEMAIL_TIMEOUT = 150
LOOP_TIMEOUT = 0.5
DELAY_BETWEEN_CALLS = 1

log = logging.getLogger("FIXTURES-HELPERS-ROUTING")


@pytest.fixture
def wait_until_queue_octopus_is_not_empty():
    def waiting_function(company_id, timeout=OCTOPUS_TIMEOUT):
        start = time.time()
        log.info("Wait until queue is not empty - company: {0}".format(company_id))
        while not octopus.get_queue(company_id) and time.time() - start < timeout:
            time.sleep(LOOP_TIMEOUT)

        return octopus.get_queue(company_id)

    return waiting_function


@pytest.fixture
def wait_calls(ring_then_no_ring):
    def waiting_function(user_id, company_id, nr_calls):
        for _ in range(nr_calls):
            ring_then_no_ring(user_id, company_id)

    return waiting_function


@pytest.fixture
def wait_until_queue_octopus_is_empty():
    def waiting_function(
        company_id, timeout=OCTOPUS_TIMEOUT, loop_timeout=LOOP_TIMEOUT
    ):
        start = time.time()
        log.info("Wait until queue is empty - company: {0}".format(company_id))
        while octopus.get_queue(company_id) and time.time() - start < timeout:
            log.debug(octopus.get_queue(company_id))
            time.sleep(loop_timeout)

        return not octopus.get_queue(company_id)

    return waiting_function


@pytest.fixture
def wait_until_agent_ringing():
    def waiting_function(
        user_id,
        company_id,
        timeout=OCTOPUS_TIMEOUT,
        loop_timeout=LOOP_TIMEOUT,
    ):
        start = time.time()
        log.info("Wait until agent {0} is ringing".format(user_id))
        while (
            not octopus.is_agent_in_queue(user_id, company_id, "ringing")
            and time.time() - start < timeout
        ):
            time.sleep(loop_timeout)

        return octopus.is_agent_in_queue(user_id, company_id, "ringing")

    return waiting_function


@pytest.fixture
def wait_until_agent_not_ringing():
    def waiting_function(
        user_id,
        company_id,
        timeout=OCTOPUS_TIMEOUT,
        task_id=None,
        loop_timeout=LOOP_TIMEOUT,
    ):
        start = time.time()
        log.info("Wait until agent {0} is NOT ringing".format(user_id))
        while (
            octopus.is_agent_in_queue(user_id, company_id, "ringing", task_id)
            and time.time() - start < timeout
        ):
            time.sleep(loop_timeout)

        return not octopus.is_agent_in_queue(user_id, company_id, "ringing")

    return waiting_function


@pytest.fixture
def wait_until_agent_not_ringing_during():
    def waiting_function(user_id, company_id, timeout=OCTOPUS_TIMEOUT, task_id=None):
        start = time.time()
        log.info("Wait agent {0} is not ringing during {1}".format(user_id, timeout))
        while (
            not octopus.is_agent_in_queue(user_id, company_id, "ringing", task_id)
            and time.time() - start < timeout
        ):
            time.sleep(LOOP_TIMEOUT)

        return not octopus.is_agent_in_queue(user_id, company_id, "ringing")

    return waiting_function


@pytest.fixture
def wait_agent_not_in_queue_during():
    def waiting_function(user_id, company_id, timeout=30):
        start = time.time()
        found_agent = False
        log.info(
            "Wait during {0} seconds that agent {1} is not in the queue".format(
                timeout, user_id
            )
        )
        while not found_agent and time.time() - start < timeout:
            found_agent = octopus.is_agent_in_queue(user_id, company_id)
            time.sleep(1)
        return not found_agent

    return waiting_function


@pytest.fixture
def wait_until_agent_not_in_queue():
    def waiting_function(
        user_id, company_id, timeout=OCTOPUS_TIMEOUT, loop_timeout=LOOP_TIMEOUT
    ):
        start = time.time()
        log.info("Wait until agent {0} is not in the queue".format(user_id))
        while (
            octopus.is_agent_in_queue(user_id, company_id)
            and time.time() - start < timeout
        ):
            time.sleep(loop_timeout)

        return not octopus.is_agent_in_queue(user_id, company_id)

    return waiting_function


@pytest.fixture
def wait_until_voicemail_recorded():
    def waiting_function(phone, call_uuid, timeout=VOICEMAIL_TIMEOUT):
        start = time.time()
        log.info("Wait until call {0} is recorded".format(call_uuid))
        call = phone.get_call(call_uuid)
        proxied_voicemail_url = call.get("proxied_voicemail_url")

        # Wait until proxied_voicemail_url is set or timeout
        while (not call or not proxied_voicemail_url) and time.time() - start < timeout:
            call = phone.get_call(call_uuid)
            proxied_voicemail_url = call.get("proxied_voicemail_url")
            time.sleep(LOOP_TIMEOUT * 2)

        return proxied_voicemail_url

    return waiting_function


@pytest.fixture
def agents_ringing():
    def assert_function(phones):
        log.info("Compute agent octopus ringing state")
        pool = Pool(len(phones))
        company_id = phones[0].company_id
        args = [(agent.user_id, company_id, "ringing") for agent in phones]
        in_octopus_responses = pool.starmap(octopus.is_agent_in_queue, args)
        pool.close()
        pool.join()

        return in_octopus_responses

    return assert_function


@pytest.fixture(scope="function")
def ring_then_no_ring(wait_until_agent_ringing, wait_until_agent_not_ringing):
    def assert_function(user_id, company_id):
        assert wait_until_agent_ringing(
            user_id=user_id, company_id=company_id, loop_timeout=0.1
        )
        assert wait_until_agent_not_ringing(
            user_id=user_id, company_id=company_id, loop_timeout=0.1
        )

    return assert_function


@pytest.fixture
def wait_until_task_not_in_queue():
    def waiting_function(
        task_id, company_id, timeout=OCTOPUS_TIMEOUT, loop_timeout=LOOP_TIMEOUT
    ):
        start = time.time()
        log.info("Wait until task {0} is not in the queue".format(task_id))
        while (
            octopus.is_task_in_queue(task_id, company_id)
            and time.time() - start < timeout
        ):
            time.sleep(loop_timeout)

        return not octopus.is_task_in_queue(task_id, company_id)

    return waiting_function


@pytest.fixture(scope="function")
def incoming_calls(external_call_number):
    def create_calls(
        receive_call_number, nr_queued_calls, loop, delay=DELAY_BETWEEN_CALLS
    ):
        """
        Args:
            receive_call_number (str): phone number
            nr_queued_calls (int): number of calls to trigger
            loop (int): duration of the calls (1 -> 30 sec, 2 -> 6O sec, etc)
            delay (int): delay between each call
            If delay = 0, we use multithreading to trigger all the calls at the same time
        Returns:
            call_sid_list (list):  List of the twilio calls IDs created
        """
        if not delay:
            args = [
                (external_call_number, receive_call_number, loop)
                for _ in range(nr_queued_calls)
            ]
            call_sid_list = multithreaded(nr_queued_calls, args, twilio.inbound_call)
            log.info("Concurrent Twilio calls: {0}".format(call_sid_list))
        else:
            call_sid_list = []
            for _ in range(nr_queued_calls):
                current_call = twilio.inbound_call(
                    external_call_number, receive_call_number, loop
                )
                call_info = twilio.get_call(current_call)
                log.info("{0}: {1}".format(current_call, call_info.status))
                call_sid_list.append(current_call)
                # Add this delay to control calls order
                time.sleep(delay)
        return call_sid_list

    return create_calls


@pytest.fixture(scope="function")
def measure_ringing_time(wait_until_agent_ringing, wait_until_agent_not_ringing):
    def wait_function(phone, ringing_time_expected, acceptance_percentage):

        user_id = phone.user_id
        company_id = phone.company_id

        assert wait_until_agent_ringing(
            user_id=user_id, company_id=company_id, loop_timeout=0.1
        )
        # Start the stopwatch
        start = time.time()

        # Store the call ID
        call_id = phone.task_id

        # Agent did not pick up the call, wait for call end
        wait_time = ringing_time_expected * (1 + 2 * acceptance_percentage)
        assert wait_until_agent_not_ringing(
            user_id=user_id,
            company_id=company_id,
            loop_timeout=0.1,
            timeout=wait_time,
        ), log.error("Ringing time > {0}".format(wait_time))

        # Compute ringing time
        end = time.time()
        ringing_time = end - start

        return ringing_time, call_id

    return wait_function


@pytest.fixture(scope="function")
def first_ringing():
    def wait_function(phones):
        start = time.time()
        not_received = [not phone.received for phone in phones]

        while (all(not_received)) and time.time() - start < INCOMING_TIMEOUT:
            not_received = [not phone.received for phone in phones]
            continue

        index = not_received.index(False)
        first_ringing = phones[index]
        log.info("Agent {0} first to receive the call".format(first_ringing.user_id))
        return first_ringing

    return wait_function


@pytest.fixture(scope="function")
def ringing_order():
    """
    Order phones by ringing time
    """

    def wait_function(phones):
        start = time.time()
        not_received = [not phone.received for phone in phones]

        while (all(not_received)) and time.time() - start < INCOMING_TIMEOUT:
            not_received = [not phone.received for phone in phones]
            continue

        # Sort by timestamp of the first received call
        phones_active = [phone for phone in phones if phone.received]
        phones_active.sort(key=lambda x: x.received_w_time[0][1])

        # Handle phones with no call received
        phones_inactive = [phone for phone in phones if not phone.received]

        phones = phones_active + phones_inactive

        log.info("Ringing order: {0}".format([phone.user_id for phone in phones]))

        return phones

    return wait_function


@pytest.fixture(scope="function")
def check_ringing_time():
    """
    Check ringing time is respected for the current phone
    """

    def loop_function(phone, ringing_time_expected_l, acceptance_percentage_l):
        # Get received and rejected time lists from phone object
        received_l = phone.received_w_time
        rejected_l = phone.rejected_w_time

        for rejected, received, ringing_time_expected, acceptance_percentage in zip(
            rejected_l, received_l, ringing_time_expected_l, acceptance_percentage_l
        ):
            call_id, t_rejected = rejected
            call_id2, t_received = received
            # Compute ringing time for each call
            ringing_time = t_rejected - t_received
            assert call_id == call_id2
            log.info(
                "Ringing time: {0}- expected: {1}".format(
                    ringing_time, ringing_time_expected
                )
            )
            # Ensure the ringing time is correct
            assert is_in_range(
                ringing_time, ringing_time_expected, acceptance_percentage
            ), ringing_time

    return loop_function


@pytest.fixture(scope="function")
def wait_until_call_status(dashboard_admin):
    """
    Wait until the call state is `expected_state`
    """

    def wait_function(call_id, number_id, expected_state, timeout=10):
        login, password = dashboard_admin
        dashboard = Dashboard(login, password)

        call_info = dashboard.get_call_info(call_id, number_id)
        state = call_info["state"]
        start = time.time()
        while state != expected_state and (time.time() - start < timeout):
            call_info = dashboard.get_call_info(call_id, number_id)
            state = call_info["state"]
        return state == expected_state

    return wait_function
