import logging
import pytest
import threading
import time

from back_tests.framework.routing.public_api import public_api
from back_tests.framework.routing.webhook.flask_server import FlaskServer
from back_tests.framework.routing.webhook.ngrok_tunnel import NgrokTunnel

log = logging.getLogger("FIXTURES-PUBLIC-API")

# Timing
WEBHOOK_ID = 1347


@pytest.fixture(scope="function")
def wait_until_transfer_succeed():
    def wait_function(current_call_id, payload, timeout=30):
        ret = public_api.transfers(current_call_id, payload)
        status_code = ret.status_code
        start = time.time()
        while status_code != 204 and time.time() - start < timeout:
            ret = public_api.transfers(current_call_id, payload)
            status_code = ret.status_code
        log.info("Transfer status code: {0}".format(status_code))
        return status_code == 204

    return wait_function


@pytest.fixture(scope="session")
def activate_webhook(webhook_id=WEBHOOK_ID):
    """
    Activate the webhook WEBHOOK_ID via public API
    """
    ret = public_api.update_webhook(webhook_id)
    assert ret["webhook"]["active"] is True


@pytest.fixture(scope="function")
def wait_until_call_created(daemon_flask_server):
    """
    Wait until the webhook call.created is received
    """

    def wait_function(timeout=30):
        start = time.time()
        log.info(daemon_flask_server.call_created)

        while not daemon_flask_server.call_created and (time.time() - start < timeout):
            time.sleep(0.5)
            log.info(daemon_flask_server.call_created)
        return daemon_flask_server.call_created

    return wait_function


@pytest.fixture(scope="function")
def wait_until_call_answered(daemon_flask_server):
    """
    Wait until the webhook call.answered is received
    """

    def wait_function(timeout=30):
        start = time.time()
        log.info(daemon_flask_server.call_answered)

        while not daemon_flask_server.call_answered and time.time() - start < timeout:
            time.sleep(0.5)
            log.info(daemon_flask_server.call_answered)
        return daemon_flask_server.call_answered

    return wait_function


@pytest.fixture(scope="session")
def daemon_flask_server():
    server = FlaskServer()
    thread = threading.Thread(target=server.start_app)
    thread.daemon = True
    thread.start()
    yield server


@pytest.fixture(scope="session")
def daemon_ngrok_tunnel():
    server = NgrokTunnel()
    thread = threading.Thread(target=server.start)
    thread.daemon = True
    thread.start()
    yield server
    server.stop()


@pytest.fixture(scope="function")
def reset_calls_server(daemon_flask_server):
    daemon_flask_server.reset()
    yield
    daemon_flask_server.reset()
