"""
Fixtures related to User Management
"""
import logging
import pytest
import random
import string
import os

from back_tests.framework.common.utils import multithreaded
from back_tests.framework.user_management.usm_login import UserManagement

log = logging.getLogger("FIXTURES")


def _usm_sign_in(login, password):
    user_management = UserManagement(login, password)
    return user_management


def _teardown_sign_out(user_management):
    if user_management.idToken:
        user_management.usm_sign_out(user_management.idToken)


@pytest.fixture(scope="function")
def bulk_usm_sign_in(request, return_users_credentials):
    log.info("Usm sign in: {0}\n".format(request))

    # Use multithreading to sign in user management
    users = request.param
    creds = return_users_credentials
    args = [(login, password) for login, password in creds.items() if login in users]
    args = sorted(args)
    user_managements = multithreaded(len(args), args, _usm_sign_in)

    yield user_managements

    log.info("Teardown: {0}\n".format(request))

    # Use multithreading to sign out user management
    multithreaded(len(user_managements), user_managements, _teardown_sign_out)


@pytest.fixture(scope="function")
def user_management_invite_firstname():
    return "nurinvite"


@pytest.fixture(scope="function")
def user_management_invite_lastname():
    return "senerinvite"


@pytest.fixture(scope="function")
def user_management_invite_isAdmin():
    return "true"


@pytest.fixture(scope="function")
def user_management_invite_language():
    return "en-US"


@pytest.fixture(scope="function")
def create_user_management_invite_email():
    def generate(size):
        emailname = "nur.sener+"
        emaildomain = "@aircall.io"
        value = (
            emailname
            + "".join(random.choices(string.digits + string.ascii_lowercase, k=size))
            + emaildomain
        )
        return value

    return generate
