import pytest


@pytest.fixture
def countries_updated_license(date_time_iso_format):
    payload = {"license": "licensed", "licenseObtainedDate": "fds"}
    return payload
