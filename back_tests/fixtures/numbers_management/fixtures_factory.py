import pytest
import logging
from back_tests.framework.numbers_management.carriers import carriers_client

log = logging.getLogger("FACTORY")


@pytest.fixture
def factory_create_carriers(create_carriers_payload_basics):
    response = carriers_client.carriers_create(create_carriers_payload_basics)

    if response.status_code != 201:
        log.info("Factory create carrier : {}".format(response.json()))
        raise Exception("Sorry, we can't create the carrier")

    return response.json()


@pytest.fixture
def carrier_link_area_code(list_fr_us_area_code_uuid, factory_create_carriers):

    carrier_uuid = factory_create_carriers["carrierUUID"]

    payload = {
        "capabilities": ["sms", "voice"],
        "regionList": list_fr_us_area_code_uuid,
        "supportRequired": True,
        "enabled": True,
    }

    response = carriers_client.carriers_add_regions_params(carrier_uuid, payload)

    if response.status_code != 201:
        raise Exception("Sorry, we can't update the carrier")
    return carrier_uuid


@pytest.fixture
def carrier_link_outbound_traffic(list_country_code_uuid, factory_create_carriers):

    carrier_uuid = factory_create_carriers["carrierUUID"]

    payload = {
        "capabilities": ["sms"],
        "countryList": list_country_code_uuid,
        "enabled": True,
    }

    response = carriers_client.carriers_specify_outbound_traffic(carrier_uuid, payload)

    if response.status_code != 201:
        log.info("Factory outbound traffic : {}".format(response.json()))
        raise Exception("Sorry, we can't add outbound traffic into the carrier")

    return carrier_uuid


@pytest.fixture
def carrier_link_inbound_services(
    list_country_code_uuid, factory_create_carriers, france_country_code_uuid
):

    carrier_uuid = factory_create_carriers["carrierUUID"]
    payload = {"inboundServices": ["cnam"]}

    response = carriers_client.carriers_create_inbound_services_countries(
        carrier_uuid, france_country_code_uuid, payload
    )

    if response.status_code != 201:
        log.info("Factory outbound traffic : {}".format(response.json()))
        raise Exception("Sorry, we can't add inbound service into the carrier")

    return carrier_uuid


@pytest.fixture
def carrier_link_inbound_services_and_area_code(
    factory_create_carriers,
    carrier_link_inbound_services,
    carrier_link_area_code,
):

    carrier_uuid = factory_create_carriers["carrierUUID"]
    return carrier_uuid
