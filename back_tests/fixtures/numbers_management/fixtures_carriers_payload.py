import uuid
import pytest


@pytest.fixture
def create_carriers_payload_basics():
    payload = {
        "name": "QA-" + str(uuid.uuid4()),
        "mainAccount": "QA",
        "portalLink": "QA-test.aircall.io",
        "numbersOfferings": [
            {
                "offering": "reselling",
            }
        ],
    }
    return payload


@pytest.fixture(scope="session")
def create_carriers_payload_basics_static():
    payload = {
        "name": "QA-static-name",
        "mainAccount": "QA",
        "portalLink": "QA-test.aircall.io",
        "numbersOfferings": [
            {
                "offering": "reselling",
            }
        ],
    }
    return payload


@pytest.fixture
def create_carriers_payload_without_name():
    payload = {
        "mainAccount": "QA",
        "portalLink": "QA-test.aircall.io",
        "numbersOfferings": [
            {
                "offering": "reselling",
            }
        ],
    }
    return payload


@pytest.fixture
def create_carriers_payload_empty_payload():
    payload = {}
    return payload


@pytest.fixture
def create_carriers_payload_without_numbers_offering():
    payload = {
        "name": "QA-" + str(uuid.uuid4()),
        "mainAccount": "QA",
        "portalLink": "QA-test.aircall.io",
    }
    return payload


@pytest.fixture
def create_carriers_with_wrong_numbers_offerings():
    payload = {
        "name": "QA-" + str(uuid.uuid4()),
        "mainAccount": "QA",
        "portalLink": "QA-test.aircall.io",
        "numbersOfferings": [
            {
                "offering": "WRONG_OFFERING",
            }
        ],
    }
    return payload


@pytest.fixture
def create_carriers_with_partner_and_company_numbers_offerings():
    payload = {
        "name": "QA-" + str(uuid.uuid4()),
        "mainAccount": "QA",
        "portalLink": "QA-test.aircall.io",
        "numbersOfferings": [
            {"offering": "reselling", "partnerId": "12"},
            {
                "companyId": "123",
                "offering": "hosting",
            },
            {
                "companyId": "bring_your_own_carrier",
                "offering": "hosting",
            },
        ],
    }
    return payload


@pytest.fixture
def create_carriers_with_no_numbers_offerings():
    payload = {
        "name": "QA-" + str(uuid.uuid4()),
        "mainAccount": "QA",
        "portalLink": "QA-test.aircall.io",
        "numbersOfferings": [],
    }
    return payload


@pytest.fixture
def create_carriers_payload_with_api_offering():
    payload = {
        "name": "QA-" + str(uuid.uuid4()),
        "mainAccount": "QA",
        "portalLink": "QA-test.aircall.io",
        "numbersOfferings": [
            {
                "offering": "reselling",
            }
        ],
        "apiOffers": ["phone_ordering"],
    }
    return payload


@pytest.fixture
def create_carriers_payload_with_several_api_offering():
    payload = {
        "name": "QA-" + str(uuid.uuid4()),
        "mainAccount": "QA",
        "portalLink": "QA-test.aircall.io",
        "numbersOfferings": [
            {
                "offering": "reselling",
            }
        ],
        "apiOffers": [
            "phone_ordering",
            "cnam",
            "proof_management",
            "portabilities",
            "emergency_service",
        ],
    }
    return payload


@pytest.fixture
def create_carriers_payload_with_wrong_api_offering():
    payload = {
        "name": "QA-" + str(uuid.uuid4()),
        "mainAccount": "QA",
        "portalLink": "QA-test.aircall.io",
        "numbersOfferings": [
            {
                "offering": "reselling",
            }
        ],
        "apiOffers": ["WRONG_OFFERS"],
    }
    return payload


@pytest.fixture
def create_carriers_payload_with_contacts():
    payload = {
        "name": "QA-" + str(uuid.uuid4()),
        "mainAccount": "QA",
        "portalLink": "QA-test.aircall.io",
        "numbersOfferings": [
            {
                "offering": "reselling",
            }
        ],
        "contacts": [
            {
                "country": "FR",
                "role": "toto",
                "phoneNumber": "00000",
                "city": "FR",
                "surname": "Hello",
                "name": "toto",
                "isResponsiveFor": "string",
                "email": "1",
            },
            {
                "country": "FR",
                "role": "toto",
                "phoneNumber": "00000",
                "city": "FR",
                "surname": "Hello",
                "name": "toto",
                "isResponsiveFor": "string",
                "email": "2",
            },
        ],
    }
    return payload


@pytest.fixture
def create_carriers_payload_minimal_requirement_contacts():
    payload = {
        "name": "QA-" + str(uuid.uuid4()),
        "mainAccount": "QA",
        "portalLink": "QA-test.aircall.io",
        "numbersOfferings": [
            {
                "offering": "reselling",
            }
        ],
        "contacts": [
            {
                "name": "QA-Team",
                "email": "NOT_VERIFIED",
            },
            {
                "name": "QA-Team_2",
                "email": "NOT_VERIFIED_2",
            },
        ],
    }
    return payload


@pytest.fixture
def create_carriers_payload_wrong_contacts():
    payload = {
        "name": "QA-" + str(uuid.uuid4()),
        "mainAccount": "QA",
        "portalLink": "QA-test.aircall.io",
        "numbersOfferings": [
            {
                "offering": "reselling",
            }
        ],
        "contacts": [
            {
                "email": "WRONG_PAYLOAD",
            },
        ],
    }
    return payload


@pytest.fixture
def create_carriers_payload_with_trunks():
    payload = {
        "name": "QA-" + str(uuid.uuid4()),
        "mainAccount": "QA",
        "portalLink": "QA-test.aircall.io",
        "numbersOfferings": [
            {
                "offering": "reselling",
            }
        ],
        "trunks": [
            {
                "identifier": "HELLO",
                "numberOfChannels": 12345,
                "pointOfConnection": "WORLD",
            }
        ],
    }
    return payload


@pytest.fixture
def create_carriers_payload_with_wrong_trunks():
    payload = {
        "name": "QA-" + str(uuid.uuid4()),
        "mainAccount": "QA",
        "portalLink": "QA-test.aircall.io",
        "numbersOfferings": [
            {
                "offering": "reselling",
            }
        ],
        "trunks": [
            {
                "numberOfChannels": 12345,
                "pointOfConnection": "WORLD",
            }
        ],
    }
    return payload


@pytest.fixture
def create_carriers_payload_with_too_long_integer_trunks():
    payload = {
        "name": "QA-" + str(uuid.uuid4()),
        "mainAccount": "QA",
        "portalLink": "QA-test.aircall.io",
        "numbersOfferings": [
            {
                "offering": "reselling",
            }
        ],
        "trunks": [
            {
                "identifier": "HELLO",
                "numberOfChannels": 123423456789009,
                "pointOfConnection": "WORLD",
            }
        ],
    }
    return payload
