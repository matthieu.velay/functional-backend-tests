import pytest
from back_tests.framework.numbers_management.countries import countries_client


@pytest.fixture(scope="session")
def fra_area_code_uuid():
    """
    FRANCE REGION_AREA_CODE
    """
    response = countries_client.countries_get_related_area_code("FRA", 1)
    return response[0][0]["stringValue"]


@pytest.fixture(scope="session")
def tur_area_code_uuid_never_link():
    """
    location: NULL CODE
    type: national
    """
    response = countries_client.countries_get_related_area_code("TUR", 1)
    return response[0][0]["stringValue"]


@pytest.fixture(scope="session")
def list_fr_us_area_code_uuid():
    list_area_code_usa_fra = []
    fr_area_code = countries_client.countries_get_related_area_code("FRA", 2)
    usa_area_code = countries_client.countries_get_related_area_code("USA", 2)

    for fra_element, usa_element in zip(fr_area_code, usa_area_code):
        list_area_code_usa_fra.extend(
            (fra_element[0]["stringValue"], usa_element[0]["stringValue"])
        )

    return list_area_code_usa_fra


@pytest.fixture(scope="session")
def france_country_code_uuid():
    """FRANCE COUNTRY CODE"""
    response = countries_client.countries_get_by_alpha3_code("FRA")
    return response[0][0]["stringValue"]


@pytest.fixture(scope="session")
def usa_country_code_uuid():
    """USA COUNTRY CODE"""
    response = countries_client.countries_get_by_alpha3_code("USA")
    return response[0][0]["stringValue"]


@pytest.fixture(scope="session")
def turkey_country_code_uuid():
    """TURKEY COUNTRY CODE"""
    response = countries_client.countries_get_by_alpha3_code("TUR")
    return response[0][0]["stringValue"]


@pytest.fixture(scope="session")
def list_country_code_uuid(france_country_code_uuid, usa_country_code_uuid):
    """LIST COUNTRY CODE : FRANCE AND USA"""
    return [france_country_code_uuid, usa_country_code_uuid]


@pytest.fixture
def random_country_code_uuid():
    response = countries_client.countries_get_random_code_uuid()
    return response[0][0]["stringValue"]
