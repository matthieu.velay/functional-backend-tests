import pytest
from datetime import datetime
import uuid


@pytest.fixture
def date_time_iso_format():
    return datetime.now().isoformat().__str__() + "Z"


@pytest.fixture
def generate_uuid():
    return str(uuid.uuid4())
