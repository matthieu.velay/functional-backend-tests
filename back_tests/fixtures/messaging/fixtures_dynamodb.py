"""
Fixtures related to DynamoDB
"""

import logging

import pytest

from back_tests.framework.messaging.messaging_dynamoDB import MessagingDynamoDB

log = logging.getLogger("FIXTURES")


@pytest.fixture
def get_current_numberofMessages():
    def get_number(agent_id):
        dynamodb_conn = MessagingDynamoDB()
        dynamodb_conn.get_dynamodb_agentlimit(agent_id)
        currrent_numberOfMessages = dynamodb_conn.nbOfMessage
        return currrent_numberOfMessages

    return get_number


@pytest.fixture
def update_numberofMessages():
    def update_number(agent_id, number):
        dynamodb_conn = MessagingDynamoDB()
        dynamodb_conn.get_dynamodb_agentlimit(agent_id)
        dynamodb_conn.update_dynamodb_agentlimit(agent_id, number)

    return update_number


@pytest.fixture(scope="function")
def dynamodb_setup_teardown_for_agent(
    request, get_current_numberofMessages, update_numberofMessages
):

    agent_ids_str = request.param
    agent_ids = [
        request.getfixturevalue(agent_id_str) for agent_id_str in agent_ids_str
    ]

    log.info("Setup DynamoDB connection for: {0}\n".format(agent_ids))

    numberofmessageslist = []
    for agent_id in agent_ids:
        numberofMessage = get_current_numberofMessages(agent_id)
        numberofmessageslist.append(numberofMessage)

    yield

    log.info("Teardown DynamoDB connection for: {0}\n".format(agent_ids))

    for agent_id, numberofMessage in zip(agent_ids, numberofmessageslist):
        update_numberofMessages(agent_id, numberofMessage)
