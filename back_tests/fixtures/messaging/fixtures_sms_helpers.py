"""
Fixtures related to Messaging
"""

import json
import logging
import os
import string
import random
import datetime
import emoji

import pytest


log = logging.getLogger("FIXTURES")


@pytest.fixture(scope="function")
def create_huge_string():
    def set_size(size):
        huge_string = "".join(
            random.choices(
                string.ascii_uppercase
                + string.digits
                + string.ascii_letters
                + string.punctuation,
                k=size,
            )
        )
        return huge_string

    return set_size


@pytest.fixture(scope="function")
def create_emoji():
    def set_emoji_text(text):
        emoji_ret = emoji.emojize(text, use_aliases=True)
        return emoji_ret

    return set_emoji_text


@pytest.fixture(scope="function")
def create_random_messagesid():
    def generate(size):
        chars = "SM"
        value = chars + "".join(
            random.choices(string.digits + string.ascii_lowercase, k=size)
        )
        return value

    return generate


@pytest.fixture(scope="function")
def twilio_from_number():
    return "+13238709383"


@pytest.fixture(scope="function")
def twilio_from_number_without_plus():
    return "13238709383"


@pytest.fixture(scope="function")
def twilio_to_number():
    return "+17272633485"


@pytest.fixture(scope="function")
def twilio_wrong_from_number():
    return "+132"


@pytest.fixture(scope="function")
def twilio_wrong_to_number():
    return "+172"


@pytest.fixture(scope="function")
def messaging_external_us_number():
    return "17272633485"


@pytest.fixture(scope="function")
def messaging_us_line_id():
    return "18563"


@pytest.fixture(scope="function")
def messaging_external_us_number1():
    return "17602309624"


@pytest.fixture(scope="function")
def messaging_us_line_id1():
    return "18564"


@pytest.fixture(scope="function")
def messaging_wrong_external_number():
    return "1727"


@pytest.fixture(scope="function")
def messaging_wrong_line_id():
    return "13445"


@pytest.fixture(scope="function")
def messaging_same_number_with_us_line_id():
    return "17272633485"


@pytest.fixture(scope="function")
def messaging_unauth_line_id():
    return "26629"


@pytest.fixture(scope="function")
def messaging_non_covered_area_line_id():
    return "22485"


@pytest.fixture(scope="function")
def messaging_canadian_line_id():
    return "21492"


@pytest.fixture(scope="function")
def messaging_backend_agent_id():
    return 49926


@pytest.fixture(scope="function")
def messaging_backend_company_id():
    return 39098


@pytest.fixture(scope="function")
def messaging_backend_unauth_company_id():
    return 38780


@pytest.fixture(scope="function")
def messaging_backend_unauth_agent_id():
    return 49974


@pytest.fixture(scope="function")
def messaging_gb_number():
    return "447723955400"


@pytest.fixture(scope="function")
def messaging_gb_line_id():
    return "21136"


@pytest.fixture(scope="function")
def messaging_gb_number1():
    return "447830366250"


@pytest.fixture(scope="function")
def messaging_gb_line_id1():
    return "21137"


@pytest.fixture(scope="function")
def messaging_backend_agent_id2():
    return 49927


@pytest.fixture(scope="function")
def messaging_non_sms_capable_gb_number():
    return "441855432000"


@pytest.fixture(scope="function")
def messaging_non_sms_capable_gb_line_id():
    return "21372"


@pytest.fixture(scope="function")
def twilio_receive_sms_body():
    return "Receive SMS from Twilio"


@pytest.fixture(scope="function")
def twilio_nummedia0():
    return "0"


@pytest.fixture(scope="function")
def twilio_nummedia1():
    return "1"


@pytest.fixture(scope="function")
def messaging_canadian_number():
    return "18192005387"


@pytest.fixture(scope="function")
def messaging_canadian_line_id():
    return "22473"


@pytest.fixture(scope="function")
def messaging_canadian_number1():
    return "12267967649"


@pytest.fixture(scope="function")
def messaging_canadian_line_id1():
    return "22474"


@pytest.fixture(scope="function")
def messaging_australian_number():
    return "61480014481"


@pytest.fixture(scope="function")
def messaging_australian_line_id():
    return "22484"


@pytest.fixture(scope="function")
def messaging_australian_number1():
    return "61480015338"


@pytest.fixture(scope="function")
def messaging_australian_line_id1():
    return "23571"


@pytest.fixture(scope="function")
def messaging_french_number():
    return "33644644059"


@pytest.fixture(scope="function")
def messaging_french_line_id():
    return "22481"


@pytest.fixture(scope="function")
def messaging_french_number1():
    return "33644602496"


@pytest.fixture(scope="function")
def messaging_french_line_id1():
    return "22482"


@pytest.fixture(scope="function")
def messaging_limit_line_id():
    return "22520"


@pytest.fixture(scope="function")
def messaging_limit_agent_id():
    return 50217


@pytest.fixture(scope="function")
def messaging_limit_company_id():
    return 41882


@pytest.fixture(scope="function")
def messsaging_text():
    return "Test backend messaging"


@pytest.fixture(scope="function")
def empty_text():
    return ""


@pytest.fixture(scope="function")
def text_with_space():
    return " "


@pytest.fixture(scope="function")
def empty_ext_num():
    return ""


@pytest.fixture(scope="function")
def empty_text_error_msg():
    return {"message": '"text" is not allowed to be empty', "key": "VALIDATION_ERROR"}


@pytest.fixture(scope="function")
def internalapi_empty_text_error_msg():
    return '"text" is not allowed to be empty'


@pytest.fixture(scope="function")
def text_space_error_msg():
    return {
        "message": "Validation error: message text can not be empty",
        "key": "EmptyMessageText",
    }


@pytest.fixture(scope="function")
def internalapi_text_space_error_msg():
    return "Validation error: message text can not be empty"


@pytest.fixture(scope="function")
def empty_ext_num_error_msg():
    return {
        "message": '"externalNumber" is not allowed to be empty',
        "key": "VALIDATION_ERROR",
    }


@pytest.fixture(scope="function")
def internalapi_empty_ext_num_error_msg():
    return '"externalNumber" is not allowed to be empty'


@pytest.fixture(scope="function")
def wrong_ext_num_error_msg():
    return {
        "message": 'The "externalNumber" property contains an invalid phone number.',
        "key": "VALIDATION_ERROR",
    }


@pytest.fixture(scope="function")
def internalapi_wrong_ext_num_error_msg():
    return 'The "externalNumber" property contains an invalid phone number.'


@pytest.fixture(scope="function")
def wrong_line_id_error_msg():
    return {
        "message": "Validation error: the line does not exist",
        "key": "LineDoesNotExist",
    }


@pytest.fixture(scope="function")
def internalapi_wrong_line_id_error_msg():
    return "Validation error: the line does not exist"


@pytest.fixture(scope="function")
def agent_itself_error_msg():
    return {
        "message": "Validation error: the receiver should be different from the sender",
        "key": "SameDestinationAsSender",
    }


@pytest.fixture(scope="function")
def internalapi_agent_itself_error_msg():
    return "Validation error: the receiver should be different from the sender"


@pytest.fixture(scope="function")
def unauth_company_error_msg():
    return {
        "message": "Forbidden access: messaging is not enabled for this company",
        "key": "MessagingIsNotEnabled",
    }


@pytest.fixture(scope="function")
def internalapi_unauth_company_error_msg():
    return "Forbidden access: messaging is not enabled for this company"


@pytest.fixture(scope="function")
def uncovered_country_error_msg():
    return {
        "message": "Validation error: the country is not covered",
        "key": "CountryIsNotCovered",
    }


@pytest.fixture(scope="function")
def internalapi_uncovered_country_error_msg():
    return "Validation error: the country is not covered"


@pytest.fixture(scope="function")
def between_internationl_messages_error_msg():
    return {
        "message": "Validation error: the line can not handle international conversations",
        "key": "CanNotHandleInternationalConversations",
    }


@pytest.fixture(scope="function")
def internalapi_between_internationl_messages_error_msg():
    return "Validation error: the line can not handle international conversations"


@pytest.fixture(scope="function")
def chars_1601_messages_error_msg():
    return {
        "message": "Validation error: message length exceeds the maximum of 1600 characters",
        "key": "MessageTooLong",
    }


@pytest.fixture(scope="function")
def internalapi_chars_1601_messages_error_msg():
    return "Validation error: message length exceeds the maximum of 1600 characters"


@pytest.fixture(scope="function")
def receive_empty_message_error_msg():
    return {
        "message": "Validation error: the message does not have content",
        "key": "MessageDoesNotHaveContent",
    }


@pytest.fixture(scope="function")
def receive_empty_ToNum_error_msg():
    return {
        "message": '"To" is not allowed to be empty',
        "key": "VALIDATION_ERROR",
    }


@pytest.fixture(scope="function")
def receive_empty_FromNum_error_msg():
    return {
        "message": '"From" is not allowed to be empty',
        "key": "VALIDATION_ERROR",
    }


@pytest.fixture(scope="function")
def receive_invalid_ToNum_error_msg():
    return {
        "message": 'The "To" property contains an invalid phone number.',
        "key": "VALIDATION_ERROR",
    }


@pytest.fixture(scope="function")
def receive_invalid_FromNum_error_msg():
    return {
        "message": 'The "From" property contains an invalid phone number.',
        "key": "VALIDATION_ERROR",
    }
