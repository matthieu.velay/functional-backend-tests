import json
from random import uniform, choices
from time import sleep


def _sleep_time():
    random_time_to_wait = round(uniform(0.1, 0.2), 3)
    sleep(random_time_to_wait)


def _status_code(error_rate=0.0):
    """
    @param error_rate: HTTP error rate
    @type error_rate: float

    @return status_code: HTTP status code
    @rtype: int
    """
    status_code = choices(
        [200, 500, 400], [1 - 2 * error_rate, error_rate, error_rate], k=1
    )[0]

    return status_code


def inbound_calls(event, context):
    _sleep_time()
    body = {"message": "inbound_calls response", "input": event}

    response = {"statusCode": _status_code(error_rate=0.1), "body": json.dumps(body)}

    return response


def inbound_call_summaries(event, context):
    _sleep_time()
    body = {"message": "inbound_call_summaries response", "input": event}

    response = {"statusCode": _status_code(), "body": json.dumps(body)}

    return response


def inbound_call_callbacks(event, context):
    _sleep_time()
    body = {"message": "inbound_call_callbacks response", "input": event}

    response = {"statusCode": _status_code(), "body": json.dumps(body)}

    return response


def flows(event, context):
    _sleep_time()
    body = {"message": "flows response", "input": event}

    response = {"statusCode": _status_code(), "body": json.dumps(body)}

    return response
