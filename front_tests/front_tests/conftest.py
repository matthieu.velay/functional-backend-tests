import pytest

from .fixtures_helpers import *
from .fixtures_numbers import *
from .fixtures_users import *

# from back_tests.fixtures.common.fixtures_helpers import *


def pytest_addoption(parser):
    parser.addoption(
        "--no-head",
        action="store_false",
        help="Arguments to start the browser without headless mode",
    )


@pytest.fixture
def no_head(request):
    return request.config.getoption("--no-head")
