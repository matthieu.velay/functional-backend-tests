import pytest
import os
import logging
import json

from selenium import webdriver
from front_tests.framework.user import User
from cryptography.fernet import Fernet

KEY = Fernet(os.environ["FERNET_KEY"].encode())

log = logging.getLogger("FIXTURES | USERS")


@pytest.fixture(scope="function")
def inbound_agent_email():
    return "aircall1@aircall.io"


@pytest.fixture(scope="function")
def inbound_agent_password(inbound_agent_email, return_users_credentials):
    return return_users_credentials.get(inbound_agent_email)


@pytest.fixture(scope="session")
def return_users_credentials(environment):
    """
    Decrypt the file with user credentials
    @return: json with user credentials
    """
    if environment == "staging":
        creds_file = "./json_store/Credentials"
    elif environment == "production":
        creds_file = "./json_store/Credentials-Prod"
    else:
        raise Exception("env must be staging or production")
    with open(creds_file, "r") as file:
        credentials = file.read().encode()

    return json.loads(KEY.decrypt(credentials))


@pytest.fixture
def initiate_driver(no_head):
    """
    Initiate a google chrome webdriver
    @return: google chrome webdriver
    """

    def _init_driver():
        log.info("chromedriver Initialization")

        chrome_driver = "/usr/local/bin/chromedriver"
        # Options for connect phone communication
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--incognito")
        chrome_options.add_argument("--use-fake-ui-for-media-stream")
        chrome_options.add_argument("--use-fake-device-for-media-stream")
        chrome_options.add_argument("--disable-dev-shm-usage")

        if no_head:
            chrome_options.add_argument("--headless")
        else:
            log.info("NO HEADLESS MODE ENABLE")

        chrome_options.add_argument("--no-sandbox")

        driver = webdriver.Chrome(chrome_driver, options=chrome_options)
        # driver.maximize_window() # option
        driver.implicitly_wait(10)
        return driver

    return _init_driver


@pytest.fixture(scope="function")
def open_user_available(inbound_agent_email, inbound_agent_password, initiate_driver):
    """
    Create a available user with target credentials
    @param inbound_agent_email: email address
    @param inbound_agent_password: password
    """
    user = User(inbound_agent_email, inbound_agent_password, initiate_driver())
    log.info("user {0} is logged in".format(user.email))

    yield user
    log.info("user {0} is logged out".format(user.email))
    user.driver.quit()


@pytest.fixture(scope="function")
def open_user():
    def open_user_wrap(email, password):
        """
        Create a available user with target credentials
        @param email: email address
        @param password: password
        """
        user = User(email, password, initiate_driver())
        log.info("user {0} is logged in".format(user.email))

        return user

    return open_user_wrap


@pytest.fixture(scope="function")
def open_user_do_not_disturb(
    inbound_agent_email, inbound_agent_password, initiate_driver
):
    """
    Create a dnd user with target credentials
    @param inbound_agent_email: email address's user
    @param inbound_agent_password: password's user
    """
    user = User(inbound_agent_email, inbound_agent_password, initiate_driver())
    log.info("user {0} is logged in".format(user.email))
    user.switch_availability("do not disturb")

    yield user
    log.info("user {0} is logged out".format(user.email))
    user.switch_availability("available")
    user.driver.quit()


def _user_login(email, password, dnd_status, initiate_driver):
    """
    @param email:
    @param password:
    @param dnd_status:
    @return: user instance
    """
    user = User(email, password, initiate_driver)
    log.info("user {0} is logged in".format(user.email))
    if dnd_status:
        user.switch_availability("do not disturb")
    return user


# TODO: do not disturb setting
def _user_teardown(user):
    """
    User closes the browser
    @param user: target's user
    """
    if not user.is_available() and not user.is_incall():
        user.switch_availability("available")
    user.driver.quit()
    log.info("user {0} is logged out".format(user.email))


# TODO: rework login password
@pytest.fixture(scope="function")
def bulk_creation_available_agent(
    request, multiprocessing, return_users_credentials, initiate_driver
):
    """
    Create several users
    @param return_users_credentials: fixture function (see documentation related)
    @param request: fixture function where the parametrization is defined
    @param multiprocessing:  fixture function (see documentation related)
    """
    users = request.param
    users = [
        (email, return_users_credentials.get(email), status, initiate_driver())
        for email, status in users
    ]

    phones = multiprocessing(_user_login, users)

    yield phones

    multiprocessing(_user_teardown, phones)
