import pytest


@pytest.fixture(scope="function")
def external_call_number():
    """TwiML number :
    - Inbound : answers automatically and plays music
    - Outbound : plays music
    """
    return "+33186760900"


@pytest.fixture(scope="function")
def external_blacklisted_number():
    """
    blacklisted number on staging
    """
    return "+33640041234"


@pytest.fixture(scope="function")
def receive_call_number_one_agent():
    """Call distribution :
    Agent 1
    """
    return "+3253280675"


@pytest.fixture(scope="function")
def receive_call_number_team_random_one_agent():
    """Call distribution :
    Team A - Random - 1 min queuing time
        Agent 1
    """
    return "+3253280674"


@pytest.fixture(scope="function")
def receive_call_number_rqt_team_random_one_agent():
    """Call distribution :
    Team A - Random - 1 min queuing time
        Agent 1
    """
    return "+3278482376"


@pytest.fixture(scope="function")
def receive_call_number_team_simultaneous_one_agent():
    """Call distribution :
    Team A - Simultaneous - 1 min queuing time
        Agent 1
    """
    return "+3253280676"


@pytest.fixture(scope="function")
def receive_call_number_one_agent_two_teams():
    """Call distribution :
    Team A : Random - Queueing time 1 min
        Agent 1

    Team B : Simultaneous - Queueing time 1 min
        Agent 1
    """
    return "+3253280677"


@pytest.fixture(scope="function")
def receive_call_two_agents():
    """Call distribution :
    Team A : Random - Queueing time 1 min
        Agent 1
        Agent 2
    """
    return "+3253280688"


@pytest.fixture(scope="function")
def receive_call_rqt_two_agents():
    """Call distribution :
    Team A : Random - Queueing time 1 min
        Agent 1
        Agent 2
    """
    return "+3253280700"


@pytest.fixture(scope="function")
def receive_call_three_agents_20s():
    """Call distribution :
    Team A : Random - Queueing time 20 seconds
        Agent 1
        Agent 2
        Agent 3
    """
    return "+3268480269"


@pytest.fixture(scope="function")
def receive_call_three_agents_rqt():
    """Call distribution :
    Team A : Random - Queueing time 1 minute
        Agent 1
        Agent 2
        Agent 3
    """
    return "+34950684003"


@pytest.fixture(scope="function")
def receive_call_two_agent_two_teams():
    """Call distribution :
    Team A : Random - Queueing time 1 min
        Agent 1
        Agent 2

    Team B : Simultaneous - Queueing time 1 min
        Agent 1
        Agent 2
    """
    return "+3253280678"


@pytest.fixture(scope="function")
def receive_call_number_public_api():
    """Call distribution :
    Agent 1
    """
    return "+3250580344"


@pytest.fixture(scope="function")
def blacklisted_number():
    """Call distribution :
    Agent 1 :  aircall2@aircall.io
    """
    return "+3253280259"


@pytest.fixture(scope="function")
def team_public_api_id():
    """
    aircall2@aircall.io
    aircall3@aircall.io
    """
    return "1177"


@pytest.fixture(scope="function")
def user_public_api_id():
    """
    user_id = aircall2@aircall.io
    """

    return "41804"


@pytest.fixture(scope="function")
def call_service_number(environment):
    """Call distribution :
    Agent 1 :  Call Service
    """
    if environment == "staging":
        return "+1331-256-9842"
    elif environment == "production":
        return "+3283480116"
