import functools
import operator
import pytest
import time
import logging
import json
import os


from selenium.common.exceptions import TimeoutException, NoSuchElementException
from multiprocessing.dummy import Pool
from time import sleep
from random import randint
from back_tests.framework.common.twilio import twilio

TIMEOUT = 65
DEFAULT_PERCENT = 10

PERCENT = lambda part, whole: float(whole) / 100 * float(part)

# Recordings
RECORDING_USER_PATTERN = "matthieu.velay+record"
NR_RECORDING_CALLS = 10  # per user
NR_RECORDING_CONCURRENT_USERS = 2  # Up to 20


log = logging.getLogger("FIXTURES | HELPERS")


@pytest.fixture(scope="session")
def environment():
    return os.getenv("ENV")


@pytest.fixture(autouse=True)
def footer_function_duration():
    """Report test durations after each function."""
    start = time.time()
    yield
    stop = time.time()
    delta = round(stop - start, 2)
    log.info("Test duration : {0} seconds".format(delta))


@pytest.fixture
def multiprocessing():
    """
    Multiprocessing a target function
    """

    def pool_sub_function(function, args):
        """
        Multiprocessing a target function

        @param function: function to processing
        @param args: arguments of function
        @return: results of function
        """
        pool = Pool(len(args))
        log.info("Starting the pool")

        if type(args[0]) is tuple:
            try:
                result = pool.starmap(function, args)
                log.info("Terminating the pool")
                pool.terminate()
                pool.join()
                return result

            except Exception as e:
                log.error("Terminating the pool with error : {0}".format(e))
                pool.terminate()
                pool.join()
        else:
            try:
                result = pool.map(function, args)
                log.info("Terminating the pool")
                pool.terminate()
                pool.join()
                return result
            except Exception as e:
                log.error("Terminating the pool with error : {0}".format(e))
                pool.terminate()
                pool.join()

    return pool_sub_function


@pytest.fixture
def create_external_calls(external_call_number):
    """
    create several external callers
    @param external_call_number: default external call number
    """

    def _create_external_calls(
        receive_call_number,
        external_call_number_twilio=external_call_number,
        twiml_loop=1,
        number_of_calls=1,
        delays_between_calls=0,
    ):
        """
        create several external callers

        @param receive_call_number: call recipient number
        @param external_call_number_twilio: callers number
        @param twiml_loop: number of times we'll play the twiml music
        @param number_of_calls: number of calls we make
        @param delays_between_calls: delay between each call
        @return: list of call_uuid
        """

        external_calls_sid = []

        for _ in range(number_of_calls):
            external_calls_sid.append(
                twilio.inbound_call(
                    external_call_number_twilio, receive_call_number, twiml_loop
                )
            )

            sleep(delays_between_calls)
        return external_calls_sid

    return _create_external_calls


@pytest.fixture
def measuring_time_ringing():
    """
    measures how long time a target user has been ringing
    """

    def _measuring_time_ringing(user):
        """
        measures how long time a target user has been ringing
        @param user: target user
        @return: time the user has rung
        """
        log.info("{0} starts ring".format(user.email))

        user.driver.implicitly_wait(0)
        start = time.time()

        while (
            user.is_ringing() or user.is_ringing_minimize()
        ) and time.time() - start < TIMEOUT:
            continue

        end = time.time()
        final_time = round(end - start, 2)
        user.driver.implicitly_wait(10)
        log.info("{0} ends ring at {1} ".format(user.email, final_time))

        return final_time

    return _measuring_time_ringing


@pytest.fixture
def interval_measuring():
    """
    Validate if a number is bounded by a target number (bound delta percent)
    """

    def _interval_measuring(number_to_check, bound, bounded_percent=DEFAULT_PERCENT):
        """
        Validate if a number is bounded by a target number (bound delta percent)
        @param number_to_check: number to compare with the bound
        @param bound: inclusion bound
        @return: boolean
        """
        log.info(
            "interval measuring : {0} bounded by {1} ".format(number_to_check, bound)
        )

        delta_bound = PERCENT(bounded_percent, bound)
        if bound - delta_bound < number_to_check < bound + delta_bound:
            return True
        else:
            return False

    return _interval_measuring


@pytest.fixture
def make_outbound_call_and_wait():
    """
    Given a number and user, compute a outbound call
    """

    def _make_outbound_call_and_wait(user, number):
        """
        Given a number and user, compute a outbound call
        @param user: user used to make the call
        @param number: number to call
        """
        log.info("user : {0} calls {1} ".format(user.email, number))

        user.click_on_keyboard()
        user.make_outbound_call(number)
        user.wait_until_incall()

    return _make_outbound_call_and_wait


@pytest.fixture
def make_outbound_call():
    """
    Given a number and user, compute a outbound call
    """

    def _make_outbound_call(user, number):
        """
        Given a number and user, compute a outbound call
        @param user: user used to make the call
        @param number: number to call
        """
        log.info("user : {0} calls {1} ".format(user.email, number))

        user.click_on_keyboard()
        user.make_outbound_call(number)

    return _make_outbound_call


@pytest.fixture
def call_ringing_then_stop():
    """
    Check the call is ringing then stop (keyboard dial present)
    """

    def _call_ringing_then_stop(user, timeout):
        """Check the call is ringing then stop (keyboard dial present)

        @param user: user used to make the call
        @param timeout: waiting time
        """
        start = time.time()
        while time.time() - start < timeout and (not user.is_ringing_minimize()):
            time.sleep(0.1)

        assert user.is_ringing_minimize()

        start = time.time()
        while time.time() - start < timeout and (not user.is_keyboard_dial()):
            time.sleep(0.1)

        assert user.is_keyboard_dial()

    return _call_ringing_then_stop


@pytest.fixture
def call_ringing():
    """
    Check the call is ringing
    """

    def _call_ringing(user, timeout):
        """Check the call is ringing

        @param user: user used to make the call
        @param timeout: waiting time
        """
        start = time.time()
        while time.time() - start < timeout and (not user.is_ringing_minimize()):
            time.sleep(0.1)

        assert user.is_ringing_minimize()

    return _call_ringing


@pytest.fixture
def user_action(measuring_time_ringing, interval_measuring, external_call_number):
    """
    Execute an action on target user
    """

    def _user_action(
        user, action, waiting_until_ringing_timeout=30, timeout_ringing=25
    ):
        """
        Execute an action on target user
        @param user: user target
        @param action: action to execute
        @param waiting_until_ringing_timeout: timeout defines for wait until user ringing
        @param timeout_ringing: time that the user should ring
        """
        if action == "decline":
            log.info("user action : {0} declines".format(user.email))
            user.wait_until_receive_call(waiting_until_ringing_timeout)
            sleep(randint(1, 5))  # To be less aggressive on declines
            user.click_on_decline()

        if action == "accept":
            log.info("user action : {0} accepts".format(user.email))
            user.wait_until_receive_call(waiting_until_ringing_timeout)
            sleep(randint(2, 5))  # To be less aggressive on hang up
            user.click_on_answer()

        if action == "raise_exception":
            log.info("user action : {0} doesn't ring".format(user.email))
            with pytest.raises((TimeoutException, NoSuchElementException)):
                user.wait_until_receive_call(waiting_until_ringing_timeout)

        if action == "not_pick_up":
            log.info("user action : {0} not pick-up".format(user.email))
            user.wait_until_receive_call(waiting_until_ringing_timeout)
            time_measuring = measuring_time_ringing(user)
            assert interval_measuring(time_measuring, timeout_ringing)

        if action == "busy_to_available_ring":
            log.info("user action: {0} is busy then available".format(user.email))
            sleep(randint(5, 30))  # To be less aggressive on hang up
            user.hang_up()

            log.info("user action : {0} becomes available".format(user.email))
            user.wait_until_receive_call(waiting_until_ringing_timeout)
            time_measuring = measuring_time_ringing(user)

            if timeout_ringing == 0:
                assert time_measuring > 0
            else:
                assert interval_measuring(time_measuring, timeout_ringing)

        if action == "available_busy":
            user.wait_until_receive_call()
            sleep(randint(1, 5))  # To be less aggressive on minimize
            user.click_minimize()
            user.click_on_keyboard()
            user.make_outbound_call(external_call_number)

        if action == "just-ringing":
            log.info("user action : {0} not pick-up".format(user.email))
            user.wait_until_receive_call(waiting_until_ringing_timeout)
            time_measuring = measuring_time_ringing(user)
            assert time_measuring > 0

    return _user_action


@pytest.fixture
def call_timeline(measuring_time_ringing, interval_measuring, external_call_number):
    """
    This function defines the call timeline for a target user during X seconds where X equals to timeout_loop
    """

    def _call_timeline(user, waiting_until_ringing_timeout=5, timeout_loop=30):
        """
        This function defines the call timeline for a target user during X seconds where X equals to timeout_loop
        @param user: user target
        @param waiting_until_ringing_timeout: timeout defines for wait until user ringing
        @param timeout_loop: time waiting before break the loop
        """

        start = time.time()

        while time.time() - start < timeout_loop:
            try:
                user.wait_until_receive_call(waiting_until_ringing_timeout)
                call_info = {
                    "user_email": user.email,
                    "ringing_at": time.strftime("%H:%M:%S"),
                    "call_uuid": user.get_call_uuid(),
                    "duration_ringing": measuring_time_ringing(user),
                }
                user.add_call_info(call_info)

            except TimeoutException:
                continue

    return _call_timeline


@pytest.fixture
def print_json_formatted():
    """displays a well-formatted json"""

    def _print_json_formatted(to_be_formatted):
        print(json.dumps(to_be_formatted, indent=4))

    return _print_json_formatted


@pytest.fixture
def sorted_call_timeline(print_json_formatted):
    """
    sorted a list of json by default key ringing_at
    @param print_json_formatted: displays a well-formatted json
    @param users: list of users each with their own call timeline
    @param target_key: the key used to sort the list by
    @return: sorted list of json
    """

    def _sorted_call_timeline(users, target_key="ringing_at"):

        list_sorted = [user.incoming_call_info for user in users]
        list_sorted = functools.reduce(operator.iconcat, list_sorted, [])
        list_sorted.sort(key=lambda item: (item[target_key], item["duration_ringing"]))

        print_json_formatted(list_sorted)
        return list_sorted

    return _sorted_call_timeline


def _outbound_quick_hangup(
    open_user, email, pwd, nr_calls, make_outbound_call_and_wait, external_call_number
):
    """
    Make nr_calls outbound calls and hang up after 2 seconds
    """
    user = open_user(email, pwd)
    for _ in range(nr_calls):
        make_outbound_call_and_wait(user, external_call_number)
        time.sleep(2)
        user.hang_up()


@pytest.fixture
def create_recordings(
    open_user,
    multiprocessing,
    external_call_number,
    receive_call_number_one_agent,
    user_action,
    make_outbound_call_and_wait,
    return_users_credentials,
):
    """
    Make outbound calls to a number with recordings enabled
    """
    users = {
        email: pwd
        for email, pwd in return_users_credentials.items()
        if RECORDING_USER_PATTERN in email
    }
    args = [
        (
            open_user,
            email,
            pwd,
            NR_RECORDING_CALLS,
            make_outbound_call_and_wait,
            external_call_number,
        )
        for email, pwd in users.items()
    ][:NR_RECORDING_CONCURRENT_USERS]

    multiprocessing(_outbound_quick_hangup, args)


@pytest.fixture
def checking_is_in_call():
    def _all_check(user):

        user.wait_until_hold_button_clickable()
        user.wait_until_mute_button_clickable()
        user.wait_until_keypad_button_clickable()
        user.wait_until_recording_button_clickable()
        user.wait_until_adding_button_clickable()
        user.wait_until_transfer_button_clickable()

    return _all_check


@pytest.fixture
def outbound_call_checking(make_outbound_call_and_wait, checking_is_in_call):
    def _outbound_call_and_check(user, number):
        make_outbound_call_and_wait(user, number)
        checking_is_in_call(user)

    return _outbound_call_and_check


@pytest.fixture
def call_ended_or_keyboard():
    def _call_ended_or_keyboard(user, timeout=30):
        start = time.time()
        is_keyboard = user.is_keyboard_dial()
        is_ended = user.is_call_ended()
        while time.time() - start < timeout and (not is_keyboard and not is_ended):
            is_keyboard = user.is_keyboard_dial()
            is_ended = user.is_call_ended()

        return is_keyboard or is_ended

    return _call_ended_or_keyboard


@pytest.fixture
def outbound_call_failed(make_outbound_call_and_wait):
    def _outbound_call_and_check(user, number):
        make_outbound_call_and_wait(user, number)
        checking_is_in_call(user)

    return _outbound_call_and_check


@pytest.fixture
def inbound_call_checking(checking_is_in_call):
    def _answered_call_and_check(user):
        log.info("user action : {0} accepts".format(user.email))
        user.wait_until_receive_call()
        sleep(randint(2, 5))  # To be less aggressive on hang up
        user.click_on_answer()
        checking_is_in_call(user)

    return _answered_call_and_check
