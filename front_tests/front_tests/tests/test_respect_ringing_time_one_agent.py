import pytest
from pytest_testrail.plugin import pytestrail
from back_tests.framework.common.twilio import twilio


@pytestrail.case("C10782724")
def test_one_agent_available(
    open_user_available,
    external_call_number,
    receive_call_number_one_agent,
    user_action,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Agent 1 : -
            Agent 1 Available
        And a external caller A

    When the external caller A calls ‘Baguette'

    Then I expect:
        Agent 1 rings during 25 s BUT don't pick up
        When Agent 1 timeouts
        Then the call ends
    """
    # external caller A calls ‘Baguette'
    call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_one_agent, 3
    )

    # Agent 1 rings during 25 s BUT don't pick up
    user_action(open_user_available, "not_pick_up", 30, 25)

    # Then the call ends
    assert twilio.waiting_until_call_status(call_sid, "completed", 10)


@pytestrail.case("C10782727")
def test_team_random_one_agent_available(
    open_user_available,
    external_call_number,
    receive_call_number_team_random_one_agent,
    user_action,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Team A - Random - 1 min queuing time
            Agent 1 Available
        And a external caller A

    When the external caller A calls ‘Baguette'

    Then I expect:
        Agent 1 rings during 25 s BUT don't pick up
        When Agent 1 timeouts
        Then the call ends
    """

    # external caller A calls ‘Baguette'
    call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_team_random_one_agent, 3
    )

    # Agent 1 rings during 25 s BUT don't pick up
    user_action(open_user_available, "not_pick_up", 30, 25)

    # Then the call ends
    assert twilio.waiting_until_call_status(call_sid, "completed", 10)


@pytestrail.case("C10782728")
def test_team_simultaneous_one_agent_available(
    open_user_available,
    external_call_number,
    receive_call_number_team_simultaneous_one_agent,
    user_action,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Team A - Simultaneous - 1 min queuing time
            Agent 1 Available
        And a external caller A

        When the external caller A calls ‘Baguette'

        Then I expect:
            Agent 1 rings during 1 min BUT don't pick up
            When Agent 1 timeouts
            Then the call ends
    """
    # external caller A calls ‘Baguette'
    call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_team_simultaneous_one_agent, 3
    )

    # Agent 1 rings during 1m BUT don't pick up
    user_action(open_user_available, "not_pick_up", 30, 60)

    # Then the call ends
    assert twilio.waiting_until_call_status(call_sid, "completed", 10)


@pytestrail.case("C10782740")
def test_one_agent_dnd(
    open_user_do_not_disturb,
    external_call_number,
    receive_call_number_one_agent,
    user_action,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Agent 1 : -
            Agent 1 Unavailable
        And a external caller A

    When the external caller A calls ‘Baguette'

    And there are no available users on any of the distribution items:

    Agent 1 is Offline or Do not disturb
    Then I expect the call to not be dispatched
    Then the call ends
    """
    # external caller A calls ‘Baguette'
    call_sid = twilio.inbound_call(external_call_number, receive_call_number_one_agent)

    # Then I expect the call to not be dispatched
    user_action(open_user_do_not_disturb, "raise_exception", 10)

    # Then the call ends
    assert twilio.waiting_until_call_status(call_sid, "completed", 10)


@pytestrail.case("C10782725")
def test_one_agent_busy(
    open_user_available,
    external_call_number,
    receive_call_number_one_agent,
    user_action,
    make_outbound_call_and_wait,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Agent 1 : -
            Agent 1 Busy
        And a external caller A

    When the external caller A calls ‘Baguette'

    Then the routing will try to ring Agent 1 for 25 s checking if it has become available,
    When calling time is elapsed
    Then the call ends
    """
    # Make Agent 1 in busy
    make_outbound_call_and_wait(open_user_available, external_call_number)

    # external caller A calls ‘Baguette'
    call_sid = twilio.inbound_call(external_call_number, receive_call_number_one_agent)

    # Then I expect the call to not be dispatched
    user_action(open_user_available, "raise_exception", 25)

    # Then the call ends
    assert twilio.waiting_until_call_status(call_sid, "completed", 10)


@pytestrail.case("C10782729")
def test_one_agent_two_teams(
    open_user_available,
    external_call_number,
    receive_call_number_one_agent_two_teams,
    user_action,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Team A : Random - Queueing time 1 min
            Agent 1 Available

        Team B : Simultaneous - Queueing time 1 min
            Agent 1 Available

        And a external caller A

    When the external caller A calls ‘Baguette'

    Then I expected Team A:
    Agent 1 rings during 25 s BUT don't pick up
    When Agent 1 timeouts

    Then I expected Team B:
    Agent 1 rings during 1 min BUT don't pick up
    Then the call ends
    """

    # external caller A calls ‘Baguette'
    call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_one_agent_two_teams, 5
    )

    # Agent 1 rings during 25 s BUT don't pick up
    user_action(open_user_available, "not_pick_up", 30, 25)

    # Agent 1 rings during 1 min BUT don't pick up
    user_action(open_user_available, "not_pick_up", 60, 60)

    # Then the call ends
    assert twilio.waiting_until_call_status(call_sid, "completed", 10)


def test_one_agent_two_teams_declines(
    open_user_available,
    external_call_number,
    receive_call_number_one_agent_two_teams,
    user_action,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Team A : Random - Queueing time 1 min
            Agent 1 Available

        Team B : Simultaneous - Queueing time 1 min
            Agent 1 Available

        And a external caller A

    When the external caller A calls ‘Baguette'

    Then I expected Team A:
    Agent 1 rings during and declines

    Then I expected Team B:
    Agent 1 doesn't ringing

    Then the call ends
    """

    # external caller A calls ‘Baguette'
    call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_one_agent_two_teams, 5
    )

    # Agent 1 rings during and declines
    user_action(open_user_available, "decline", 30)

    # Agent 1 doesn't ringing
    user_action(open_user_available, "raise_exception", 30)

    # Then the call ends
    assert twilio.waiting_until_call_status(call_sid, "completed", 10)


@pytest.mark.skip(reason="deprecated phone feature")
def test_one_agent_available_then_busy(
    open_user_available,
    external_call_number,
    receive_call_number_one_agent,
    user_action,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Agent 1 : -
            Agent 1 Available then busy
        And a external caller A

    When the external caller A calls ‘Baguette'

    Then I expect:
        Agent 1 rings and then becomes busy
        When Agent 1 timeouts
        Then the call ends
    """

    # external caller A calls ‘Baguette'
    call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_one_agent, 3
    )

    # Agent 1 rings during and declines
    user_action(open_user_available, "available_busy", 30)

    # Then the call ends
    assert twilio.waiting_until_call_status(call_sid, "completed", 30)
