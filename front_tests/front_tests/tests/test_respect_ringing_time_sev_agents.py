import pytest
from pytest_testrail.plugin import pytestrail

from back_tests.framework.common.twilio import twilio


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall1@aircall.io", False), ("aircall2@aircall.io", False)]],
    indirect=True,
)
@pytestrail.case("C10782730")
def test_two_agents_available(
    bulk_creation_available_agent,
    external_call_number,
    receive_call_two_agent_two_teams,
    multiprocessing,
    user_action,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Team A : Random - Queueing time 1 min
            Agent 1 Available
            Agent 2 Available

        Team B : Simultaneous - Queueing time 1 min
            Agent 1 Available
            Agent 2 Available

        And a external caller A

    When the external caller A calls ‘Baguette'

    Then I expect Team A:
        A random agent (Agent 1) rings during 25 s BUT don't pick up
        When Agent 1 timeouts after 25 s ringing
        Then I expect another agent in the same team to ring
        Another agent (Agent 2) rings during 25 s BUT don't pick up

    When all agents timeouts after 25 s ringing each

    Then I expect Team B:
        All agents to ring simultaneously (Agent 1, Agent 2) during 1 min BUT don't pick up

    Then the call ends
    """
    user_1, user_2 = bulk_creation_available_agent

    # external caller A calls ‘Baguette'
    call_sid = twilio.inbound_call(
        external_call_number, receive_call_two_agent_two_teams, 5
    )

    # Expected Team A
    args = [
        (user_1, "not_pick_up", 60, 25),
        (user_2, "not_pick_up", 60, 25),
    ]

    multiprocessing(user_action, args)

    # Expect Team B
    args = [
        (user_1, "not_pick_up", 30, 60),
        (user_2, "not_pick_up", 30, 60),
    ]

    multiprocessing(user_action, args)

    # Then expected call end
    assert twilio.waiting_until_call_status(call_sid, "completed", 10)


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall1@aircall.io", False), ("aircall2@aircall.io", False)]],
    indirect=True,
)
@pytestrail.case("C10782733")
def test_two_agents_one_declines(
    bulk_creation_available_agent,
    external_call_number,
    receive_call_two_agent_two_teams,
    user_action,
    multiprocessing,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Team A : Random - Queueing time 1 min
            Agent 1 Available
            Agent 2 Available

        Team B : Simultaneous - Queueing time 1 min
            Agent 1 Available
            Agent 2 Available

        And a external caller A

    When the external caller A calls ‘Baguette'

    Then I expect Team A:
        A random agent (Agent 1) rings and declines
        Then I expect another agent in the same team to ring
        Another agent (Agent 2) rings during 25 s BUT don't pick up

    When all agents timeouts after 25 s ringing each

    Then I expect Team B:
         Agent 2 rings during 1 min BUT don't pick up

    Then the call ends
    """
    user_1, user_2 = bulk_creation_available_agent

    # external caller A calls ‘Baguette'
    call_sid = twilio.inbound_call(
        external_call_number, receive_call_two_agent_two_teams, 5
    )

    # Expect Team A
    args = [
        (user_1, "decline", 60, 25),
        (user_2, "not_pick_up", 60, 25),
    ]

    multiprocessing(user_action, args)

    # Expect Team B
    args = [
        (user_1, "raise_exception", 30, 25),
        (user_2, "not_pick_up", 30, 60),
    ]

    multiprocessing(user_action, args)

    # Then expected call end
    assert twilio.waiting_until_call_status(call_sid, "completed", 10)


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall1@aircall.io", True), ("aircall2@aircall.io", True)]],
    indirect=True,
)
@pytestrail.case("C10782732")
def test_two_agents_two_dnd(
    bulk_creation_available_agent,
    external_call_number,
    receive_call_two_agent_two_teams,
    user_action,
    multiprocessing,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Team A : Random - Queueing time 1 min
            Agent 1 Unavailable
            Agent 2 Unavailable

        Team B : Simultaneous - Queueing time 1 min
            Agent 1 Unavailable
            Agent 2 Unavailable

        And a external caller A

    When the external caller A calls ‘Baguette'

    Then I expect:
        Agent 1 doesn't ringing
        Agent 2 doesn't ringing

    Then the call ends
    """
    user_1, user_2 = bulk_creation_available_agent

    # When the external caller A calls ‘Baguette'
    call_sid = twilio.inbound_call(
        external_call_number, receive_call_two_agent_two_teams, 3
    )

    # I expect:
    #         Agent 1 doesn't ringing
    #         Agent 2 doesn't ringing
    args = [
        (user_1, "raise_exception", 30),
        (user_2, "raise_exception", 30),
    ]

    multiprocessing(user_action, args)
    # Then the call ends
    assert twilio.waiting_until_call_status(call_sid, "completed", 10)


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall1@aircall.io", False), ("aircall2@aircall.io", True)]],
    indirect=True,
)
@pytestrail.case("C10782731")
def test_two_agents_one_dnd(
    bulk_creation_available_agent,
    external_call_number,
    receive_call_two_agent_two_teams,
    user_action,
    multiprocessing,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Team A : Random - Queueing time 1 min
            Agent 1 Available
            Agent 2 Unavailable

        Team B : Simultaneous - Queueing time 1 min
            Agent 1 Available
            Agent 2 Unavailable

        And a external caller A

    When the external caller A calls ‘Baguette'

    Then I expect Team A:
        Agent 1 rings during 25 s BUT don't pick up
        Agent 2 doesn't ringing

    When all agents timeouts after 25 s ringing each

    Then I expect Team B:
         Agent 1 rings during 1 min BUT don't pick up

    Then the call ends
    """
    user_1, user_2 = bulk_creation_available_agent

    # external caller A calls ‘Baguette'
    call_sid = twilio.inbound_call(
        external_call_number, receive_call_two_agent_two_teams, 5
    )

    # Expect Team A
    args = [
        (user_1, "not_pick_up", 30, 25),
        (user_2, "raise_exception", 30),
    ]

    multiprocessing(user_action, args)

    # Expect Team B
    args = [
        (user_1, "not_pick_up", 30, 60),
        (user_2, "raise_exception", 30),
    ]

    multiprocessing(user_action, args)

    assert twilio.waiting_until_call_status(call_sid, "completed", 10)


# TODO : waiting time external waiting before go to the second distrib
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall1@aircall.io", False), ("aircall2@aircall.io", False)]],
    indirect=True,
)
@pytestrail.case("C10782734")
def test_two_agents_one_busy(
    bulk_creation_available_agent,
    external_call_number,
    receive_call_two_agent_two_teams,
    user_action,
    multiprocessing,
    make_outbound_call_and_wait,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Team A : Random - Queueing time 1 min
            Agent 1 Available
            Agent 2 Busy

        Team B : Simultaneous - Queueing time 1 min
            Agent 1 Available
            Agent 2 Busy

        And a external caller A

    When the external caller A calls ‘Baguette'

    Then I expect Team A:
        Agent 1 rings during 25 s BUT don't pick up
        Agent 2 doesn't ringing

    When all agents timeouts after 1min ringing each

    Then I expect Team B:
         Agent 1 rings during 1 min BUT don't pick up

    Then the call ends
    """
    user_1, user_2 = bulk_creation_available_agent

    # Busy Agent2
    make_outbound_call_and_wait(user_2, external_call_number)

    # External caller A calls ‘Baguette'
    call_sid = twilio.inbound_call(
        external_call_number, receive_call_two_agent_two_teams, 5
    )

    # Expect Team A
    args = [
        (user_1, "not_pick_up", 30, 25),
        (user_2, "raise_exception", 30),
    ]

    multiprocessing(user_action, args)

    # Expect Team B
    args = [
        (user_1, "not_pick_up", 60, 60),
        (user_2, "raise_exception", 30),
    ]

    multiprocessing(user_action, args)

    assert twilio.waiting_until_call_status(call_sid, "completed", 10)


# TODO : waiting time external waiting before go to the second distrib
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall1@aircall.io", False), ("aircall2@aircall.io", False)]],
    indirect=True,
)
@pytestrail.case("C10782735")
def test_two_agents_two_busy(
    bulk_creation_available_agent,
    external_call_number,
    receive_call_two_agent_two_teams,
    user_action,
    multiprocessing,
    make_outbound_call_and_wait,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Team A : Random - Queueing time 1 min
            Agent 1 Busy
            Agent 2 Busy

        Team B : Simultaneous - Queueing time 1 min
            Agent 1 Busy
            Agent 2 Busy

        And a external caller A

    When the external caller A calls ‘Baguette'

    Then I expect Team A:
        Agent 1 doesn't ringing
        Agent 2 doesn't ringing

    When all agents timeouts after 1min ringing each

    Then I expect Team B:
         Agent 1 doesn't ringing
         Agent 2 doesn't ringing

    When all agents timeouts after 1min ringing each

    Then the call ends
    """
    user_1, user_2 = bulk_creation_available_agent

    # Busy status agent
    args = [
        (user_1, external_call_number),
        (user_2, external_call_number),
    ]

    multiprocessing(make_outbound_call_and_wait, args)

    call_sid = twilio.inbound_call(
        external_call_number, receive_call_two_agent_two_teams, 5
    )

    # expected never ring
    args = [
        (user_1, "raise_exception", 120),
        (user_2, "raise_exception", 120),
    ]

    multiprocessing(user_action, args)
    assert twilio.waiting_until_call_status(call_sid, "completed", 15)


@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall1@aircall.io", False), ("aircall2@aircall.io", False)]],
    indirect=True,
)
@pytestrail.case("C10782736")
def test_two_agents_one_busy_then_available(
    bulk_creation_available_agent,
    external_call_number,
    receive_call_two_agent_two_teams,
    user_action,
    multiprocessing,
    make_outbound_call_and_wait,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Team A : Random - Queueing time 1 min
            Agent 1 Available
            Agent 2 Busy -> Available

        Team B : Simultaneous - Queueing time 1 min
            Agent 1 Available
            Agent 2 Available

        And a external caller A

    When the external caller A calls ‘Baguette'

    Then I expect Team A:
       A random agent (Agent 1) rings during 25 s BUT don't pick up
       Before ending the 25 s ringing to Agent 1 (therefore, before moving to next distribution item):
       Agent 2 is  transitioning from busy to available

    When all agents timeouts after 1min ringing each

    Then I expect Team B:
         Agent 1 rings
         Agent 2 rings

    When all agents timeouts after 1min ringing each

    Then the call ends
    """
    user_1, user_2 = bulk_creation_available_agent

    # Busy status agent
    make_outbound_call_and_wait(user_2, external_call_number)

    call_sid = twilio.inbound_call(
        external_call_number, receive_call_two_agent_two_teams, 7
    )

    # expect Team A
    args = [
        (user_1, "not_pick_up", 60, 25),
        (user_2, "busy_to_available_ring", 60, 25),
    ]

    multiprocessing(user_action, args)

    # expect Team B
    args = [
        (user_1, "not_pick_up", 60, 60),
        (user_2, "not_pick_up", 60, 60),
    ]

    multiprocessing(user_action, args)

    assert twilio.waiting_until_call_status(call_sid, "completed", 15)


@pytest.mark.skip(reason="deprecated phone feature")
def test_two_agents_one_available_then_busy(
    open_user_available, external_call_number, receive_call_two_agent_two_teams
):
    pass
