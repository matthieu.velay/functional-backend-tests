from time import sleep

import pytest
from back_tests.framework.common.twilio import twilio


@pytest.mark.smoke_test_call_service
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall+call-service-smoke@aircall.io", False)]],
    indirect=True,
)
def test_basic_inbound_call(
    bulk_creation_available_agent,
    external_call_number,
    call_service_number,
    inbound_call_checking,
):
    """
    Basic Inbound call test

    * Scenario:
      1. One Agents available
      2. User receive an incoming call
      3. The user answers

    * Acceptance criteria
        The call is active and can perform all available actions :
            - mute, hold, keypad, recording, adding people on the conference and transfer
        This check prevent that the call stay stuck in the ringing view
    """
    (user_1,) = bulk_creation_available_agent
    _ = twilio.inbound_call(external_call_number, call_service_number, 3)

    inbound_call_checking(user_1)


@pytest.mark.smoke_test_call_service
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall+call-service-smoke@aircall.io", False)]],
    indirect=True,
)
def test_basic_outbound_call(
    bulk_creation_available_agent,
    external_call_number,
    outbound_call_checking,
):
    """
    Basic Outbound call test

    * Scenario:
      1. One Agents available
      2. User starting an outbound call
      3. The user waits until the call is answered

    * Acceptance criteria
        The call is active and can perform all available actions :
            - mute, hold, keypad, recording, adding people on the conference and transfer
        This check prevent that the call stay stuck in the ringing view
    """
    (user_1,) = bulk_creation_available_agent

    outbound_call_checking(user_1, external_call_number)


@pytest.mark.smoke_test_call_service
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall+call-service-smoke@aircall.io", False)]],
    indirect=True,
)
def test_outbound_to_blacklisted(
    bulk_creation_available_agent,
    external_blacklisted_number,
    make_outbound_call,
    call_ringing_then_stop,
):
    """
    Basic Outbound call test

    * Scenario:
      1. One Agents available
      2. User starting an outbound call to a blacklisted number

    * Acceptance criteria
      The call must fail
    """
    (user_1,) = bulk_creation_available_agent

    make_outbound_call(user_1, external_blacklisted_number)

    call_ringing_then_stop(user_1, 5)


@pytest.mark.smoke_test_call_service
@pytest.mark.parametrize("waiting_time", [t / 10 for t in range(5, 30, 5)])
@pytest.mark.parametrize("hang_up_from", ["agent", "external"])
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall+call-service-smoke@aircall.io", False)]],
    indirect=True,
)
def test_pickup_hangup_concurrency_inbound(
    bulk_creation_available_agent,
    external_call_number,
    call_service_number,
    call_ended_or_keyboard,
    hang_up_from,
    user_action,
    waiting_time,
):
    """
    Pick up / Hang up concurrency call

    * Scenario:
      1. One Agent available
      2. External calls
      3. Agent picks up
      4. <hang_up_from> hangs up simultaneously

    * Acceptance criteria
      The call must gracefully ends after at most the 15-seconds Livecall timeout
      Agent should be available
    """
    (user,) = bulk_creation_available_agent

    # Inbound call triggered
    call_sid = twilio.inbound_call(external_call_number, call_service_number, 3)

    # Agent is answering
    user_action(user, "accept")

    sleep(waiting_time)  # Random waiting time to trigger the concurrency issue

    if hang_up_from == "external":
        twilio.hang_up(call_sid)
        assert call_ended_or_keyboard(user)
    elif hang_up_from == "agent":
        user.hang_up()

    # Agent must be available after at most 15 seconds (Livecall timeout)
    assert user.is_available(30)


@pytest.mark.smoke_test_call_service
@pytest.mark.parametrize(
    "wait_in_call,waiting_time",
    zip([True] + [False] * 10, [t / 10 for t in range(0, 55, 5)]),
)
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall+call-service-smoke@aircall.io", False)]],
    indirect=True,
)
def test_pickup_hangup_concurrency_outbound(
    bulk_creation_available_agent,
    make_outbound_call,
    external_call_number,
    call_ringing,
    wait_in_call,
    waiting_time,
):
    """
    Pick up / Hang up concurrency call

    * Scenario:
      1. One Agent available
      2. Agent makes an outbound call
      3. External picks up
      4. <hang_up_from> hangs up simultaneously

    * Acceptance criteria
      The call must gracefully ends after at most the 15-seconds Livecall timeout
      Agent should be available
    """
    (user,) = bulk_creation_available_agent

    make_outbound_call(user, external_call_number)

    call_ringing(user, 5)

    if wait_in_call:
        user.wait_until_incall()
    else:
        sleep(waiting_time)
    user.hang_up()

    # Agent must be available after at most 15 seconds (Livecall timeout)
    assert user.is_available(30)
