import pytest
from pytest_testrail.plugin import pytestrail

from back_tests.framework.common.twilio import twilio

# https://aircall-product.atlassian.net/browse/CORE-1024
@pytestrail.case("C10797509")
def test_team_random_one_agent_available(
    open_user_available,
    external_call_number,
    receive_call_number_rqt_team_random_one_agent,
    call_timeline,
    interval_measuring,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Team A - Random - 1 min queuing time
            Agent 1 Available
        And a external caller A

    When the external caller A calls ‘Baguette'

    Then I expect:
        Then I expect the routing to keep calling any available agent in the Team,
        trying to distribute Call A, for the entire queuing time:
            Agent 1 rings during 25 s BUT don't pick up
            Agent 1 rings during 25 s BUT don't pick up
            Agent 1 rings during 5 s BUT don't pick up

        When Agent 1 timeouts
        Then the call ends
    """
    # external caller A calls ‘Baguette'

    call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_rqt_team_random_one_agent, 3
    )

    call_timeline(open_user_available, 5, 65)

    assert len(open_user_available.incoming_call_info) == 3

    assert (
        open_user_available.incoming_call_info[0]["call_uuid"]
        == open_user_available.incoming_call_info[1]["call_uuid"]
        == open_user_available.incoming_call_info[2]["call_uuid"]
    )

    for call_info in open_user_available.incoming_call_info[:1]:
        interval_measuring(call_info["duration_ringing"], 25)

    interval_measuring(
        open_user_available.incoming_call_info[2]["duration_ringing"], 10, 80
    )

    # Then the call ends
    assert twilio.waiting_until_call_status(call_sid, "completed", 10)


# https://aircall-product.atlassian.net/browse/CORE-1025
@pytestrail.case("C10797510")
def test_team_random_one_agent_two_callers(
    open_user_available,
    receive_call_number_rqt_team_random_one_agent,
    create_external_calls,
    call_timeline,
    interval_measuring,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Team A - Random - 1 min queuing time
            Agent 1 Available
        And a external caller A
        And a external caller B

    When the external caller A calls ‘Baguette'

    Then I expect:
        Then I expect the routing to keep calling any available agent in the Team,
        trying to distribute CallA, for the entire queuing time:
            Agent 1 rings during 25 s BUT don't pick up
            Agent 1 rings during 25 s BUT don't pick up
            Agent 1 rings during 5 s BUT don't pick up

        When Agent 1 timeouts
        Then the call ends
    """

    # external caller A calls ‘Baguette'
    call_sid, call_sid_2 = create_external_calls(
        receive_call_number_rqt_team_random_one_agent,
        twiml_loop=5,
        number_of_calls=2,
        delays_between_calls=1,
    )

    call_timeline(open_user_available, 5, 65)

    assert len(open_user_available.incoming_call_info) == 3
    interval_measuring(
        open_user_available.incoming_call_info[0]["duration_ringing"], 25
    )
    interval_measuring(
        open_user_available.incoming_call_info[1]["duration_ringing"], 25
    )

    assert open_user_available.incoming_call_info[2]["duration_ringing"] > 0.0

    assert (
        open_user_available.incoming_call_info[0]["call_uuid"]
        == open_user_available.incoming_call_info[1]["call_uuid"]
        == open_user_available.incoming_call_info[2]["call_uuid"]
    )

    # Then the call ends
    assert twilio.waiting_until_call_status(call_sid, "completed", 10)
    assert twilio.waiting_until_call_status(call_sid_2, "completed", 10)


# https://aircall-product.atlassian.net/browse/CORE-1026
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall1@aircall.io", False), ("aircall2@aircall.io", False)]],
    indirect=True,
)
@pytestrail.case("C10797511")
def test_two_agents_available_one_team(
    bulk_creation_available_agent,
    external_call_number,
    receive_call_rqt_two_agents,
    multiprocessing,
    call_timeline,
    sorted_call_timeline,
    interval_measuring,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Team A : Random - Queueing time 1 min
            Agent 1 Available
            Agent 2 Available

        And a external caller A

    When the external caller A calls ‘Baguette'

    Then I expect Team A:
        A random agent (Agent 1) rings during 25 s BUT don't pick up
        When Agent 1 timeouts after 25 s ringing
        Then I expect another agent in the same team to ring
        Another agent (Agent 2) rings during 25 s BUT don't pick up

        Then I expect the routing to keep calling any available agent in the Team for the entire queuing time:
            A random agent (Agent 1) rings approx during 10 s BUT don't pick up

    Then the call ends
    """
    user_1, user_2 = bulk_creation_available_agent

    # external caller A calls ‘Baguette'
    call_sid = twilio.inbound_call(external_call_number, receive_call_rqt_two_agents, 5)

    # Expected Team A
    args = [
        (user_1, 5, 65),
        (user_2, 5, 65),
    ]

    multiprocessing(call_timeline, args)

    list_users = [user_1, user_2]

    call_timeline_info = sorted_call_timeline(list_users)

    # The first ring during 25s
    assert interval_measuring(call_timeline_info[0]["duration_ringing"], 25)

    # The second ring during 25s
    # on a different user than 1
    assert interval_measuring(call_timeline_info[1]["duration_ringing"], 25) and (
        call_timeline_info[0]["user_email"] != call_timeline_info[1]["user_email"]
    )

    # The last ring during 5s
    assert call_timeline_info[2]["duration_ringing"] > 0.0

    # all calls has the same call uuid
    assert (
        call_timeline_info[0]["call_uuid"]
        == call_timeline_info[1]["call_uuid"]
        == call_timeline_info[2]["call_uuid"]
    )

    # Then expected call end
    assert twilio.waiting_until_call_status(call_sid, "completed", 10)


# https://aircall-product.atlassian.net/browse/CORE-1027
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [
        [
            ("aircall1@aircall.io", False),
            ("aircall2@aircall.io", False),
            ("aircall3@aircall.io", False),
        ]
    ],
    indirect=True,
)
@pytestrail.case("C10797513")
def test_three_agents_available_random(
    bulk_creation_available_agent,
    external_call_number,
    receive_call_three_agents_20s,
    multiprocessing,
    call_timeline,
    sorted_call_timeline,
    interval_measuring,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Team A : Random - Queueing time 20 s
            Agent 1 Available
            Agent 2 Available
            Agent 3 Available

        And a external caller A

    When the external caller A calls ‘Baguette'

    Then I expect Team A:
        A random agent (Agent 1) rings during 20 s BUT don't pick up
        When Agent 1 timeouts after 20 s ringing (as it is the max queuing time)
        Then the call ends


    Then the call ends
    """
    user_1, user_2, user_3 = bulk_creation_available_agent

    # external caller A calls ‘Baguette'
    call_sid = twilio.inbound_call(
        external_call_number, receive_call_three_agents_20s, 5
    )

    # Expected Team A
    args = [user_1, user_2, user_3]

    multiprocessing(call_timeline, args)

    call_timeline_info = sorted_call_timeline(args)

    # The first ring during 20s and only one time
    assert interval_measuring(call_timeline_info[0]["duration_ringing"], 20)
    assert len(call_timeline_info) == 1

    # Then expected call end
    assert twilio.waiting_until_call_status(call_sid, "completed", 10)
    assert interval_measuring(twilio.get_call_duration(call_sid), 25)


# https://aircall-product.atlassian.net/browse/CORE-1028
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall1@aircall.io", False), ("aircall2@aircall.io", True)]],
    indirect=True,
)
@pytestrail.case("C10797515")
def test_two_agents_one_dnd(
    bulk_creation_available_agent,
    external_call_number,
    receive_call_rqt_two_agents,
    user_action,
    multiprocessing,
    interval_measuring,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Team A : Random - Queueing time 1 min
            Agent 1 Available
            Agent 2 Unavailable

        And a external caller A

    When the external caller A calls ‘Baguette'

    Then I expect Team A:
        Agent 1 rings during 25 s BUT don't pick up

    When Agent 1 timeouts after 25 s

    Then I expect the routing to keep calling any available agent in the Team for the entire queuing time,
    as there is no other available Agent it will call again Agent 1

        Agent 1 rings

        When Agent 1 rejects/declines

        Then Agent 1 stops ringing

        When 1 min is elapsed

    Then the call ends
    """
    user_1, user_2 = bulk_creation_available_agent

    # external caller A calls ‘Baguette'
    call_sid = twilio.inbound_call(external_call_number, receive_call_rqt_two_agents, 5)

    # Expect Team A
    args = [
        (user_1, "decline", 30, 25),
        (user_2, "raise_exception", 30),
    ]

    multiprocessing(user_action, args)

    assert twilio.waiting_until_call_status(call_sid, "completed", 80)
    assert interval_measuring(twilio.get_call_duration(call_sid), 65)


# https://aircall-product.atlassian.net/browse/CORE-1033
@pytestrail.case("C10797516")
def test_two_agents_two_offline(
    external_call_number,
    receive_call_rqt_two_agents,
    user_action,
    multiprocessing,
    interval_measuring,
):
    """
    Given a Classic number ‘Baguette' with the call distribution :
        Team A : Random - Queueing time 1 min
            Agent 1 Unavailable
            Agent 2 Unavailable

        And a external caller A

    When the external caller A calls ‘Baguette'

    And there are no available users on any of the distribution items:
        Agent 1 is Offline or Do not disturb
        Agent 2 is Offline or Do not disturb

    Then I expect the call to respect the queuing time waiting for a user to become available

    When 1 min is elapsed

    Then the call ends
    """

    # external caller A calls ‘Baguette'
    call_sid = twilio.inbound_call(external_call_number, receive_call_rqt_two_agents, 5)

    assert twilio.waiting_until_call_status(call_sid, "completed", 70)
    assert interval_measuring(twilio.get_call_duration(call_sid), 65)


# https://aircall-product.atlassian.net/browse/CORE-1031
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall1@aircall.io", False), ("aircall2@aircall.io", False)]],
    indirect=True,
)
@pytestrail.case("C10797512")
def test_two_agents_available_one_team_three_callers(
    bulk_creation_available_agent,
    external_call_number,
    receive_call_rqt_two_agents,
    multiprocessing,
    call_timeline,
    sorted_call_timeline,
    interval_measuring,
    create_external_calls,
):
    """
    * Scenario:
        - Agent 1 & 2 Available
        - trigger 3 call(s) with 5 seconds delay between each
        - Agent 1 does not pick up
        - Agent 2 does not pick up

    * Acceptance criteria
        - Agent 1 rings during 25 seconds call A
        - Agent 2 rings during 25 seconds call B
        - Agent 1 rings during 25 seconds call A
        - Agent 2 rings during 25 seconds call B
        - Agent 1 rings during 10 seconds call A
        - Agent 2 rings during 10 seconds call B
        - Agent 1 rings during 10 seconds call C

    - Call A ends
    - Call B ends
    - Call C ends
    """

    user_1, user_2 = bulk_creation_available_agent

    call_sid, call_sid_2, call_sid_3 = create_external_calls(
        receive_call_rqt_two_agents,
        twiml_loop=5,
        number_of_calls=3,
        delays_between_calls=2,
    )

    args = [(user_1, 5, 90), (user_2, 5, 90)]

    multiprocessing(call_timeline, args)

    call_timeline_info = sorted_call_timeline([user_1, user_2])

    # All call should be rings 25s except the last one
    for call_information in call_timeline_info[0:3]:
        assert interval_measuring(call_information["duration_ringing"], 25, 25)

    assert call_timeline_info[0]["user_email"] != call_timeline_info[1]["user_email"]
    assert call_timeline_info[0]["call_uuid"] != call_timeline_info[1]["call_uuid"]

    # Call A's assertion
    assert (
        call_timeline_info[0]["user_email"]
        == call_timeline_info[2]["user_email"]
        == call_timeline_info[4]["user_email"]
    )
    assert (
        call_timeline_info[0]["call_uuid"]
        == call_timeline_info[2]["call_uuid"]
        == call_timeline_info[4]["call_uuid"]
    )

    # Call B's assertion
    assert (
        call_timeline_info[1]["user_email"]
        == call_timeline_info[3]["user_email"]
        == call_timeline_info[5]["user_email"]
    )
    assert (
        call_timeline_info[1]["call_uuid"]
        == call_timeline_info[3]["call_uuid"]
        == call_timeline_info[5]["call_uuid"]
    )

    # Last call information corresponds to Call C
    assert call_timeline_info[-1]["duration_ringing"] > 0.5
    assert call_timeline_info[-1]["call_uuid"] != call_timeline_info[0]["call_uuid"]
    assert call_timeline_info[-1]["call_uuid"] != call_timeline_info[1]["call_uuid"]

    assert twilio.waiting_until_call_status(call_sid, "completed", 10)
    assert twilio.waiting_until_call_status(call_sid_2, "completed", 10)
    assert twilio.waiting_until_call_status(call_sid_3, "completed", 10)


# https://aircall-product.atlassian.net/browse/CORE-1030
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [
        [
            ("aircall1@aircall.io", False),
            ("aircall2@aircall.io", False),
            ("aircall3@aircall.io", False),
        ]
    ],
    indirect=True,
)
@pytestrail.case("C10797514")
def test_three_agents_available_one_team_two_callers(
    bulk_creation_available_agent,
    receive_call_three_agents_rqt,
    multiprocessing,
    call_timeline,
    sorted_call_timeline,
    interval_measuring,
    create_external_calls,
):
    """
    * Scenario:
        - Agent 1 & 2 & 3 Available
        - trigger 2 call(s) with 2 seconds delay between each
        - Agent 1 does not pick up
        - Agent 2 does not pick up
        - Agent 3 does not pick up

    * Acceptance criteria
        - Agent 1 rings during 25 seconds call A
        - Agent 2 rings during 25 seconds call B
        - Agent 3 rings during 25 seconds call A
        - Agent 1 rings during 25 seconds call B
        - Agent 2 rings during 10 seconds call A
        - Agent 3 rings during 10 seconds call B

    - Call A ends
    - Call B ends

    """

    user_1, user_2, user_3 = bulk_creation_available_agent

    call_sid, call_sid_2 = create_external_calls(
        receive_call_three_agents_rqt,
        twiml_loop=5,
        number_of_calls=2,
        delays_between_calls=2,
    )

    args = [(user_1, 5, 90), (user_2, 5, 90), (user_3, 5, 90)]

    multiprocessing(call_timeline, args)

    list_of_users = [user_1, user_2, user_3]

    call_timeline_info = sorted_call_timeline(list_of_users)

    for call_info in call_timeline_info[0:3]:
        assert interval_measuring(call_info["duration_ringing"], 25, 25)

    for call_info in call_timeline_info[4:]:
        assert interval_measuring(call_info["duration_ringing"], 10, 90)

    for user in list_of_users:
        assert len(user.incoming_call_info) == 2
        assert user.incoming_call_info[0] != user.incoming_call_info[1]

    assert (
        call_timeline_info[0]["call_uuid"]
        == call_timeline_info[2]["call_uuid"]
        == call_timeline_info[4]["call_uuid"]
    )

    assert (
        call_timeline_info[1]["call_uuid"]
        == call_timeline_info[3]["call_uuid"]
        == call_timeline_info[5]["call_uuid"]
    )

    assert twilio.waiting_until_call_status(call_sid, "completed", 10)
    assert twilio.waiting_until_call_status(call_sid_2, "completed", 10)
