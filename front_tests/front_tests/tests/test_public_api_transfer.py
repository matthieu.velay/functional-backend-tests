import pytest

from pytest_testrail.plugin import pytestrail

from back_tests.framework.common.twilio import twilio
from back_tests.framework.routing.public_api import public_api


@pytest.mark.webhook
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall1@aircall.io", False), ("aircall2@aircall.io", False)]],
    indirect=True,
)
@pytestrail.case("C10917595")
def test_transfer_to_user_already_answered(
    bulk_creation_available_agent,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    external_call_number,
    receive_call_number_public_api,
    user_action,
    user_public_api_id,
    activate_webhook,
    reset_calls_server,
):
    """
    Given a User A with an user_id '123'
    When test company transfers the call to User A already answered
    Then i expected User A ringing during 25s
    And test company receives a status code 204
    """
    user_1, user_2 = bulk_creation_available_agent

    payload = {"user_id": user_public_api_id}

    twilio.inbound_call(external_call_number, receive_call_number_public_api, 5)

    user_action(user_1, "accept")

    user_1.wait_until_transfer_button_visible()

    current_call_id = daemon_flask_server.call_created[0]["data"]["id"]

    response = public_api.transfers(current_call_id, payload)
    assert response.status_code == 204
    user_action(user_2, "just-ringing")


@pytest.mark.webhook
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [
        [
            ("aircall1@aircall.io", False),
            ("aircall2@aircall.io", False),
            ("aircall3@aircall.io", False),
        ]
    ],
    indirect=True,
)
@pytestrail.case("C10917596")
def test_transfer_to_team_already_answered(
    bulk_creation_available_agent,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    external_call_number,
    receive_call_number_public_api,
    user_action,
    team_public_api_id,
    multiprocessing,
    activate_webhook,
    reset_calls_server,
):
    """
    Given a Team A with an team_id '123'
    When test company transfers the call to Team A already answered
    Then i expected all agents inside Team A ringing during 25s
    And test company receives a status code 204
    """
    payload = {"team_id": team_public_api_id}

    user_1, user_2, user_3 = bulk_creation_available_agent

    twilio.inbound_call(external_call_number, receive_call_number_public_api, 5)

    user_action(user_1, "accept")

    user_1.wait_until_transfer_button_visible()

    current_call_id = daemon_flask_server.call_created[0]["data"]["id"]
    response = public_api.transfers(current_call_id, payload)
    assert response.status_code == 204

    args = [
        (user_3, "just-ringing"),
        (user_2, "just-ringing"),
    ]

    multiprocessing(user_action, args)


@pytest.mark.webhook
@pytestrail.case("C10917597")
def test_transfer_to_external_already_answered(
    open_user_available,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    external_call_number,
    receive_call_number_public_api,
    user_action,
    user_public_api_id,
    activate_webhook,
    reset_calls_server,
):
    """
    Given a External User with an number '12326054602'
    When test company transfers the call to external user already answered
    Then I expected the call not transferred
    And test company receives a status code 400
    """

    payload = {"number": external_call_number}

    twilio.inbound_call(external_call_number, receive_call_number_public_api, 5)

    user_action(open_user_available, "accept")

    open_user_available.wait_until_transfer_button_visible()

    current_call_id = daemon_flask_server.call_created[0]["data"]["id"]

    response = public_api.transfers(current_call_id, payload)
    assert response.status_code == 400
    assert open_user_available.is_incall()


@pytest.mark.webhook
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall1@aircall.io", False), ("aircall2@aircall.io", False)]],
    indirect=True,
)
@pytestrail.case("C10916050")
def test_transfer_to_blacklisted_number(
    bulk_creation_available_agent,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    external_call_number,
    receive_call_number_public_api,
    user_action,
    user_public_api_id,
    activate_webhook,
    blacklisted_number,
    make_outbound_call_and_wait,
    call_timeline,
    reset_calls_server,
):
    """
    Given a blacklisted number with a number  '+123451515'
    When test company transfers the call the blacklisted number
    Then i expected the blacklisted number doesnt ring
    And test company receives a status code 400
    """

    payload = {"number": blacklisted_number}
    user_1, user_2 = bulk_creation_available_agent

    make_outbound_call_and_wait(user_1, external_call_number)

    current_call_id = daemon_flask_server.call_created[0]["data"]["id"]

    response = public_api.transfers(current_call_id, payload)
    assert response.status_code == 400
    call_timeline(user_2)
    assert not user_2.incoming_call_info
    user_1.hang_up()


@pytest.mark.webhook
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [[("aircall1@aircall.io", False), ("aircall2@aircall.io", False)]],
    indirect=True,
)
@pytestrail.case("C10916047")
def test_transfer_to_user_busy(
    bulk_creation_available_agent,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    external_call_number,
    receive_call_number_public_api,
    user_action,
    make_outbound_call_and_wait,
    user_public_api_id,
    activate_webhook,
    reset_calls_server,
):
    """
    Given a a User A with an user_id '123'
    And User A is busy
    When test company transfers the call to User A
    Then i expected user A doesn't ring
    And I expected User A receives a notification in his missed call list
    And test company receives a status code 204
    """
    user_1, user_2 = bulk_creation_available_agent

    make_outbound_call_and_wait(user_2, external_call_number)

    payload = {"user_id": user_public_api_id}

    call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_public_api, 5
    )
    assert twilio.waiting_until_call_status(call_sid, "in-progress", 10)

    current_call_id = daemon_flask_server.call_created[1]["data"]["id"]
    response = public_api.transfers(current_call_id, payload)
    assert response.status_code == 204

    user_action(user_2, "busy_to_available_ring", 30, 0)


@pytest.mark.webhook
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [
        [
            ("aircall1@aircall.io", False),
            ("aircall2@aircall.io", False),
            ("aircall3@aircall.io", False),
        ]
    ],
    indirect=True,
)
@pytestrail.case("C11031386")
def test_transfer_to_team_call_already_answered(
    bulk_creation_available_agent,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    external_call_number,
    receive_call_number_public_api,
    user_action,
    user_public_api_id,
    activate_webhook,
    reset_calls_server,
    multiprocessing,
    call_timeline,
    sorted_call_timeline,
    interval_measuring,
    team_public_api_id,
):
    """
    Given an incoming call on company and Team A with an team_id '123'
    And the following configuration : Team A :
    - Agent 1 Available
    - Agent 2 Available
    - Agent 3 Available
    When call is accepted by an agent
    And test company transfers the call to Team A using random strategy
    Then i expected Agent 3 and Agent 2 to ring for 25s simultaneously (since the transfer is handled as an in call action)
    And Nobody answer, the call comes back to Agent 1
    """
    user_1, user_2, user_3 = bulk_creation_available_agent

    # Team name : public-API-front
    payload = {"team_id": team_public_api_id}

    call_sid = twilio.inbound_call(
        external_call_number, receive_call_number_public_api, 5
    )

    user_action(user_1, "accept")

    user_1.wait_until_transfer_button_visible()

    current_call_id = daemon_flask_server.call_created[0]["data"]["id"]

    response = public_api.transfers(current_call_id, payload)
    assert response.status_code == 204

    args = [user_2, user_3]

    multiprocessing(call_timeline, args)
    call_timeline_info = sorted_call_timeline(args)

    # ringing 30s and at the same time
    assert interval_measuring(call_timeline_info[0]["duration_ringing"], 30)
    assert interval_measuring(call_timeline_info[1]["duration_ringing"], 30)
    assert call_timeline_info[1]["ringing_at"] == call_timeline_info[0]["ringing_at"]

    # The call come back and user_1 decline
    user_action(user_1, "decline")
    assert twilio.waiting_until_call_status(call_sid, "completed", 10)


@pytest.mark.webhook
@pytest.mark.parametrize(
    "bulk_creation_available_agent",
    [
        [
            ("aircall1@aircall.io", False),
            ("aircall2@aircall.io", False),
            ("aircall3@aircall.io", False),
        ]
    ],
    indirect=True,
)
@pytestrail.case("C11031474")
def test_transfer_to_team_busy_call_already_answered(
    bulk_creation_available_agent,
    daemon_flask_server,
    daemon_ngrok_tunnel,
    external_call_number,
    receive_call_number_public_api,
    user_action,
    user_public_api_id,
    activate_webhook,
    reset_calls_server,
    multiprocessing,
    call_timeline,
    sorted_call_timeline,
    interval_measuring,
    make_outbound_call_and_wait,
    team_public_api_id,
):
    """
    Given an incoming call on company and Team A with an team_id '123'
    And the following configuration : Team A :
    - Agent 1 Available
    - Agent 2 busy
    - Agent 3 Available
    When call is accepted by an agent
    And test company transfers the call to Team A using random strategy
    Then i expected Agent 3 to ring for 25s
    And i expected Agent 2 doesn't ring
    And call still in waiting
    """
    user_1, user_2, user_3 = bulk_creation_available_agent

    # Team name : public-API-front
    payload = {"team_id": team_public_api_id, "dispatching_strategy": "simultaneous"}

    _ = twilio.inbound_call(external_call_number, receive_call_number_public_api, 5)

    user_action(user_1, "accept")
    user_1.wait_until_transfer_button_visible()

    make_outbound_call_and_wait(user_2, external_call_number)

    current_call_id = daemon_flask_server.call_created[0]["data"]["id"]

    response = public_api.transfers(current_call_id, payload)
    assert response.status_code == 204

    # call timeline waiting during 65s, this number can be increase
    args = [
        (user_2, 5, 65),
        (user_3, 5, 65),
    ]

    multiprocessing(call_timeline, args)

    list_users = [user_2, user_3]
    call_timeline_info = sorted_call_timeline(list_users)

    # only user 3 ringing, one time during 30s
    assert (
        interval_measuring(call_timeline_info[0]["duration_ringing"], 30)
        and call_timeline_info[0]["user_email"] == "aircall3@aircall.io"
        and len(call_timeline_info) == 1
    )

    # TODO : Teardown should be remove from the test
    user_2.hang_up()
