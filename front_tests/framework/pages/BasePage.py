import os

from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

ENV = os.getenv("ENV")


class BasePage:
    def __init__(self, driver, url=""):
        if ENV == "staging":
            self.base_url = "https://phone.aircall-staging.com/" + url
        elif ENV == "production":
            self.base_url = "https://phone.aircall.io/" + url
        else:
            raise ("ENV must be 'staging' or 'production'")
        self.driver = driver
        self.wait = WebDriverWait(driver, 30)

    def get_element(self, locator):
        return self.driver.find_element(By.CSS_SELECTOR, locator)

    def fill_form(self, locator, value):
        elem = self.driver.find_element(By.CSS_SELECTOR, locator)
        elem.send_keys(value)

    def action_fill_form(self, locator, value):

        # initiate new instance of action
        action = ActionChains(self.driver)

        element = self.get_element(locator)
        action.send_keys_to_element(element, value)
        action.perform()

    def click(self, locator):
        elem = self.driver.find_element(By.CSS_SELECTOR, locator)
        elem.click()

    def navigate(self):
        self.driver.get(self.base_url)

    def get_current_url(self):
        return self.driver.current_url

    # TODO : add try and except for waiting element
    def wait_current_url_change(self):
        waiting_current_change = self.get_current_url()
        self.wait.until(lambda driver: driver.current_url != waiting_current_change)

    def wait_element_is_visible(self, locator, timeout=30):
        self.wait = WebDriverWait(self.driver, timeout)
        self.wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, locator)))

    def wait_element_is_clickable(self, locator, timeout=30):
        self.wait = WebDriverWait(self.driver, timeout)
        self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, locator)))
