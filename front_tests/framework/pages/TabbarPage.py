from .BasePage import BasePage
from selenium.common.exceptions import TimeoutException

import logging

log = logging.getLogger("USER")


class Tabbar(BasePage):
    tab_bar_settings = '[data-test="tab-bar-settings"]'
    available_button = 'label[class*="_available"], label[class*="styles_green"]'
    do_not_disturb_button = 'label[class*="_unavailable"], label[class*="styles_red"]'
    auto_button = 'label[class*="_auto"], label[class*="styles_blue"]'
    avatar_available_status = '[data-test-availability-status="available"]'
    avatar_dnd_status = '[data-test-availability-status="do_not_disturb"]'
    tab_bar_keyboard = '[data-test="tab-bar-keypad"]'

    def __init__(self, browser):
        BasePage.__init__(self, browser)

    def click_on_settings(self):
        self.click(self.tab_bar_settings)

    def click_available(self):
        self.click(self.available_button)

    def click_do_not_disturb(self):
        self.click(self.do_not_disturb_button)

    def click_auto(self):
        self.click(self.auto_button)

    def switch_availability(self, status):
        self.click_on_settings()
        if status == "available":
            self.click_available()
            self.wait_until_avatar_available()
            log.info("Switch to available")

        if status == "do not disturb":
            self.click_do_not_disturb()
            self.wait_until_avatar_dnd()
            log.info("Switch to do not disturb")

        if status == "auto":
            self.click_auto()

    def wait_until_avatar_available(self, timeout=30):
        self.wait_element_is_visible(self.avatar_available_status, timeout)

    def wait_until_avatar_dnd(self):
        self.wait_element_is_visible(self.avatar_dnd_status)

    def click_on_keyboard(self):
        self.click(self.tab_bar_keyboard)

    def is_available(self, timeout=3):
        try:
            self.wait_until_avatar_available(timeout)
            return True
        except TimeoutException:
            return False
