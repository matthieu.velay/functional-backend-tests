from selenium.common.exceptions import NoSuchElementException

from .BasePage import BasePage
import logging

log = logging.getLogger("USER")


class Ringing(BasePage):
    answer_button = "[data-test='pickup-button']"
    decline_button = "[data-test='hangup-button']"
    minimize_button = '[data-icon="iconMinimize"]'
    minimize_incall_banner = '[data-test="minimized-incall-banner"]'
    data_test_call_active_id = "div[data-test-call-active-id*='CA']"

    def __init__(self, browser, url="ringing"):
        BasePage.__init__(self, browser, url)

    def wait_until_receive_call(self, timeout=30):
        log.info("Wait until receive a call")
        self.wait_element_is_visible(self.answer_button, timeout)
        log.info("Call received with active uuid : {0}".format(self.get_call_uuid()))

    def click_on_answer(self):
        self.click(self.answer_button)

    def click_on_decline(self):
        self.click(self.decline_button)

    # TODO: change into wait until element is clickable ?
    def is_ringing(self):
        try:
            self.get_element(self.answer_button)
            return True
        except NoSuchElementException:
            return False

    def is_ringing_minimize(self):
        try:
            self.get_element(self.minimize_incall_banner)
            return True
        except NoSuchElementException:
            return False

    def click_minimize(self):
        self.click(self.minimize_button)

    def get_call_uuid(self):
        return self.get_element(self.data_test_call_active_id).get_attribute(
            "data-test-call-active-id"
        )
