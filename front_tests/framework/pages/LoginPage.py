from .BasePage import BasePage


class Login(BasePage):

    sign_in_button = '[data-test="signin-button"]'
    email_input = '[data-test="signin-email-input"]'
    password_input = '[data-test="signin-password-input"]'

    def __init__(self, browser, url="login"):
        BasePage.__init__(self, browser, url)

    def set_email(self, value):
        self.action_fill_form(self.email_input, value)

    def set_password(self, value):
        self.action_fill_form(self.password_input, value)

    def click_login(self):
        self.click(self.sign_in_button)
        self.wait_current_url_change()

    def login_on_phone(self, email, password):
        self.set_email(email)
        self.set_password(password)
        return self.click_login()
