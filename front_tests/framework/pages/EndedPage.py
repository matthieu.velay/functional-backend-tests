import logging

from selenium.common.exceptions import NoSuchElementException

from .BasePage import BasePage

log = logging.getLogger("USER")


class EndedCall(BasePage):
    call_ended_view = '[data-test="call-ended-view"]'

    def __init__(self, browser, url="call-ended"):
        BasePage.__init__(self, browser, url)

    def is_call_ended(self):
        try:
            self.get_element(self.call_ended_view)
            return True
        except NoSuchElementException:
            return False

    def wait_until_call_ended(self, timeout):
        log.info("Wait until ended view is displayed")
        self.wait_element_is_visible(self.call_ended_view, timeout)
