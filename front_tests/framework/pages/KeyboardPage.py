import logging

from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException

from .BasePage import BasePage

log = logging.getLogger("USER")


class Keyboard(BasePage):

    keyboard_input = 'input[data-test="keyboard-input-text"]'
    keyboard_dial = '[data-test="keyboard-button-dial"]'

    def __init__(self, browser, url=""):
        BasePage.__init__(self, browser, url)

    def is_keyboard_dial(self):
        try:
            self.get_element(self.keyboard_dial)
            return True
        except NoSuchElementException:
            return False

    def make_outbound_call(self, number):
        keyboard_txt = self.get_element(self.keyboard_input)
        keyboard_txt.send_keys(number)
        keyboard_txt.send_keys(Keys.ENTER)

    def wait_until_keyboard_dial(self, timeout):
        log.info("Wait until keyboard dial is displayed")
        self.wait_element_is_visible(self.keyboard_dial, timeout)

        # TODO: deprecated used dial button
        # self.click(self.keyboard_dial)
