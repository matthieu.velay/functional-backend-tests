from selenium.common.exceptions import NoSuchElementException

from .BasePage import BasePage


class Incall(BasePage):
    hang_up_button = '[data-test="hangup-button"]'
    incall_action = '[data-test="in-call-actions"]'
    transfer_button = "a[href='/incall/transfer'] > div"

    action_hold = "[data-test*='hold']"
    action_mute = "div[data-test='action-mute'],div[data-test='action-unmute']"
    action_keypad = "div[data-test='action-keypad']"
    action_pause_recording = "div[data-test*='-recording']"
    action_add_or_call_button = "a[href='/incall/keyboard']"

    call_status = "div[class*=styles_status]"

    def __init__(self, browser, url="incall"):
        BasePage.__init__(self, browser, url)

    def hang_up(self):
        self.click(self.hang_up_button)

    def wait_until_incall(self, timeout=30):
        self.wait_element_is_visible(self.incall_action, timeout)

    def is_incall(self):
        try:
            self.get_element(self.incall_action)
            return True
        except NoSuchElementException:
            return False

    def wait_until_transfer_button_visible(self, timeout=30):
        self.wait_element_is_visible(self.transfer_button, timeout)

    def wait_until_hold_button_clickable(self, timeout=30):
        self.wait_element_is_clickable(self.action_hold, timeout)

    def wait_until_mute_button_clickable(self, timeout=30):
        self.wait_element_is_clickable(self.action_mute, timeout)

    def wait_until_keypad_button_clickable(self, timeout=30):
        self.wait_element_is_clickable(self.action_keypad, timeout)

    def wait_until_recording_button_clickable(self, timeout=30):
        self.wait_element_is_clickable(self.action_pause_recording, timeout)

    def wait_until_adding_button_clickable(self, timeout=30):
        self.wait_element_is_clickable(self.action_add_or_call_button, timeout)

    def wait_until_transfer_button_clickable(self, timeout=30):
        self.wait_element_is_clickable(self.transfer_button, timeout)
