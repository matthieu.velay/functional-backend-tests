import os

from selenium.common.exceptions import TimeoutException

from .pages.LoginPage import Login
from .pages.RingingPage import Ringing
from .pages.TabbarPage import Tabbar
from .pages.KeyboardPage import Keyboard
from .pages.IncallPage import Incall
from .pages.EndedPage import EndedCall

SCRIPT_ONBOARD = (
    'localStorage.setItem("beta-switch", "{ \\"wizardCompleted\\": true }");'
)
SCRIPT_LANGUAGE = "window.localStorage.setItem('i18nextLng', JSON.stringify('en'));"

ENV = os.getenv("ENV")


class User(Login, Ringing, Tabbar, Keyboard, Incall, EndedCall):
    def __init__(self, email, password, driver):
        self.email = email
        self.password = password
        self.driver = driver

        self.incoming_call_info = []

        Login.__init__(self, driver)
        Ringing.__init__(self, driver)
        Tabbar.__init__(self, driver)
        Keyboard.__init__(self, driver)
        Incall.__init__(self, driver)
        EndedCall.__init__(self, driver)

        if ENV == "staging":
            login_url = "https://phone.aircall-staging.com/login"
        elif ENV == "production":
            login_url = "https://phone.aircall.io/login"
        else:
            raise ("ENV must be 'staging' or 'production'")

        self.driver.get(login_url)
        self.driver.execute_script(SCRIPT_LANGUAGE)
        self.driver.execute_script(SCRIPT_ONBOARD)

        self.login_on_phone(self.email, self.password)

        # If agent is not available at the beginning
        # Switch agent to available
        try:
            self.wait_until_avatar_available()
        except TimeoutException:
            self.switch_availability("available")

    def add_call_info(self, call_info):
        self.incoming_call_info.append(call_info)
