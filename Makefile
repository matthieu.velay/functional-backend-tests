# Signifies our desired python version
PYTHON = python3

# .PHONY defines parts of the makefile that are not dependant on any specific file
# This is most often used to store functions
.PHONY = help black req-install back-test-routing front-test-routing deploy-serverless messaging

# Defining an array variable
FILES = input output

# Defines the default target that `make` will to try to make, or in the case of a phony target, execute the specified commands
# This target is executed whenever we just type `make`
.DEFAULT_GOAL = help

help:
	@echo "---------------HELP-----------------"
	@echo "make black : black reformatting all project"
	@echo "make req-install : install requirements.txt"
	@echo "make back-end-routing : run all back-end telephony"
	@echo "make front-end-routing : run all front-end telephony"
	@echo "make test-public-api : run all public-api transfer test"
	@echo "make deploy-serverless : deploy livecall mock serverless"
	@echo "make run + ARGS : run the test given as ARGS otherwise run all tests in repositories"
	@echo "------------------------------------"

req-install:
	@echo "Install requirements.txt"
	pip3 install -r requirements.txt
	@echo "Install requirements.txt done"

black :
	black .

back-test-routing:
	${PYTHON} -m pytest -vs -k "not webhook and not messaging" back_tests/tests/

front-test-routing:
	${PYTHON} -m pytest -vs -k "not webhook" front_tests/tests/

messaging:
	${PYTHON} -m pytest "messaging" back_tests/tests/

test-public-api:
	${PYTHON} -m pytest -vs -k "webhook"

deploy-sam:
	cd mock_livecall; sam deploy --debug

run:
	@echo "NEEDS ARGUMENTS"
	${PYTHON} -m pytest -vs $(filter-out $@,$(MAKECMDGOALS))